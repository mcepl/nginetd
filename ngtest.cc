#include "nginetd.h"

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <netinet/in.h>


namespace msg = nginetd::msg;

int main(int argc, const char * const argv[])
{
  struct sockaddr_un addr;
  int rc;
  int s = socket(PF_UNIX, SOCK_SEQPACKET, 0);

  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, "/tmp/nginetd.sock");

  rc = connect(s, (struct sockaddr *) &addr, SUN_LEN(&addr));
  printf("rc=%d\n", rc);

  usleep(100000);

  {
    msg::Hello_Request_Builder hello;
    hello.version(1);

    struct msghdr hdr;
    hdr.msg_name = NULL;
    hdr.msg_namelen = 0;
    hdr.msg_iov = hello.iov();
    hdr.msg_iovlen = hello.iovlen();
    hdr.msg_control = NULL;
    hdr.msg_controllen = 0;
    hdr.msg_flags = 0;

    rc = sendmsg(s, &hdr, MSG_EOR);
    printf("rc=%d\n", rc);
  }

  if (false)
  {
    msg::Bind_Request_Builder bind;
    bind.service((argc > 1) ? argv[1] : "ngtest");

    struct msghdr hdr;
    hdr.msg_name = NULL;
    hdr.msg_namelen = 0;
    hdr.msg_iov = bind.iov();
    hdr.msg_iovlen = bind.iovlen();
    hdr.msg_control = NULL;
    hdr.msg_controllen = 0;
    hdr.msg_flags = 0;

    rc = sendmsg(s, &hdr, MSG_EOR);
    printf("rc=%d\n", rc);
  }

  usleep(100000);

  {
    msg::Listen_Request_Builder listen;
    listen.service((argc > 1) ? argv[1] : "ngtest");

    struct msghdr hdr;
    hdr.msg_name = NULL;
    hdr.msg_namelen = 0;
    hdr.msg_iov = listen.iov();
    hdr.msg_iovlen = listen.iovlen();
    hdr.msg_control = NULL;
    hdr.msg_controllen = 0;
    hdr.msg_flags = 0;

    rc = sendmsg(s, &hdr, MSG_EOR);
    printf("rc=%d\n", rc);
  }

  usleep(100000);

  while (true)
  {
    struct msghdr hdr;
    struct iovec iov;

    union
    {
      void *ptr;
      char buffer[1024];
    } data;

    union
    {
      struct cmsghdr cmsg;
      char buffer[16 * CMSG_SPACE(sizeof(int))];
    } cdata;

    iov.iov_base = data.buffer;
    iov.iov_len = sizeof(data.buffer);
    hdr.msg_name = NULL;
    hdr.msg_namelen = 0;
    hdr.msg_iov = &iov;
    hdr.msg_iovlen = 1;
    hdr.msg_control = cdata.buffer;
    hdr.msg_controllen = sizeof(cdata.buffer);
    hdr.msg_flags = 0;

    rc = recvmsg(s, &hdr, 0);
    printf("rc=%d %lu %d\n", rc, hdr.msg_controllen, hdr.msg_flags);

    if (rc > 0)
    {
      const msg::Header &hdr = msg::Header_Proxy::get(data.buffer, rc);
      if (hdr.msg_type() == msg::Accept_Event::MSG)
      {
	const msg::Accept_Event_Proxy &accept_event =
	  msg::Accept_Event_Proxy::get(data.buffer, rc);
	printf("accept event rdns=%s\n", accept_event.rdns_ptr());

	const int sock = *(const int *) CMSG_DATA(&cdata.cmsg);
	printf("socket=%d\n", sock);

	shutdown(sock, SHUT_WR);

	char buffer[1024];
	size_t len = sizeof(buffer);
	while (recv(sock, buffer, sizeof(buffer), 0) > 0)
	{ }

	break;
      }
      else if (hdr.msg_type() == msg::Bind_Reply::MSG)
      {
	const msg::Bind_Reply_Proxy &bind_reply =
	  msg::Bind_Reply_Proxy::get(data.buffer, rc);
	printf("bind_reply error=%d\n", bind_reply.error());
	printf("socket=%d\n", *((const int *) CMSG_DATA(&cdata.cmsg)));
      }
    }
    else
    {
      break;
    }
  }

  close(s);

  return 0;
}
