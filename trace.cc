/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "trace.h"

#include <algorithm>
#include <iterator>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <sstream>
#include <utility>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


namespace
{
const char trace_level_char[] =
{
  '*', 'C', 'E', 'W', 'I', 'D'
};
}

class Trace_Multiple_Writer
  : public Trace_Writer
{
 public:
  Trace_Multiple_Writer(Trace_Writer &writer1, Trace_Writer &writer2,
			Trace_Levels output_level = TRACE_DEBUG)
    : Trace_Writer(output_level),
      writer1_(writer1), writer2_(writer2)
  { }

  ~Trace_Multiple_Writer()
  { }


  virtual void trace(Trace_Levels trace_level,
		     const std::list<std::pair<char *, size_t> > &buf)
  {
    if (trace_level <= output_level())
    {
      writer1_.trace(trace_level, buf);
      writer2_.trace(trace_level, buf);
    }
  }


 private:
  Trace_Writer &writer1_;
  Trace_Writer &writer2_;
};


class Trace_File_Writer
  : public Trace_Writer
{
  static const unsigned long MAX_BYTES_IN_FILE = 200000;
  static const unsigned long MAX_ENTRIES_IN_FILE = 10000;

 public:
  Trace_File_Writer(Trace_Levels output_level = TRACE_DEBUG)
    : Trace_Writer(output_level),
      trace_dir_(""), filename_("server.trc"),
      bytes_written_(MAX_BYTES_IN_FILE),
      entries_written_(MAX_ENTRIES_IN_FILE)
  { }

  ~Trace_File_Writer()
  { }


  virtual void trace(Trace_Levels trace_level,
		     const std::list<std::pair<char *, size_t> > &buf)
  {
    if (trace_level <= output_level())
    {
      std::lock_guard<std::mutex> guard(output_sync_);

      if ((bytes_written_ >= MAX_BYTES_IN_FILE) ||
	  (entries_written_ > MAX_ENTRIES_IN_FILE))
      {
	rotate();
      }

      output_stream_ << '<' << trace_level_char[trace_level] << "> ";
      bytes_written_ += 4;

      for (std::list<std::pair<char *, size_t> >::const_iterator iter =
	     buf.begin();
	   iter != buf.end();
	   ++iter)
      {
	output_stream_.write(iter->first, iter->second);
	bytes_written_ += iter->second;
      }

      output_stream_ << std::flush;
      entries_written_++;
    }
  }


 protected:
  void rotate()
  {
    if (output_stream_.is_open())
    {
      output_stream_.close();
    }

    std::string::size_type ext_pos = filename_.find_last_of('.');

    struct stat stat_buf;
    if (!stat((trace_dir_ + filename_).c_str(), &stat_buf))
    {
      // we use a ostrstream to build our filenames
      std::unique_ptr<std::ostringstream> old_filename(new std::ostringstream());
      *old_filename << trace_dir_
		    << std::string(filename_, 0, ext_pos)
		    << '-' << GENERATIONS
		    << std::string(filename_, ext_pos);

      unlink(old_filename->str().c_str());

      for (int i = GENERATIONS; i > 1; i--)
      {
	std::unique_ptr<std::ostringstream> new_filename(old_filename.release());
	old_filename =
	  std::unique_ptr<std::ostringstream>(new std::ostringstream());
	*old_filename << trace_dir_
		      << std::string(filename_, 0, ext_pos)
		      << '-' << (i - 1)
		      << std::string(filename_, ext_pos);

	unlink(new_filename->str().c_str());
	rename(old_filename->str().c_str(), new_filename->str().c_str());
      }

      rename((trace_dir_ + filename_).c_str(), old_filename->str().c_str());
    }


    bytes_written_ = entries_written_ = 0;
    output_stream_.open((trace_dir_ + filename_).c_str());
  }

  static const int GENERATIONS = 9;


 private:
  const std::string trace_dir_;
  const std::string filename_;

  unsigned long bytes_written_;
  unsigned long entries_written_;

  std::ofstream output_stream_;
  std::mutex output_sync_;
};


class Trace_Console_Writer
  : public Trace_Writer
{
 public:
  Trace_Console_Writer(Trace_Levels output_level = TRACE_DEBUG)
    : Trace_Writer(output_level)
  { }

  ~Trace_Console_Writer()
  { }


  virtual void trace(Trace_Levels trace_level,
		     const std::list<std::pair<char *, size_t> > &buf)
  {
    if (trace_level <= output_level())
    {
      std::lock_guard<std::mutex> guard(output_sync_);

      std::cerr << '<' << trace_level_char[trace_level] << "> ";
      for (std::list<std::pair<char *, size_t> >::const_iterator iter =
	     buf.begin();
	   iter != buf.end();
	   ++iter)
      {
	std::cerr.write(iter->first, iter->second);
      }
    }
  }

 private:
  std::mutex output_sync_;
};



Trace_Writer::Stream_Buffer::~Stream_Buffer()
{
  cleanup_buffer();
  buflst_.pop_back();
}


void Trace_Writer::Stream_Buffer::cleanup_buffer()
{
  while (buflst_.size() > 1)
  {
    delete[] buflst_.back().first;
    buflst_.pop_back();
  }
}


int Trace_Writer::Stream_Buffer::overflow(int c)
{
  if (c == traits_type::eof())
  {
    return sync() ? traits_type::eof() : 0;
  }

  if (pptr() == epptr())
  {
    const std::pair<char *, size_t> newbuf =
      std::make_pair(new char[BUF_SIZE], BUF_SIZE);

    setp(newbuf.first, newbuf.first + newbuf.second);
    buflst_.push_back(newbuf);
  }

  *pptr() = char(c);
  pbump(1);

  return c;
}

int Trace_Writer::Stream_Buffer::sync()
{
  buflst_.back().second = (pptr() - pbase());
  write_trace(buflst_);

  cleanup_buffer();

  setp(pbuf_, pbuf_ + sizeof(pbuf_));
  buflst_.front().first = pbuf_;
  buflst_.front().second = sizeof(pbuf_);

  return 0;
}

int Trace_Writer::Stream_Buffer::underflow()
{
  return traits_type::eof();
}


void Trace_Writer::Stream_Buffer::write_trace(
  const std::list<std::pair<char *, size_t> > &buf)
{
  trace_writer_.trace(current_trace_level_, buf);
}


static Trace_File_Writer trace_file_writer_;
Trace_Writer &trace_file_writer = trace_file_writer_;

static Trace_Console_Writer trace_console_writer_;
Trace_Writer &trace_console_writer = trace_console_writer_;

static Trace_Multiple_Writer trace_multiple_writer_(trace_file_writer,
						    trace_console_writer);

Trace_Writer &Trace_Stream::trace_writer_ = trace_multiple_writer_;
thread_local std::unique_ptr<Trace_Stream::Stream_Buffer_Pair> Trace_Stream::thread_specific_trace_;


std::ostream &Trace_Stream::trace_stream(bool reset, Trace_Levels trace_level)
{
  if (!thread_specific_trace_)
  {
    std::unique_ptr<Trace_Writer::Stream_Buffer> buffer(new Trace_Writer::Stream_Buffer(trace_writer_));
    std::unique_ptr<std::ostream> stream(new std::ostream(buffer.get()));

    thread_specific_trace_.reset(new Stream_Buffer_Pair(std::move(stream), std::move(buffer)));
  }

  if (reset)
  {
    thread_specific_trace_->second->level(trace_level);
  }

  return *thread_specific_trace_->first;
}


std::ostream &Trace_Stream::stream(const Trace_Levels level,
				   const bool reset) const
{
  std::ostream &stream = trace_stream(reset, level);
  if (reset)
  {
    stream << '[' << module_name_ << "] ";
  }

  return stream;
}


std::ostream Trace_Stream::null_stream(NULL);
Trace_Levels Trace_Stream::global_level_(TRACE_NONE);


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
