/*	-*- C++ -*-
 * Copyright (C) 2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__FIBER_HANDLER__H
#define INCLUDED__FIBER_HANDLER__H

#include "trace.h"

#include <functional>


class Fiber_Handler
{
  static Trace_Stream logger;

  friend void fiber_handler_cleanup_fiber(void * const context);

public:
  Fiber_Handler() = delete;
  Fiber_Handler(Fiber_Handler const &) = delete;
  Fiber_Handler &operator =(Fiber_Handler const &) = delete;
  ~Fiber_Handler() = delete;


  void static yield();
  void static schedule_fiber(void *fiber);

  void static create_fiber(const std::function<void ()> &func,
		    const size_t stack_size = 16*4096);
  void static destroy_fiber();

  inline static void *current_fiber()
  {
#if defined(_WIN32)
    return GetCurrentFiber();
#else
    return current_fiber_;
#endif
  }

  static std::function<void (std::function<void ()> const &)> get_wrapper();

 protected:
#if defined(POSIX)
  inline static void current_fiber(void *fiber)
  {
    current_fiber_ = fiber;
  }
#endif

  inline static void main_fiber(void *fiber)
  {
    main_fiber_ = fiber;
  }

  inline static void *main_fiber()
  {
    return main_fiber_;
  }

 private:
#if defined(_WIN32)
  static __declspec(thread) void *main_fiber_;
#else
  static thread_local void *main_fiber_;
  static thread_local void *current_fiber_;
#endif
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
