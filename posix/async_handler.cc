/*	-*- C++ -*-
 * Copyright (C) 2003-2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_handler.h"

#include "../async_resolver.h"
#include "../async_socket.h"
#include "../async_timer.h"
#include "../async_worker.h"

#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include <utility>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }


namespace
{
  int set_socket_nonblocking(int socket)
  {
    int rc_nonblock;
    int flags = ::fcntl(socket, F_GETFL, 0);
    if (flags >= 0)
    {
      rc_nonblock = ::fcntl(socket, F_SETFL, flags | O_NONBLOCK);
    }
    else
    {
      rc_nonblock = flags;
    }

    return rc_nonblock;
  }
}


void Async_Handler::release(std::unique_ptr<Async_Socket> async_socket)
{
  std::lock_guard<std::mutex> guard(socket_pool_sync_);
  socket_pool_.push_back(std::move(async_socket));
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
