/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_socket.h"

#include "../async_handler.h"

#include <algorithm>
#include <numeric>

#include <sys/socket.h>

#include <errno.h>
#include <string.h>
#include <unistd.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }


namespace
{
  template<typename T>
  Async_Socket::req_id_t pop_front_and_cancelled(T & cont)
  {
    Async_Socket::req_id_t popped = 0;

    do
    {
      cont.pop_front();
      ++popped;
    } while (! cont.empty() && cont.front().cancelled);

    return popped;
  }

  size_t accumulate_iov_len(size_t i, const struct iovec &iov)
  {
    return i + iov.iov_len;
  }

  struct copy_iovec_data
  {
    explicit copy_iovec_data(char *ptr)
      : ptr_(ptr)
    { }

    void operator () (const struct iovec & iov)
    {
      ptr_ = std::copy(reinterpret_cast<char *>(iov.iov_base),
		       reinterpret_cast<char *>(iov.iov_base) + iov.iov_len,
		       ptr_);
    }

   private:
    char *ptr_;
  };
}


Async_Socket::Async_Socket(Async_Handler &handler)
  : handler_(handler), socket_(-1), event_data_(this)
  , recv_req_id_(0), recv_events_(false)
  , send_req_id_(0), send_events_(false)
{ }

Async_Socket::~Async_Socket()
{
  close();
}

void Async_Socket::init(socket_t socket)
{
  TRACE(logger.debug(), "init fd=" << socket);
  socket_ = socket;
  recv_req_id_ = send_req_id_ = 0;
  recv_events_ = send_events_ = false;
}

Async_Socket::req_id_t Async_Socket::send(
  const char *data, size_t len, int flags,
  const Async_Handler::send_handler &cb)
{
  if (len == 0)
  {
    if (cb)
    {
      cb(EINVAL);
    }
    return NO_REQ_ID;
  }

  std::unique_lock<std::mutex> guard(send_queue_sync_);
  if (auto rguard = handler_.recurse())
  {
    if (send_queue_.empty())
    {
      int rc_send;
      RETRY_EINTR_RC(rc_send, ::send(socket_, data, len, flags));
      if ((rc_send < 0) && (errno != EWOULDBLOCK) && (errno != EAGAIN))
      {
	const int last_error = errno;
	guard.unlock();
	if (cb)
	{
	  cb(last_error);
	}
	return NO_REQ_ID;
      }
      else if (rc_send >= 0)
      {
	data += rc_send;
	len -= rc_send;

	if (len == 0)
	{
	  guard.unlock();
	  if (cb)
	  {
	    cb(0);
	  }
	  return NO_REQ_ID;
	}
      }
    }
  }

  char * const copied_data = new char[len];
  std::copy(data, data + len, copied_data);

  const req_id_t req_id = send_req_id_ + send_queue_.size();
  send_queue_.push_back(send_request(copied_data, len, flags, cb));

#if defined(HAVE_EPOLL)
  if (!send_events_)
  {
    guard.unlock();

    std::lock_guard<std::mutex> recv_guard(recv_queue_sync_);
    guard.lock();

    if (!send_events_ && !send_queue_.empty())
    {
      send_events_ = true;
      handler_.register_events(*this, recv_events_, send_events_);
    }
  }
#elif defined(HAVE_KQUEUE)
  if (!send_events_)
  {
    send_events_ = true;
    handler_.register_send(*this);
  }
#endif

  return req_id;
}


Async_Socket::req_id_t Async_Socket::sendmsg(
  const struct iovec *iov, int iov_len, const char *cdata, size_t clen,
  int flags, const Async_Handler::sendmsg_handler &cb)
{
  if (iov_len == 0)
  {
    if (cb)
    {
      cb(EINVAL);
    }
    return NO_REQ_ID;
  }

  std::unique_lock<std::mutex> guard(send_queue_sync_);
  if (auto rguard = handler_.recurse())
  {
    if (send_queue_.empty())
    {
      struct msghdr hdr;

      hdr.msg_name = NULL;
      hdr.msg_namelen = 0;
      hdr.msg_iov = const_cast<struct iovec *>(iov);
      hdr.msg_iovlen = iov_len;
      hdr.msg_control = const_cast<char *>(cdata);
      hdr.msg_controllen = clen;
      hdr.msg_flags = 0;

      int rc_send;
      RETRY_EINTR_RC(rc_send, ::sendmsg(socket_, &hdr, flags));
      if ((rc_send < 0) && (errno != EWOULDBLOCK) && (errno != EAGAIN))
      {
	const int last_error = errno;
	guard.unlock();
	if (cb)
	{
	  cb(last_error);
	}
	return NO_REQ_ID;
      }
      else if (rc_send >= 0)
      {
	guard.unlock();
	if (cb)
	{
	  cb(0);
	}
	return NO_REQ_ID;
      }
    }
  }

  const size_t data_len = std::accumulate(iov, iov + iov_len, 0,
					  accumulate_iov_len);

  char * const copied_data = new char[data_len + clen];
  std::for_each(iov, iov + iov_len, copy_iovec_data(copied_data));
  if (clen)
  {
    std::copy(cdata, cdata + clen, copied_data + data_len);
  }

  const req_id_t req_id = send_req_id_ + send_queue_.size();
  send_queue_.push_back(send_request(copied_data, data_len, clen, flags, cb));

#if defined(HAVE_EPOLL)
  if (!send_events_)
  {
    guard.unlock();

    std::lock_guard<std::mutex> recv_guard(recv_queue_sync_);
    guard.lock();

    if (!send_events_ && !send_queue_.empty())
    {
      send_events_ = true;
      handler_.register_events(*this, recv_events_, send_events_);
    }
  }
#elif defined(HAVE_KQUEUE)
  if (!send_events_)
  {
    send_events_ = true;
    handler_.register_send(*this);
  }
#endif

  return req_id;
}


Async_Socket::req_id_t Async_Socket::sendmsg(
  const char *data, size_t len, const char *cdata, size_t clen, int flags,
  const Async_Handler::sendmsg_handler &cb)
{
  if (len == 0)
  {
    if (cb)
    {
      cb(EINVAL);
    }
    return NO_REQ_ID;
  }

  struct iovec iov;
  iov.iov_base = const_cast<char *>(data);
  iov.iov_len = len;

  return sendmsg(&iov, 1, cdata, clen, flags, cb);
}


Async_Socket::req_id_t Async_Socket::recv(
  char *buffer, size_t len, const Async_Handler::recv_handler &cb, int flags)
{
  if (len == 0)
  {
    cb(EINVAL, 0);
    return NO_REQ_ID;
  }

  std::unique_lock<std::mutex> guard(recv_queue_sync_);
  if (auto rguard = handler_.recurse())
  {
    if (recv_queue_.empty())
    {
      int rc_recv;
      RETRY_EINTR_RC(rc_recv, ::recv(socket_, buffer, len, flags));
      if ((rc_recv < 0) && (errno != EWOULDBLOCK) && (errno != EAGAIN))
      {
	const int last_error = errno;
	guard.unlock();
	cb(last_error, 0);
	return NO_REQ_ID;
      }
      else if (rc_recv >= 0)
      {
	guard.unlock();
	cb(0, rc_recv);
	return NO_REQ_ID;
      }
    }
  }

  const req_id_t req_id = recv_req_id_ + recv_queue_.size();
  recv_queue_.push_back(recv_request(buffer, len, flags, cb));

#if defined(HAVE_EPOLL)
  if (!recv_events_)
  {
    std::lock_guard<std::mutex> send_guard(send_queue_sync_);
    recv_events_ = true;
    handler_.register_events(*this, recv_events_, send_events_);
  }
#elif defined(HAVE_KQUEUE)
  if (!recv_events_)
  {
    recv_events_ = true;
    handler_.register_recv(*this);
  }
#endif

  return req_id;
}

Async_Socket::req_id_t Async_Socket::recvmsg(
  char *buffer, size_t len, char *cbuffer, size_t clen,
  const Async_Handler::recvmsg_handler &cb, int flags)
{
  if (len == 0)
  {
    cb(EINVAL, 0, 0, 0);
    return NO_REQ_ID;
  }

  std::unique_lock<std::mutex> guard(recv_queue_sync_);
  if (auto rguard = handler_.recurse())
  {
    if (recv_queue_.empty())
    {
      struct msghdr hdr;
      struct iovec iov;

      iov.iov_base = buffer;
      iov.iov_len = len;
      hdr.msg_name = NULL;
      hdr.msg_namelen = 0;
      hdr.msg_iov = &iov;
      hdr.msg_iovlen = 1;
      hdr.msg_control = cbuffer;
      hdr.msg_controllen = clen;
      hdr.msg_flags = 0;

      int rc_recv;
      RETRY_EINTR_RC(rc_recv, ::recvmsg(socket_, &hdr, flags));
      if ((rc_recv < 0) && (errno != EWOULDBLOCK) && (errno != EAGAIN))
      {
	const int last_error = errno;
	guard.unlock();
	cb(last_error, 0, 0, 0);
	return NO_REQ_ID;
      }
      else if (rc_recv >= 0)
      {
	guard.unlock();
	cb(0, rc_recv, hdr.msg_controllen, hdr.msg_flags);
	return NO_REQ_ID;
      }
    }
  }

  const req_id_t req_id = recv_req_id_ + recv_queue_.size();
  recv_queue_.push_back(recv_request(buffer, len, cbuffer, clen, flags, cb));

#if defined(HAVE_EPOLL)
  if (!recv_events_)
  {
    std::lock_guard<std::mutex> send_guard(send_queue_sync_);
    recv_events_ = true;
    handler_.register_events(*this, recv_events_, send_events_);
  }
#elif defined(HAVE_KQUEUE)
  if (!recv_events_)
  {
    recv_events_ = true;
    handler_.register_recv(*this);
  }
#endif

  return req_id;
}


Async_Socket::req_id_t Async_Socket::connect(
  const struct sockaddr * addr, socklen_t addr_len,
  const Async_Handler::connect_handler &cb)
{
  int rc_connect;
  RETRY_EINTR_RC(rc_connect, ::connect(socket_, addr, addr_len));
  if ((rc_connect < 0) && (errno == EINPROGRESS))
  {
    connect_addr_.assign(reinterpret_cast<const unsigned char *>(addr),
			 reinterpret_cast<const unsigned char *>(addr) + addr_len);
    connect_cb_ = cb;
  }
  else if (rc_connect != 0)
  {
    const int last_error = errno;
    cb(last_error);
    return NO_REQ_ID;
  }
  else
  {
    cb(0);
    return NO_REQ_ID;
  }

#if defined(HAVE_EPOLL)
  send_events_ = true;
  handler_.register_events(*this, recv_events_, send_events_);
#elif defined(HAVE_KQUEUE)
  send_events_ = true;
  handler_.register_send(*this);
#endif

  return req_id_t();
}

bool Async_Socket::cancel_recv(req_id_t req_id)
{
  if (req_id == NO_REQ_ID)
  {
    return false;
  }

  std::unique_lock<std::mutex> guard(recv_queue_sync_);

  const req_id_t offset = req_id - recv_req_id_;
  const recv_queue_t::iterator iter = (offset < recv_queue_.size()) ?
    (recv_queue_.begin() + offset) : recv_queue_.end();

  if ((iter != recv_queue_.end()) && !iter->cancelled)
  {
    TRACE(logger.debug(), "cancel_recv fd=" << socket_ << ", id=" << req_id);

    const recv_request::Msg_Type req_typ = iter->typ;
    std::unique_ptr<Async_Handler::recv_handler> recv_cb(
      (req_typ == recv_request::Msg_Recv) ? iter->u.recv_cb : 0);
    std::unique_ptr<Async_Handler::recvmsg_handler> recvmsg_cb(
      (req_typ == recv_request::Msg_RecvMsg) ? iter->u.recvmsg_cb : 0);

    if (iter == recv_queue_.begin())
    {
      recv_req_id_ += pop_front_and_cancelled(recv_queue_);
    }
    else
    {
      iter->cancelled = true;
    }

    guard.unlock();

    if (req_typ == recv_request::Msg_Recv)
    {
      (*recv_cb)(ECANCELED, 0);
    }
    else if (req_typ == recv_request::Msg_RecvMsg)
    {
      (*recvmsg_cb)(ECANCELED, 0, 0, 0);
    }

    return true;
  }
  else
  {
    TRACE(logger.debug(), "cancel_recv fd=" << socket_ << ", id=" << req_id
	  << ' '
	  << ((iter == recv_queue_.end()) ? "not found" : "already cancelled")
	  << " in [" << recv_req_id_ << '-'
	  << (recv_req_id_ + recv_queue_.size()) << ')');
  }

  return false;
}

bool Async_Socket::cancel_send(req_id_t req_id)
{
  if (req_id == NO_REQ_ID)
  {
    return false;
  }

  std::unique_lock<std::mutex> guard(send_queue_sync_);

  const req_id_t offset = req_id - send_req_id_;
  const send_queue_t::iterator iter = (offset < send_queue_.size()) ?
    (send_queue_.begin() + offset) : send_queue_.end();

  if ((iter != send_queue_.end()) && !iter->cancelled)
  {
    TRACE(logger.debug(), "cancel_send fd=" << socket_ << ", id=" << req_id);

    std::unique_ptr<Async_Handler::send_handler> send_cb(iter->cb);

    if (iter == send_queue_.begin())
    {
      send_req_id_ += pop_front_and_cancelled(send_queue_);
    }
    else
    {
      iter->cancelled = true;
    }

    guard.unlock();

    if (send_cb)
    {
      (*send_cb)(ECANCELED);
    }
    return true;
  }
  else
  {
    TRACE(logger.debug(), "cancel_send fd=" << socket_ << ", id=" << req_id
	  << ' '
	  << ((iter == send_queue_.end()) ? "not found" : "already cancelled")
	  << " in [" << send_req_id_ << '-'
	  << (send_req_id_ + send_queue_.size()) << ')');
  }

  return false;
}

bool Async_Socket::cancel_connect(req_id_t req_id)
{
  if (req_id == NO_REQ_ID)
  {
    return false;
  }

  return false;
}

void Async_Socket::close()
{
  {
    std::lock_guard<std::mutex> guard(send_queue_sync_);
    send_queue_.clear();
  }

  {
    std::lock_guard<std::mutex> guard(recv_queue_sync_);
    recv_queue_.clear();
  }

  if (socket_ != -1)
  {
    RETRY_EINTR(::close(socket_));
    socket_ = -1;
  }
}


void Async_Socket::on_send() const
{
  TRACE(logger.debug(), "on_send fd=" << socket_);
  bool done = false;
  bool had_some_work = false;

  while (!done)
  {
    std::unique_lock<std::mutex> guard(send_queue_sync_);

    if (!connect_addr_.empty())
    {
      int rc_connect;
      RETRY_EINTR_RC(rc_connect,
		     ::connect(socket_,
			       reinterpret_cast<const struct sockaddr *>(connect_addr_.data()),
			       connect_addr_.size()));
      if (rc_connect &&
	  ((errno == EALREADY) || (errno == EINPROGRESS)))
      {
	done = true;
	break;
      }
      else
      {
	const int last_error = errno;
	connect_addr_.clear();
	auto cb (std::move(connect_cb_));
	guard.unlock();

	cb(rc_connect ? last_error : 0);
	continue;
      }
    }

    if (!send_queue_.empty())
    {
      had_some_work = true;
      send_request &req(send_queue_.front());

      int rc_send;
      if (req.typ == send_request::Msg_Send)
      {
	RETRY_EINTR_RC(rc_send,
		       ::send(socket_, req.data + req.offset,
			      req.len - req.offset, req.flags));
      }
      else if (req.typ == send_request::Msg_SendMsg)
      {
	struct msghdr hdr;
	struct iovec iov;

	iov.iov_base = const_cast<char *>(req.data + req.offset);
	iov.iov_len = req.len - req.offset;
	hdr.msg_name = NULL;
	hdr.msg_namelen = 0;
	hdr.msg_iov = &iov;
	hdr.msg_iovlen = 1;
	hdr.msg_control = const_cast<char *>(req.data + req.clen);
	hdr.msg_controllen = req.clen;
	hdr.msg_flags = req.flags;

	RETRY_EINTR_RC(rc_send, ::sendmsg(socket_, &hdr, req.flags));
      }
      else
      {
	TRACE(logger.fatal(), "invalid request type " << req.typ);
	rc_send = -1;
      }

      if (rc_send < 0)
      {
	if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
	{
	  done = true;
	}
	else
	{
	  const int last_error = errno;
	  TRACE(logger.error(), "on_send: send(), errno=" << last_error);

	  delete[] req.data;
	  const std::unique_ptr<Async_Handler::send_handler> callback(req.cb);

	  send_req_id_ += pop_front_and_cancelled(send_queue_);
	  done = send_queue_.empty();
	  guard.unlock();

	  if (callback)
	  {
	    (*callback)(last_error);
	  }
	}
      }
      else
      {
	req.offset += rc_send;

	if (req.offset >= req.len)
	{
	  delete[] req.data;
	  const std::unique_ptr<Async_Handler::send_handler> callback(req.cb);

	  send_req_id_ += pop_front_and_cancelled(send_queue_);
	  done = send_queue_.empty();
	  guard.unlock();

	  if (callback)
	  {
	    (*callback)(0);
	  }
	}
	else
	{
	  done = true;
	}
      }
    }
    else
    {
      if (! had_some_work && send_events_)
      {
	// this appears to have been a spurious wake-up, so disable
	// further events
#if defined(HAVE_EPOLL)
	guard.unlock();

	std::lock_guard<std::mutex> recv_guard(recv_queue_sync_);
	guard.lock();

	if (send_events_ && send_queue_.empty())
	{
	  send_events_ = false;
	  handler_.register_events(*this,
				   recv_events_, send_events_);
	}
#elif defined(HAVE_KQUEUE)
	send_events_ = false;
	handler_.register_send(*this, false);
#endif
      }

      done = true;
    }
  }
}


void Async_Socket::on_recv() const
{
  TRACE(logger.debug(), "on_recv fd=" << socket_);
  bool done = false;

  while (!done)
  {
    std::unique_lock<std::mutex> guard(recv_queue_sync_);

    if (!recv_queue_.empty())
    {
      recv_request &req(recv_queue_.front());

      int rc_recv;
      int recv_clen, recv_flags;

      if (req.typ == recv_request::Msg_Recv)
      {
	RETRY_EINTR_RC(rc_recv, ::recv(socket_, req.buffer, req.len, req.flags));
      }
      else if (req.typ == recv_request::Msg_RecvMsg)
      {
	struct msghdr hdr;
	struct iovec iov;

	iov.iov_base = req.buffer;
	iov.iov_len = req.len;
	hdr.msg_name = NULL;
	hdr.msg_namelen = 0;
	hdr.msg_iov = &iov;
	hdr.msg_iovlen = 1;
	hdr.msg_control = req.cbuffer;
	hdr.msg_controllen = req.clen;
	hdr.msg_flags = req.flags;

	RETRY_EINTR_RC(rc_recv, ::recvmsg(socket_, &hdr, req.flags));

	recv_clen = hdr.msg_controllen;
	recv_flags = hdr.msg_flags;
      }
      else
      {
	TRACE(logger.fatal(), "invalid request type " << req.typ);
	rc_recv = -1;
      }

      if (rc_recv < 0)
      {
	if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
	{
	  done = true;
	}
	else
	{
	  const int last_error = errno;
	  TRACE(logger.error(), "on_recv: recv(), errno=" << last_error);

	  const recv_request::Msg_Type req_typ = req.typ;
	  std::unique_ptr<Async_Handler::recv_handler> recv_cb(
	    (req_typ == recv_request::Msg_Recv) ? req.u.recv_cb : 0);
	  std::unique_ptr<Async_Handler::recvmsg_handler> recvmsg_cb(
	    (req_typ == recv_request::Msg_RecvMsg) ? req.u.recvmsg_cb : 0);

	  recv_req_id_ += pop_front_and_cancelled(recv_queue_);
	  done = recv_queue_.empty();
	  guard.unlock();

	  if (req_typ == recv_request::Msg_Recv)
	  {
	    (*recv_cb)(last_error, 0);
	  }
	  else if (req_typ == recv_request::Msg_RecvMsg)
	  {
	    (*recvmsg_cb)(last_error, 0, 0, 0);
	  }
	}
      }
      else
      {
	done = (rc_recv < req.len);

	const recv_request::Msg_Type req_typ = req.typ;
	std::unique_ptr<Async_Handler::recv_handler> recv_cb(
	  (req_typ == recv_request::Msg_Recv) ? req.u.recv_cb : 0);
	std::unique_ptr<Async_Handler::recvmsg_handler> recvmsg_cb(
	  (req_typ == recv_request::Msg_RecvMsg) ? req.u.recvmsg_cb : 0);

	recv_req_id_ += pop_front_and_cancelled(recv_queue_);
	done = recv_queue_.empty();
	guard.unlock();

	if (req_typ == recv_request::Msg_Recv)
	{
	  (*recv_cb)(0, rc_recv);
	}
	else if (req_typ == recv_request::Msg_RecvMsg)
	{
	  (*recvmsg_cb)(0, rc_recv, recv_clen, recv_flags);
	}
      }
    }
    else
    {
      done = true;
    }
  }
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
