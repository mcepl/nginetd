/*	-*- C++ -*-
 * Copyright (C) 2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../fiber_handler.h"

#include <functional>

#include <ucontext.h>


void fiber_handler_cleanup_fiber(void * const context)
{
  ucontext_t * const context_cleanup = static_cast<ucontext_t *>(context);

  while (true)
  {
    ucontext_t *const context_main =
      static_cast<ucontext_t *>(Fiber_Handler::main_fiber());
    ucontext_t *const context_curr =
      static_cast<ucontext_t *>(Fiber_Handler::current_fiber());

    Fiber_Handler::current_fiber(context_main);

    delete[] static_cast<char *>(context_curr->uc_stack.ss_sp);
    delete context_curr;

    swapcontext(context_cleanup, context_main);
  }
}

// ugly workaround for broken 64-bit makecontext
extern "C" void cleanup_fiber(unsigned int param1, unsigned int param2)
{
  const unsigned long context_ptr =
    (static_cast<unsigned long>(param2) << 32) |
    static_cast<unsigned long>(param1);
  void *const context = reinterpret_cast<void *>(context_ptr);

  fiber_handler_cleanup_fiber(context);
}

// ugly workaround for broken 64-bit makecontext
extern "C" void start_fiber_wrapper(unsigned int param1, unsigned int param2)
{
  const unsigned long func_ptr =
    (static_cast<unsigned long>(param2) << 32) |
    static_cast<unsigned long>(param1);
  const std::function<void ()> &func =
    *reinterpret_cast<const std::function<void ()> *>(func_ptr);

  func();

  Fiber_Handler::destroy_fiber();
}

std::function<void (std::function<void ()> const &)> Fiber_Handler::get_wrapper()
{
  return [] (std::function<void ()> const & func)
  {
    const size_t cleanup_stack_size = 2048;
    char cleanup_stack[cleanup_stack_size];

    ucontext_t context_main, context_cleanup;
    main_fiber(&context_main);
    current_fiber(&context_main);

    getcontext(&context_cleanup);
    context_cleanup.uc_stack.ss_sp = cleanup_stack;
    context_cleanup.uc_stack.ss_size = sizeof(cleanup_stack);
    context_cleanup.uc_link = &context_main;

    context_main.uc_link = &context_cleanup;

    TRACE(logger.debug(), "worker_thread "
	  << "context_main=" << static_cast<void *>(&context_main) << ", "
	  << "context_cleanup=" << static_cast<void *>(&context_cleanup));

    // ugly workaround for broken 64-bit makecontext
    const unsigned long context_cleanup_ptr =
      reinterpret_cast<unsigned long>(static_cast<void *>(&context_cleanup));

    makecontext(&context_cleanup, (void (*)()) cleanup_fiber, 2,
		static_cast<unsigned int>(context_cleanup_ptr & 0xffffffffUL),
		static_cast<unsigned int>(context_cleanup_ptr >> 32));

    func();

    main_fiber(NULL);
  };
}



__thread void *Fiber_Handler::main_fiber_ = 0;
__thread void *Fiber_Handler::current_fiber_ = 0;


void Fiber_Handler::yield()
{
  ucontext_t *const context_main = static_cast<ucontext_t *>(main_fiber());
  ucontext_t *const context_curr = static_cast<ucontext_t *>(current_fiber());

  current_fiber(context_main);
  swapcontext(context_curr, context_main);
}

void Fiber_Handler::schedule_fiber(void *fiber)
{
  ucontext_t *const context_curr =
    static_cast<ucontext_t *>(current_fiber());
  if (context_curr != fiber)
  {
    current_fiber(fiber);
    swapcontext(context_curr, static_cast<ucontext_t *>(fiber));
  }
}

void Fiber_Handler::create_fiber(const std::function<void ()> &func,
				 const size_t stack_size)
{
  TRACE(logger.debug(), "creating fiber, stack size=" << stack_size);

  ucontext_t *context_main = static_cast<ucontext_t *>(main_fiber());
  if (! context_main)
  {
    const size_t cleanup_stack_size = 2048;
    char *cleanup_stack = new char[cleanup_stack_size];

    ucontext_t *context_cleanup = new ucontext_t;
    context_main = new ucontext_t;
    main_fiber_ = context_main;

    getcontext(context_cleanup);
    context_cleanup->uc_stack.ss_sp = cleanup_stack;
    context_cleanup->uc_stack.ss_size = cleanup_stack_size;
    context_cleanup->uc_link = context_main;

    context_main->uc_link = context_cleanup;

    // ugly workaround for broken 64-bit makecontext
    const unsigned long context_cleanup_ptr =
      reinterpret_cast<unsigned long>(static_cast<void *>(context_cleanup));

    makecontext(context_cleanup, (void (*)()) cleanup_fiber, 2,
		static_cast<unsigned int>(context_cleanup_ptr & 0xffffffffUL),
		static_cast<unsigned int>(context_cleanup_ptr >> 32));
  }

  ucontext_t *context_curr = static_cast<ucontext_t *>(current_fiber());
  if (! context_curr)
  {
    context_curr = context_main;
  }

  ucontext_t *const context = new ucontext_t;
  getcontext(context);
  context->uc_stack.ss_sp = new char[stack_size];
  context->uc_stack.ss_size = stack_size;
  context->uc_link = context_main->uc_link;

  // ugly workaround for broken 64-bit makecontext
  const unsigned long func_ptr =
    reinterpret_cast<unsigned long>(static_cast<const void *>(&func));

  makecontext(context, (void (*)()) start_fiber_wrapper, 2,
	      static_cast<unsigned int>(func_ptr & 0xffffffffUL),
	      static_cast<unsigned int>(func_ptr >> 32));

  current_fiber(context);
  swapcontext(context_curr, context);
}

void Fiber_Handler::destroy_fiber()
{
  // nothing to do
  TRACE(logger.debug(), "destroying fiber");
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
