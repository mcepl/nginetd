/*	-*- C++ -*-
 * Copyright (C) 2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ASYNC_TLS__H
#define INCLUDED__ASYNC_TLS__H

#include "trace.h"

#include "async_socket.h"

#include <gnutls/gnutls.h>

#include <functional>
#include <memory>
#include <mutex>

extern "C" ssize_t async_tls_push(gnutls_transport_ptr_t data, const void *buf,
				  size_t len);
extern "C" ssize_t async_tls_pull(gnutls_transport_ptr_t data, void *buf,
				  size_t len);

class Async_Tls
{
  static Trace_Stream logger;

  friend ssize_t async_tls_push(gnutls_transport_ptr_t data,
				const void *buf, size_t len);
  friend ssize_t async_tls_pull(gnutls_transport_ptr_t data,
				void *buf, size_t len);

public:
  typedef Async_Socket::req_id_t req_id_t;
  static const req_id_t NO_REQ_ID = Async_Socket::NO_REQ_ID;


  explicit Async_Tls(std::unique_ptr<Async_Socket> &&sock);
  ~Async_Tls();

  Async_Tls(Async_Tls const &) = delete;
  Async_Tls &operator =(Async_Tls const &) = delete;

public:
  req_id_t handshake(const std::function<void (int)> &cb);

  req_id_t send(const char *data, size_t len, int flags = 0,
		const Async_Handler::send_handler &cb = Async_Handler::send_handler());

  req_id_t sendmsg(const struct iovec *iov, int iov_len, int flags = 0,
		   const Async_Handler::send_handler &cb = Async_Handler::send_handler());

  inline req_id_t sendmsg(const struct iovec *iov, int iov_len,
			  const Async_Handler::send_handler &cb = Async_Handler::send_handler())
  { return sendmsg(iov, iov_len, 0, cb); }

  req_id_t recv(char *buffer, size_t len,
		const Async_Handler::recv_handler &cb = Async_Handler::recv_handler());

  req_id_t shutdown(int how);

  std::unique_ptr<Async_Socket> detach();

protected:
  ssize_t async_push(const char *data, size_t len);

  ssize_t async_pull(char *buffer, size_t len);

private:
  struct recv_request
  {
    explicit
    recv_request(char * const buffer, const size_t len, const int flags,
		 const std::function<void (int, size_t)> &cb)
      : cancelled(false),
	buffer(buffer), len(len),
	flags(flags),
	recv_cb(cb)
    { }

    bool cancelled;
    char * const buffer;
    const size_t len;
    const int flags;

    std::function<void (int, size_t)> recv_cb;
  };

  typedef std::deque<recv_request> recv_queue_t;

  std::unique_ptr<Async_Socket> sock_;
  gnutls_session_t session_;

  std::mutex sync_;
  bool in_handshake_{ false };
  std::function<void (int)> handshake_cb_;

  char buffer_[8192];
  size_t buffer_len_{ 0 };
  size_t buffer_offset_{ 0 };
  bool buffer_end_{ false };
  recv_queue_t recv_queue_;
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
