/*	-*- C++ -*-
 * Copyright (C) 2003-2010, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "address_info.h"
#include "async_handler.h"
#include "async_timer.h"
#include "dnsbl_resolver.h"
#include "fiber_resolver.h"
#include "fiber_socket.h"
#include "reverse_resolver.h"
#include "socket.h"
#include "socket_stream.h"
#include "trace.h"
#include "msg_dispatcher.h"
#include "nginetd.h"

#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include <algorithm>
#include <cstring>
#include <list>
#include <map>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

#if defined(_WIN32)
#include <io.h>
#else
#include <unistd.h>
#include <sys/mman.h>
#include <sys/un.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#if defined(_WIN32)
#include <winsock2.h>
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

#include <libguile.h>


#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }

namespace msg = nginetd::msg;


struct smob_ip_address
{
  union
  {
    in_addr ipv4;
    in6_addr ipv6;
  } u;

  int af;
};

namespace
{
scm_t_bits ip_address_tag;

SCM make_ip_address(const int af, const char * const s)
{
  smob_ip_address *ipaddr =
    static_cast<smob_ip_address *>(scm_gc_malloc (sizeof(smob_ip_address),
						  "ip-address"));
  ipaddr->af = af;
  if (inet_pton(af, s, &ipaddr->u.ipv4))
  {
    SCM smob;
    SCM_NEWSMOB(smob, ip_address_tag, ipaddr);

    return smob;
  }
  else
  {
    scm_gc_free(ipaddr, sizeof(smob_ip_address), "ip-address");
    return SCM_EOL;
  }
}

SCM make_ip_address(const struct in_addr * const addr)
{
  smob_ip_address *ipaddr =
    static_cast<smob_ip_address *>(scm_gc_malloc (sizeof(smob_ip_address),
						  "ip-address"));
  ipaddr->af = AF_INET;
  std::memcpy(&ipaddr->u.ipv4, addr, sizeof(*addr));

  SCM smob;
  SCM_NEWSMOB(smob, ip_address_tag, ipaddr);
  return smob;
}

SCM make_ip_address(const struct in6_addr * const addr)
{
  smob_ip_address *ipaddr =
    static_cast<smob_ip_address *>(scm_gc_malloc (sizeof(smob_ip_address),
						  "ip-address"));
  ipaddr->af = AF_INET6;
  std::memcpy(&ipaddr->u.ipv6, addr, sizeof(*addr));

  SCM smob;
  SCM_NEWSMOB(smob, ip_address_tag, ipaddr);
  return smob;
}

extern "C" SCM make_ipv4_address (SCM s)
{
  const char * const addr_str = scm_to_locale_string(s);
  SCM smob = make_ip_address(AF_INET, addr_str);
  free(const_cast<char *>(addr_str));

  return smob;
}

extern "C" SCM make_ipv6_address (SCM s)
{
  const char * const addr_str = scm_to_locale_string(s);
  SCM smob = make_ip_address(AF_INET6, addr_str);
  free(const_cast<char *>(addr_str));

  return smob;
}

extern "C" SCM is_ipv4_address(SCM s)
{
  scm_assert_smob_type(ip_address_tag, s);

  smob_ip_address *ipaddr =
    reinterpret_cast<smob_ip_address *>(SCM_SMOB_DATA(s));
  return (ipaddr->af == AF_INET) ? SCM_BOOL_T : SCM_BOOL_F;
}

extern "C" SCM is_ipv6_address(SCM s)
{
  scm_assert_smob_type(ip_address_tag, s);

  smob_ip_address *ipaddr =
    reinterpret_cast<smob_ip_address *>(SCM_SMOB_DATA(s));
  return (ipaddr->af == AF_INET6) ? SCM_BOOL_T : SCM_BOOL_F;
}

void *init_scripting()
{
  ip_address_tag = scm_make_smob_type("ip-address", sizeof(smob_ip_address));

  scm_debug_options(scm_list_2(scm_from_locale_symbol("stack"),
			       scm_from_int(0)));

  return NULL;
}


extern "C" void *with_guile_helper(void *param)
{
  const boost::function0<void *> &func =
    *static_cast<boost::function0<void *> *>(param);

  return func();
}

inline void *with_guile(const boost::function0<void *> &func)
{
  return scm_with_guile(with_guile_helper,
			const_cast<void *>(static_cast<const void *>(&func)));
}


extern "C" void init_module(void *param)
{
  const char * const modname = static_cast<char *>(param);

  scm_c_define_gsubr("make-ipv4-address", 1, 0, 0,
		     reinterpret_cast<SCM (*)()>(&make_ipv4_address));
  scm_c_define_gsubr("make-ipv6-address", 1, 0, 0,
		     reinterpret_cast<SCM (*)()>(&make_ipv6_address));
  scm_c_define_gsubr("is-ipv4-address", 1, 0, 0,
		     reinterpret_cast<SCM (*)()>(&is_ipv4_address));
  scm_c_define_gsubr("is-ipv6-address", 1, 0, 0,
		     reinterpret_cast<SCM (*)()>(&is_ipv6_address));

  scm_c_primitive_load(modname);
}
}

class Inetd_Server
{
 private:
  class Client;

  static Trace_Stream logger;
  static const unsigned int buffer_size = 4096;

  Async_Handler &handler_;
  Fiber_Resolver resolver_;
  const socket_t listener_sock_;
  const std::string sockname_;

 protected:
  inline Async_Handler &handler()
  {
    return handler_;
  }

 public:
  Inetd_Server(Async_Handler &handler, const char * const sockname)
    : handler_(handler),
      sockname_(sockname),
      listener_sock_(::socket(PF_UNIX, SOCK_SEQPACKET, 0)),
      resolver_(handler_)
  {
    // set up TCP/IP listener socket
#if defined(_WIN32)
    if (listener_sock_ != INVALID_SOCKET)
#else
    if (listener_sock_ != -1)
#endif
    {
      ::unlink(sockname_.c_str());

      struct sockaddr_un addr;
      addr.sun_family = AF_UNIX;
      strcpy(addr.sun_path, sockname);
      const int res_bind =
	::bind(listener_sock_, (struct sockaddr *) &addr, SUN_LEN(&addr));
      if (!res_bind)
      {
	handler_.attach_listener(
	  listener_sock_, boost::bind(&Inetd_Server::on_accept, this, _1, _2));
      }
      else
      {
	TRACE(logger.error(), "bind failed");
	RETRY_EINTR(::close(listener_sock_));
      }
    }
    else
    {
      TRACE(logger.error(), "listener socket creation failed");
    }
  }

  ~Inetd_Server()
  {
    if (listener_sock_ != -1)
    {
      TRACE(logger.debug(), "cleaning up listener socket " << listener_sock_);
      handler_.detach_listener(listener_sock_);
      RETRY_EINTR(::close(listener_sock_));
      ::unlink(sockname_.c_str());
    }
  }


 protected:
  void on_accept(const socket_t sock, const unsigned long error)
  {
    TRACE(logger.debug(), "on_accept(error=" << error << ')');

    if (!error)
    {
      handler_.create_fiber(boost::bind(&Inetd_Server::handle_connection, this,
					new Fiber_Socket(handler_, sock)));
    }
  }

  void handle_connection(Fiber_Socket * const _sock);
};


class Inetd_Server::Client
{
 private:
  typedef void (Client::*MSG_HANDLER)(const char *, size_t);
  typedef ::Msg_Dispatcher<uint16_t, MSG_HANDLER> Msg_Dispatcher;

  static Msg_Dispatcher::Entry dispatch_table[];
  static const Msg_Dispatcher dispatcher;
  static Trace_Stream logger;

  Inetd_Server &server_;
  Fiber_Socket &sock_;

  std::list<socket_t> listeners_;


 public:
  inline Client(Inetd_Server &server, Fiber_Socket &sock)
    : server_(server), sock_(sock)
  { }

  ~Client()
  {
    TRACE(logger.debug(), "cleaning up listener sockets");
    while (!listeners_.empty())
    {
      const socket_t s = listeners_.back();
      listeners_.pop_back();

      server_.handler_.detach_listener(s);
      RETRY_EINTR(::close(s));
    }
  }


  void operator ()()
  {
#if defined(__linux__)
    struct ucred peer_cred;
    socklen_t crlen = sizeof(peer_cred);
    const int rc_peercred =
      sock_.getsockopt(SOL_SOCKET, SO_PEERCRED, &peer_cred, &crlen);

    const uid_t peer_uid = (rc_peercred == 0) ? peer_cred.uid : -1;
    const gid_t peer_gid = (rc_peercred == 0) ? peer_cred.gid : -1;

    TRACE(logger.debug(), "Peer's uid=" << peer_uid
	  << ", gid=" << peer_gid);
#endif

    union
    {
      void *ptr;
      char buffer[256];
    } data;

    union
    {
      struct cmsghdr cmsg;
      char buffer[CMSG_SPACE(sizeof(int))];
    } cdata;

    int flags = 0;

    try
    {
      while (true)
      {
	size_t len = sizeof(data.buffer);
	size_t clen = sizeof(cdata.buffer);
	const int rc_recvmsg =
	  sock_.recvmsg(data.buffer, len, cdata.buffer, clen, flags);
	TRACE(logger.debug(), "recvmsg returned rc=" << rc_recvmsg
	      << ", len=" << len << ", clen=" << clen
	      << ", flags=" << flags);

	if (len == 0)
	{
	  // connection closed
	  break;
	}

	const msg::Header &hdr = msg::Header_Proxy::get(data.buffer, len);
	(this->*dispatcher(hdr.msg_type()))(data.buffer, len);
      }
    }
    catch (const std::invalid_argument &iaexc)
    {
      TRACE(logger.error(), "got invalid argument exception");
    }

    sock_.shutdown(SHUT_RDWR);
  }

 protected:
  class Tcp_Connection
  {
   private:
    static Trace_Stream logger;

    Client &client_;
    const SCM module_;
    Fiber_Socket &sock_;
    char buffer_[4096];

   public:
    Tcp_Connection(Client &client, const SCM module,
		   Fiber_Socket &sock)
      : client_(client), module_(module), sock_(sock)
    { }

    ~Tcp_Connection()
    { }


    static void *guile_return_list(const SCM module,
				   const char * const lname,
				   const SCM arg,
				   std::list<std::string> &lst)
    {
      SCM func = scm_variable_ref(scm_c_module_lookup(module, lname));
      SCM l = scm_call_1(func, arg);
      while (!scm_is_null(l))
      {
	SCM elem = scm_car(l);

	const char * const listname = scm_to_locale_string(elem);

	lst.push_back(listname);
	free(const_cast<char *>(listname));

	l = scm_cdr(l);
      }

      return 0;
    }

    static void *guile_dnsbl_ipv4(const SCM module,
				  const struct in_addr * const addr,
				  std::list<std::string> &dnsbl)
    {
      return guile_return_list(module, "dnsbl", make_ip_address(addr), dnsbl);
    }

    static void *guile_dnsbl_ipv6(const SCM module,
				  const struct in6_addr * const addr,
				  std::list<std::string> &dnsbl)
    {
      return guile_return_list(module, "dnsbl", make_ip_address(addr), dnsbl);
    }

    static void *guile_check_acl(const SCM module,
				 const std::map<std::string, std::list<std::string> > &dnsbl,
				 std::string &errmsg)
    {
      TRACE(logger.debug(), "invoking check-acl");

      SCM check_acl =
	scm_variable_ref(scm_c_module_lookup(module, "check-acl"));
      SCM bl = scm_c_make_hash_table(31);

      for (std::map<std::string, std::list<std::string> >::const_iterator iter =
	     dnsbl.begin();
	   iter != dnsbl.end();
	   ++iter)
      {
	SCM l = SCM_EOL;
	for (std::list<std::string>::const_reverse_iterator liter =
	       iter->second.rbegin();
	     liter != iter->second.rend();
	     ++liter)
	{
	  SCM value = scm_from_locale_stringn(liter->data(), liter->length());
	  l = scm_cons(value, l);
	}

	SCM key = scm_from_locale_stringn(iter->first.data(),
					  iter->first.length());

	scm_hashq_set_x(bl, key, l);
      }

      SCM res = scm_call_1(check_acl, bl);
      if (scm_is_string(res))
      {
	TRACE(logger.debug(), "guile_check_acl returned string");

	size_t errlen = 0;
	const char * const errstr = scm_to_locale_stringn(res, &errlen);
	errmsg.assign(errstr, errlen);
	return reinterpret_cast<void *>(1);
      }

      return NULL;
    }


    void operator ()()
    {
      Fiber_Resolver &resolver = client_.server_.resolver_;

      struct sockaddr_storage addr_storage;
      struct sockaddr &addr =
	reinterpret_cast<struct sockaddr &>(addr_storage);
      socklen_t addr_len = sizeof(addr_storage);
      const int rc_peername = sock_.getpeername(&addr, &addr_len);
      if (!rc_peername)
      {
	const bool is_ipv4_mapped =
	  (addr.sa_family == AF_INET6) &&
	  IN6_IS_ADDR_V4MAPPED(&reinterpret_cast<const sockaddr_in6 &>(addr).sin6_addr);

	const struct in6_addr * const addr_ipv6 =
	  (addr.sa_family == AF_INET6) ?
	  &reinterpret_cast<const struct sockaddr_in6 &>(addr).sin6_addr : NULL;
	const struct in_addr * const addr_ipv4 =
	  is_ipv4_mapped ?
	  reinterpret_cast<const struct in_addr *>(&addr_ipv6->s6_addr[12]) :
	  ((addr.sa_family == AF_INET) ?
	   &reinterpret_cast<const struct sockaddr_in &>(addr).sin_addr : NULL);

	TRACE(logger.debug(), "operator () ipv4=" << (addr_ipv4 != NULL)
	      << ", ipv6=" << (addr_ipv6 != NULL));

	Dnsbl_Resolver dnsbl_res(resolver.async(), client_.server_.handler_);
	if (addr_ipv4)
	{
	  std::list<std::string> dnsbl;
	  with_guile(boost::bind(&Tcp_Connection::guile_dnsbl_ipv4,
				 module_, addr_ipv4, boost::ref(dnsbl)));

	  for (std::list<std::string>::const_iterator iter = dnsbl.begin();
	       iter != dnsbl.end();
	       ++iter)
	  {
	    const std::string &blname = *iter;

	    const unsigned char * const addr_bytes =
	      reinterpret_cast<const unsigned char *>(addr_ipv4);
	    std::ostringstream dnsbl_ostr;
	    dnsbl_ostr << int(addr_bytes[3]) << '.'
		       << int(addr_bytes[2]) << '.'
		       << int(addr_bytes[1]) << '.'
		       << int(addr_bytes[0]) << '.' << blname;

	    const std::string &dnsbl_addr = dnsbl_ostr.str();
	    TRACE(logger.debug(), "operator () dnsbl lookup " << dnsbl_addr);
	    dnsbl_res.add(dnsbl_addr, blname);
	  }
	}
	else if (addr_ipv6)
	{
	  std::list<std::string> dnsbl;
	  with_guile(boost::bind(&Tcp_Connection::guile_dnsbl_ipv6,
				 module_, addr_ipv6, boost::ref(dnsbl)));

	  for (std::list<std::string>::const_iterator iter = dnsbl.begin();
	       iter != dnsbl.end();
	       ++iter)
	  {
	    const std::string &blname = *iter;

	    const unsigned char * const addr_bytes =
	      reinterpret_cast<const unsigned char *>(addr_ipv6);
	    std::ostringstream dnsbl_ostr;
	    dnsbl_ostr << std::hex;
	    for (int idx = 0x0f; idx >= 0; idx--)
	    {
	      dnsbl_ostr << int(addr_bytes[idx] & 0x0f) << '.'
			 << int(addr_bytes[idx] & 0xf0) << '.';
	    }
	    dnsbl_ostr << blname;

	    const std::string &dnsbl_addr = dnsbl_ostr.str();
	    TRACE(logger.debug(), "operator () dnsbl lookup " << dnsbl_addr);
	    dnsbl_res.add(dnsbl_addr, blname);
	  }
	}

	Reverse_Resolver reverse(resolver, client_.server_.handler_);
	std::string rdns;
	if (addr_ipv4)
	{
	  rdns = reverse.resolve(addr_ipv4);
	}
	else if (addr_ipv6)
	{
	  rdns = reverse.resolve(addr_ipv6);
	}

	std::string acl_error;
	if (!with_guile(boost::bind(&Tcp_Connection::guile_check_acl,
				    module_, boost::cref(dnsbl_res.complete()),
				    boost::ref(acl_error))))
	{
	  TRACE(logger.debug(), "accepting connection");

	  union
	  {
	    struct cmsghdr cmsg;
	    char buffer[CMSG_SPACE(sizeof(int))];
	  } cdata;

	  cdata.cmsg.cmsg_len = CMSG_LEN(sizeof(int));
	  cdata.cmsg.cmsg_level = SOL_SOCKET;
	  cdata.cmsg.cmsg_type = SCM_RIGHTS;

	  *((int *) CMSG_DATA(&cdata.cmsg)) = sock_.socket();

	  msg::Accept_Event_Builder accept_event;
	  accept_event.rdns(rdns);
	  client_.sock_.sendmsg(accept_event.iov(), accept_event.iovlen(),
				cdata.buffer, sizeof(cdata), MSG_EOR);
	}
	else
	{
	  TRACE(logger.info(), "refusing connection, msg=" << acl_error);

	  if (!acl_error.empty())
	  {
	    sock_.send(acl_error.data(), acl_error.length());
	  }

	  TRACE(logger.debug(), "shutting down client socket");
	  sock_.shutdown(SHUT_WR);

	  size_t len = sizeof(buffer_);
	  while (!sock_.recv(buffer_, len, 0) && len)
	  {
	    len = sizeof(buffer_);
	  }

	  sleep(1);
	}
      }
      else
      {
	const int last_error = errno;
	TRACE(logger.warn(), "unable to get peer name, errno=" << last_error);
	sock_.shutdown(SHUT_WR);

	size_t len = sizeof(buffer_);
	while (!sock_.recv(buffer_, len, 0) && len)
	{
	  len = sizeof(buffer_);
	}

	sleep(1);
      }
    }
  };

  void handle_connection(const SCM module, Fiber_Socket *_sock)
  {
    boost::scoped_ptr<Fiber_Socket> sock(_sock);

    Tcp_Connection conn(*this, module, *_sock);
    conn();
  }

  void on_accept(const SCM module, const socket_t sock,
		 const unsigned long error)
  {
    TRACE(logger.debug(), "on_accept(error=" << error << ')');

    if (!error)
    {
      server_.handler_.create_fiber(
	boost::bind(&Client::handle_connection,
		    this, module,
		    new Fiber_Socket(server_.handler_, sock)));
    }
  }

  void hello_req(const char *buffer, const size_t len)
  {
    const msg::Hello_Request_Proxy &hello =
      msg::Hello_Request_Proxy::get(buffer, len);
    TRACE(logger.debug(), "hello msg_type=" << hello.msg_type()
	  << ", version=" << hello.version());
  }


  static void *load_guile_module(const std::string &modname)
  {
    const SCM module =
      scm_c_define_module(modname.c_str(), init_module,
			  const_cast<char *>((modname + ".scm").c_str()));
    return static_cast<void *>(scm_gc_protect_object(module));
  }

  static void *guile_get_bind(const SCM module,
			      std::list<std::pair<std::string, unsigned short> > &bindl)
  {
    SCM l = scm_variable_ref(scm_c_module_lookup(module, "bind"));

    while (!scm_is_null(l))
    {
      SCM elem = scm_car(l);

      const char * const hostname = scm_to_locale_string(scm_car(elem));
      const unsigned short port = scm_to_uint16(scm_cdr(elem));

      bindl.push_back(std::make_pair(hostname, port));
      free(const_cast<char *>(hostname));

      l = scm_cdr(l);
    }

    return 0;
  }


  static void bind_addresses(const std::string &service,
			     const boost::function2<bool, SCM, socket_t> &cb)
  {
    const SCM module =
      static_cast<SCM>(with_guile(boost::bind(&Client::load_guile_module,
					      boost::cref(service))));

    std::list<std::pair<std::string, unsigned short> > bindl;
    with_guile(boost::bind(&Client::guile_get_bind,
			   module, boost::ref(bindl)));

    for (std::list<std::pair<std::string, unsigned short> >::const_iterator iter = bindl.begin();
	 iter != bindl.end();
	 ++iter)
    {
      const std::string &hostname = iter->first;
      std::ostringstream port_str;
      port_str << iter->second;

      TRACE(logger.debug(), "bind_addresses addrinfo for hostname=" << hostname
	    << ", port=" << iter->second);

      Address_Info ainfo(hostname.c_str(), port_str.str().c_str(),
			 AI_PASSIVE | AI_ADDRCONFIG | AI_NUMERICHOST,
			 AF_UNSPEC, SOCK_STREAM);
      if (ainfo)
      {
	for (Address_Info::const_iterator iter = ainfo.begin();
	     iter != ainfo.end();
	     ++iter)
	{
	  struct sockaddr *addr = iter->ai_addr;
	  const socket_t s = ::socket(addr->sa_family, SOCK_STREAM, 0);

	  const int res_bind = ::bind(s, addr, iter->ai_addrlen);
	  if (!res_bind)
	  {
	    if (cb(module, s))
	    {
	      break;
	    }
	  }
	  else
	  {
	    const int last_error = errno;
	    TRACE(logger.error(), "bind failed, errno=" << last_error);
	    RETRY_EINTR(::close(s));
	  }
	}
      }
      else
      {
	TRACE(logger.warn(), "unable to resolve "
	      << hostname << ", error=" << ainfo.error());
      }
    }
  }


  static bool bind_socket_cb(std::list<socket_t> &sockl,
			     const SCM module, const socket_t sock)
  {
    sockl.push_back(sock);

    return true;
  }

  void bind_req(const char *buffer, const size_t len)
  {
    const msg::Bind_Request_Proxy &bind_req =
      msg::Bind_Request_Proxy::get(buffer, len);
    TRACE(logger.debug(), "bind_req msg_type=" << bind_req.msg_type()
	  << ", service=" << bind_req.service());

    std::list<socket_t> sockl;
    bind_addresses(bind_req.service(),
		   boost::bind(&Client::bind_socket_cb,
			       boost::ref(sockl), _1, _2));


    std::vector<char> cmsgbuf;
    cmsgbuf.resize(CMSG_SPACE(sizeof(int)) - sizeof(int));

    for (std::list<socket_t>::const_iterator iter = sockl.begin();
	 iter != sockl.end();
	 ++iter)
    {
      const size_t off = cmsgbuf.size();
      cmsgbuf.resize(cmsgbuf.size() + sizeof(int));
      reinterpret_cast<int &>(cmsgbuf[off]) = *iter;
    }

    cmsghdr &cmsg = reinterpret_cast<cmsghdr &>(cmsgbuf[0]);
    cmsg.cmsg_len = CMSG_LEN(sockl.size() * sizeof(int));
    cmsg.cmsg_level = SOL_SOCKET;
    cmsg.cmsg_type = SCM_RIGHTS;

    msg::Bind_Reply_Builder bind_reply;
    bind_reply.error(0);
    sock_.sendmsg(bind_reply.iov(), bind_reply.iovlen(),
		  &cmsgbuf[0], cmsgbuf.size(), MSG_EOR);

    while (!sockl.empty())
    {
      const socket_t s = sockl.back();
      sockl.pop_back();
      RETRY_EINTR(::close(s));
    }
  }


  bool listen_socket_cb(const SCM module, const socket_t sock)
  {
    listeners_.push_back(sock);
    server_.handler().attach_listener(sock,
				      boost::bind(&Client::on_accept, this,
						  module, _1, _2));

    return true;
  }

  void listen_req(const char *buffer, const size_t len)
  {
    const msg::Listen_Request_Proxy &listen_req =
      msg::Listen_Request_Proxy::get(buffer, len);
    TRACE(logger.debug(), "listen_req msg_type=" << listen_req.msg_type()
	  << ", service=" << listen_req.service());

    bind_addresses(listen_req.service(),
		   boost::bind(&Client::listen_socket_cb, this, _1, _2));
  }
};


Inetd_Server::Client::Msg_Dispatcher::Entry
Inetd_Server::Client::dispatch_table[] =
{
  { msg::Hello_Request::MSG, &Inetd_Server::Client::hello_req },
  { msg::Bind_Request::MSG, &Inetd_Server::Client::bind_req },
  { msg::Listen_Request::MSG, &Inetd_Server::Client::listen_req }
};
const Inetd_Server::Client::Msg_Dispatcher Inetd_Server::Client::dispatcher(
  dispatch_table, sizeof(dispatch_table) / sizeof(*dispatch_table));

Trace_Stream logger("server");
Trace_Stream Inetd_Server::logger("Inetd_Server");
Trace_Stream Inetd_Server::Client::logger("Inetd_Server::Client");
Trace_Stream Inetd_Server::Client::Tcp_Connection::logger("Inetd_Server::Client::Tcp_Connection");


void Inetd_Server::handle_connection(Fiber_Socket *_sock)
{
  boost::scoped_ptr<Fiber_Socket> sock(_sock);
  Client client(*this, *_sock);

  TRACE(logger.debug(), "handle_connection");
  client();
}


int main(int argc, char *argv[])
{
  //trace_file_writer.output_level(TRACE_INFO);
  //trace_console_writer.output_level(TRACE_INFO);

#if defined(_WIN32)
  // initialize WinSock2
  WSADATA info;
  if (WSAStartup(MAKEWORD(2,0), &info) != 0)
  {
    TRACE(logger.critical(), "can't initialize WinSock2");
    return 1;
  }
#endif

  with_guile(&init_scripting);

  Async_Handler handler(3);
  Async_Timer timer(handler);

  Inetd_Server inetd(handler, "/tmp/nginetd.sock");

#if defined(_WIN32)
  Sleep(3600000);
#else
  sleep(3600);
#endif

  return 0;
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
