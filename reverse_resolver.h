/*	-*- C++ -*-
 * Copyright (C) 2010-2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__REVERSE_RESOLVER__H
#define INCLUDED__REVERSE_RESOLVER__H

#include "trace.h"

#include "async_handler.h"
#include "fiber_resolver.h"

#include <mutex>
#include <string>


class Reverse_Resolver
{
  static Trace_Stream logger;

  std::string rdns_;
  Fiber_Resolver &resolver_;
  void * const fiber_;
  std::mutex sync_;
  unsigned int outstanding_;

 public:
  explicit Reverse_Resolver(Fiber_Resolver &resolver);
  Reverse_Resolver(Reverse_Resolver const &) = delete;
  Reverse_Resolver &operator =(Reverse_Resolver const &) = delete;
  ~Reverse_Resolver();

  template<typename addr_t>
  const std::string &resolve(const addr_t *addr);

 protected:
  void add(const char *name, const in_addr *addr);
  void add(const char *name, const in6_addr *addr);

  void wait();
  void on_wait();

  template<typename addr_t, typename dns_rr>
  void complete_rr(const addr_t *addr, const dns_rr *rr);

  template<typename addr_t, typename dns_rr>
  void find_rr(const dns_rr *rr, const addr_t *addr);
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
