/*	-*- C++ -*-
 * Copyright (C) 2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "async_tls.h"

#include "async_handler.h"

#include <gnutls/gnutls.h>

#include <utility>


namespace
{
  Trace_Stream logger("async_tls");

  struct Gnutls_Initer
  {
    Gnutls_Initer()
    {
      gnutls_global_init();
    }

    ~Gnutls_Initer()
    {
      gnutls_global_deinit();
    }
  } gnutls_initer;


  void generate_dh_params(gnutls_dh_params_t &dh_params)
  {
    unsigned int bits = gnutls_sec_param_to_pk_bits(GNUTLS_PK_DH,
						    GNUTLS_SEC_PARAM_LEGACY);

    TRACE(logger.debug(), "generate_dh_params bits=" << bits);
    gnutls_dh_params_init(&dh_params);
    gnutls_dh_params_generate2(dh_params, bits);

    TRACE(logger.debug(), "generate_dh_params done");
  }

  gnutls_dh_params_t &get_dh_params()
  {
    static gnutls_dh_params_t dh_params;
    static bool initialized = (generate_dh_params(dh_params), true);
    return dh_params;
  }

  void init_priority_cache(gnutls_priority_t &priority_cache)
  {
    gnutls_priority_init(&priority_cache,
			 "PERFORMANCE:%SERVER_PRECEDENCE",
			 NULL);
  }

  gnutls_priority_t &get_priority_cache()
  {
    static gnutls_priority_t priority_cache;
    static bool initialized = (init_priority_cache(priority_cache), true);

    return priority_cache;
  }

  void init_x509_cred(gnutls_certificate_credentials_t &x509_cred)
  {
    gnutls_certificate_allocate_credentials(&x509_cred);

    gnutls_certificate_set_x509_trust_file(x509_cred,
					   "/etc/ssl/certs/ca-certificates.crt",
					   GNUTLS_X509_FMT_PEM);

    int res;
    if ((res = gnutls_certificate_set_x509_key_file(x509_cred,
						    "cert.pem",
						    "key.pem",
						    GNUTLS_X509_FMT_PEM)) < 0)
    {
      TRACE(logger.error(),
	    "gnutls_certificate_set_x509_key_file error=" << res);
    }

    gnutls_certificate_set_dh_params(x509_cred, get_dh_params());
  }

  gnutls_certificate_credentials_t &get_x509_cred()
  {
    static gnutls_certificate_credentials_t x509_cred;
    static bool initialized = (init_x509_cred(x509_cred), true);

    return x509_cred;
  }
}

extern "C" ssize_t async_tls_push(gnutls_transport_ptr_t data, const void *buf,
				  size_t len)
{
  if (data)
  {
    Async_Tls &tls(*static_cast<Async_Tls *>(data));
    return tls.async_push(static_cast<const char *>(buf), len);
  }
  else
  {
    errno = ECONNRESET;
    return -1;
  }
}

extern "C" ssize_t async_tls_pull(gnutls_transport_ptr_t data, void *buf,
				  size_t len)
{
  if (data)
  {
    Async_Tls &tls(*static_cast<Async_Tls *>(data));
    return tls.async_pull(static_cast<char *>(buf), len);
  }
  else
  {
    errno = ECONNRESET;
    return -1;
  }
}

Trace_Stream Async_Tls::logger("Async_Tls");


Async_Tls::Async_Tls(std::unique_ptr<Async_Socket> &&sock)
  : sock_(std::move(sock))
{
  gnutls_init(&session_, GNUTLS_SERVER);
  gnutls_priority_set(session_, get_priority_cache());
  gnutls_credentials_set(session_, GNUTLS_CRD_CERTIFICATE,
			 get_x509_cred());

  gnutls_certificate_server_set_request(session_, GNUTLS_CERT_IGNORE);

  gnutls_transport_set_ptr(session_, this);
  gnutls_transport_set_push_function(session_, async_tls_push);
  gnutls_transport_set_pull_function(session_, async_tls_pull);
}

Async_Tls::~Async_Tls()
{
  gnutls_deinit(session_);
}

Async_Tls::req_id_t Async_Tls::handshake(const std::function<void (int)> &cb)
{
  std::function<void (int)> done_cb;
  int res;

  {
    std::lock_guard<std::mutex> guard(sync_);
  
    in_handshake_ = true;
    handshake_cb_ = cb;

    res = gnutls_handshake(session_);
    TRACE(logger.debug(), "gnutls handshake res=" << res);
    if (res != GNUTLS_E_AGAIN)
    {
      in_handshake_ = false;
      done_cb = std::move(handshake_cb_);
    }
  }

  if (done_cb)
  {
    done_cb(res);
  }

  return NO_REQ_ID;
}

Async_Tls::req_id_t Async_Tls::send(const char *data, size_t len, int flags,
				    const Async_Handler::send_handler &cb)
{
  while (true)
  {
    ssize_t const res = gnutls_record_send(session_, data, len);
    TRACE(logger.debug(), "gnutls send res=" << res);

    if (res < 0)
    {
      break;
    }
    else if (res < len)
    {
      data += res;
      len -= res;
      continue;
    }

    break;
  }

  return NO_REQ_ID;
}

Async_Tls::req_id_t Async_Tls::sendmsg(const struct iovec *iov, int iov_len,
				       int flags,
				       const Async_Handler::send_handler &cb)
{
  gnutls_record_cork(session_);
  ssize_t res = 0;

  for (int i = 0; (i != iov_len) && (res >= 0); ++i)
  {
    const char *data = static_cast<const char *>(iov[i].iov_base);
    size_t len = iov[i].iov_len;

    while (true)
    {
      res = gnutls_record_send(session_, data, len);
      TRACE(logger.debug(), "gnutls send res=" << res);

      if ((res >= 0) && (res < len))
      {
	data += res;
	len -= res;
	continue;
      }

      break;
    }
  }

  res = gnutls_record_uncork(session_, GNUTLS_RECORD_WAIT);

  return NO_REQ_ID;
}

Async_Tls::req_id_t Async_Tls::recv(char *buffer, size_t len,
				    const Async_Handler::recv_handler &cb)
{
  std::unique_lock<std::mutex> guard(sync_);
  bool const was_empty(recv_queue_.empty());
  recv_queue_.emplace_back(buffer, len, 0, cb);
  while (!in_handshake_ && was_empty)
  {
    ssize_t const recv_res = gnutls_record_recv(session_, buffer, len);
    TRACE(logger.debug(), "gnutls recv res=" << recv_res);

    if ((recv_res == GNUTLS_E_INTERRUPTED) || 
	(recv_res == GNUTLS_E_AGAIN))
    {
    }
    else if (recv_res == GNUTLS_E_REHANDSHAKE)
    {
      in_handshake_ = true;
      int const handshake_res = gnutls_handshake(session_);
      TRACE(logger.debug(), "gnutls handshake res=" << handshake_res);
      if (handshake_res != GNUTLS_E_AGAIN)
      {
	in_handshake_ = false;
	continue;
      }
    }
    else
    {
      recv_queue_.pop_front();
      guard.unlock();
      cb((recv_res < 0) ? recv_res : 0, (recv_res >= 0) ? recv_res : 0);
      guard.lock();
    }

    break;
  }

  return NO_REQ_ID;
}

Async_Tls::req_id_t Async_Tls::shutdown(int how)
{
  int const res =
    gnutls_bye(session_, how == SHUT_WR ? GNUTLS_SHUT_WR : GNUTLS_SHUT_RDWR);
  TRACE(logger.debug(), "gnutls shutdown res=" << res);

  if (res == GNUTLS_E_SUCCESS)
  {
    sock_->shutdown(how);
  }

  return NO_REQ_ID;
}

std::unique_ptr<Async_Socket> Async_Tls::detach()
{
  gnutls_transport_set_ptr(session_, nullptr);

  return std::move(sock_);
}

ssize_t Async_Tls::async_push(const char *data, size_t len)
{
  req_id_t const req_id = sock_->send(data, len, MSG_NOSIGNAL);
  return len;
}

ssize_t Async_Tls::async_pull(char *buffer, size_t len)
{
  static thread_local bool same_thread;

  if (! buffer_len_)
  {
    buffer_offset_ = 0;

    same_thread = true;
    req_id_t const req_id =
      sock_->recv(buffer_, std::min(len, sizeof(buffer_)),
		  [this] (int err, size_t len)
		  {
		    if (err)
		    {
		      TRACE(logger.debug(), "async_pull same_thread="
			    << same_thread << ", err=" << err);
		    }
		    else
		    {
		      TRACE(logger.debug(), "async_pull same_thread="
			    << same_thread << ", len=" << len);
		    }

		    if (same_thread)
		    {
		      buffer_len_ += len;
		      buffer_end_ = (len == 0);
		    }
		    else
		    {
		      std::unique_lock<std::mutex> guard(sync_);
		      buffer_len_ += len;
		      buffer_end_ = (len == 0);

		      if (in_handshake_)
		      {
			int const res = gnutls_handshake(session_);
			TRACE(logger.debug(), "gnutls handshake res=" << res);
			if (res != GNUTLS_E_AGAIN)
			{
			  in_handshake_ = false;
			  auto cb(std::move(handshake_cb_));

			  guard.unlock();
			  cb(res);
			  guard.lock();
			}
		      }

		      while (! in_handshake_ && ! recv_queue_.empty ())
		      {
			recv_request const & req(recv_queue_.front());

			ssize_t const recv_res =
			  gnutls_record_recv(session_, req.buffer, req.len);
			TRACE(logger.debug(), "gnutls recv res="
			      << recv_res);
			if ((recv_res == GNUTLS_E_INTERRUPTED) || 
			    (recv_res == GNUTLS_E_AGAIN))
			{
			  break;
			}
			else if (recv_res == GNUTLS_E_REHANDSHAKE)
			{
			  in_handshake_ = true;
			  int const handshake_res = gnutls_handshake(session_);
			  TRACE(logger.debug(), "gnutls handshake res="
				<< handshake_res);
			  if (handshake_res >= 0)
			  {
			    in_handshake_ = false;
			    continue;
			  }
			}
			else
			{
			  auto cb(req.recv_cb);
			  recv_queue_.pop_front();
			  guard.unlock();
			  cb((recv_res < 0) ? recv_res : 0,
			     (recv_res >= 0) ? recv_res : 0);
			  guard.lock();

			  if (! in_handshake_ && ! recv_queue_.empty())
			  {
			    continue;
			  }
			}

			break;
		      }
		    }
		  });
    same_thread = false;
  }

  if (buffer_len_ || buffer_end_)
  {
    size_t const l (std::min(len, buffer_len_));

    std::copy(buffer_ + buffer_offset_,
	      buffer_ + buffer_offset_ + l,
	      buffer);
    buffer_offset_ += l;
    buffer_len_ -= l;

    return l;
  }
  else
  {
    errno = EAGAIN;
    return -1;
  }
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
