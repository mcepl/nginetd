/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "async_handler.h"
#include "async_socket.h"

#include <unistd.h>
#include <errno.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <vector>


namespace
{
  pthread_mutex_t outstanding_sync = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t outstanding_cond = PTHREAD_COND_INITIALIZER;
  int nr_outstanding = 0;


  void print_duration(struct timeval const &start)
  {
    struct timeval end;
    gettimeofday(&end, NULL);

    end.tv_sec -= start.tv_sec;
    if (end.tv_usec < start.tv_usec)
    {
      end.tv_usec += 1000000;
      --end.tv_sec;
    }
    end.tv_usec -= start.tv_usec;

    printf("duration %ld.%06ld s\n", end.tv_sec, end.tv_usec);
  }

  void print_rusage()
  {
    struct rusage usage;
    if (!getrusage(RUSAGE_SELF, &usage))
    {
      printf("vol/invol cs %ld/%ld\n", usage.ru_nvcsw, usage.ru_nivcsw);
    }
  }
}


class Async_Server
{
  Async_Handler &handler_;
  const int nr_iter_;
  std::unique_ptr<Async_Socket> s1_;
  std::unique_ptr<Async_Socket> s2_;

  union
  {
    char buf_[sizeof(int)];
    int seq_nr_;
  };

public:
  Async_Server(Async_Handler &handler, const int nr_iter, int s1, int s2)
    : handler_(handler), nr_iter_(nr_iter),
      s1_(handler.attach(s1)), s2_(handler.attach(s2))
  {
    s1_->recv(buf_, sizeof(buf_), [this] (const int err, const size_t len)
	      { on_recv(*s1_, err, len); });
    s2_->recv(buf_, sizeof(buf_), [this] (const int err, const size_t len)
	      { on_recv(*s2_, err, len); });
  }

  void start()
  {
    seq_nr_ =0;
    s1_->send(buf_, sizeof(buf_));
  }

private:
  void on_recv(Async_Socket &s, const int err, const size_t len)
  {
    s.recv(buf_, sizeof(buf_), [this, &s] (const int err, const size_t len)
	   { on_recv(s, err, len); });

    if (++seq_nr_ < nr_iter_)
    {
      s.send(buf_, sizeof(buf_));
    }
    else if (seq_nr_ == nr_iter_)
    {
      pthread_mutex_lock(&outstanding_sync);
      if (!--nr_outstanding)
      {
	pthread_cond_broadcast(&outstanding_cond);
      }
      pthread_mutex_unlock(&outstanding_sync);
    }
  }
};


int main(int argc, char *argv[])
{
  const int nr_threads = (argc > 1) ? atoi(argv[1]) : 1;
  const int nr_iter = (argc > 2) ? atoi(argv[2]) : 1;
  const int nr_pairs = (argc > 3) ? atoi(argv[3]) : 1;

  Async_Handler handler(nr_threads);

  std::vector<Async_Server *> srvs;
  for (int i = 0; i < nr_pairs; ++i)
  {
    int sockets[2];
    socketpair(AF_UNIX, SOCK_DGRAM, 0, sockets);
    srvs.push_back(new Async_Server(handler, nr_iter,
				    sockets[0], sockets[1]));
    ++nr_outstanding;
  }

  struct timeval start;
  gettimeofday(&start, NULL);

  for (std::vector<Async_Server *>::iterator iter = srvs.begin();
       iter != srvs.end();
       ++iter)
  {
    (*iter)->start();
  }

  pthread_mutex_lock(&outstanding_sync);
  while (nr_outstanding)
  {
    pthread_cond_wait(&outstanding_cond, &outstanding_sync);
  }
  pthread_mutex_unlock(&outstanding_sync);

  print_duration(start);
  print_rusage();
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
