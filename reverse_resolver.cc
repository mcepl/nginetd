/*	-*- C++ -*-
 * Copyright (C) 2010-2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "reverse_resolver.h"

#include "fiber_handler.h"

#if defined(_WIN32)
#include <winsock2.h>
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

#include <cstring>


Trace_Stream Reverse_Resolver::logger("Reverse_Resolver");

Reverse_Resolver::Reverse_Resolver(Fiber_Resolver &resolver)
  : resolver_(resolver),
    fiber_(Fiber_Handler::current_fiber()),
    outstanding_(1)
{ }

Reverse_Resolver::~Reverse_Resolver()
{ }

template<typename addr_t>
const std::string &Reverse_Resolver::resolve(const addr_t *addr)
{
  TRACE(logger.debug(), "resolve");
  const dns_rr_ptr * const rr = resolver_.resolve_ptr(addr);
  if (rr != NULL)
  {
    TRACE(logger.debug(), "resolve ptr done");

    for (int i = 0; i < rr->dnsptr_nrr; i++)
    {
      add(rr->dnsptr_ptr[i], addr);
    }

    free(const_cast<dns_rr_ptr *>(rr));
    wait();
  }
  else
  {
    TRACE(logger.warn(), "resolve no PTR");
  }

  return rdns_;
}

void Reverse_Resolver::add(const char *name, const in_addr *addr)
{
  std::lock_guard<std::mutex> guard(sync_);
  outstanding_++;
  resolver_.async().resolve_a(name, [this, addr] (const dns_rr_a4 *rr)
			      { complete_rr(addr, rr); },
			      false);
}

void Reverse_Resolver::add(const char *name, const in6_addr *addr)
{
  std::lock_guard<std::mutex> guard(sync_);
  outstanding_++;
  resolver_.async().resolve_aaaa(name, [this, addr] (const dns_rr_a6 *rr)
				 { complete_rr(addr, rr); },
				 false);
}

void Reverse_Resolver::wait()
{
  std::lock_guard<std::mutex> guard(sync_);
  if (--outstanding_)
  {
    Fiber_Handler::create_fiber([this] () { on_wait(); }, 2048);
  }
}

void Reverse_Resolver::on_wait()
{
  TRACE(logger.debug(), "on_wait");
  sync_.unlock();
}


template<typename addr_t, typename dns_rr>
void Reverse_Resolver::complete_rr(const addr_t *addr, const dns_rr *rr)
{
  TRACE(logger.debug(), "complete_rr " << outstanding_);

  const bool is_current_fiber = (fiber_ == Fiber_Handler::current_fiber());
  if (!is_current_fiber)
  {
    sync_.lock();
  }

  if (rr && rdns_.empty())
  {
    find_rr(rr, addr);
  }

  free(const_cast<dns_rr *>(rr));
  outstanding_--;

  if (!is_current_fiber)
  {
    if (!outstanding_)
    {
      Fiber_Handler::schedule_fiber(fiber_);
    }
    else
    {
      sync_.unlock();
    }
  }
}

template<>
void Reverse_Resolver::find_rr(const dns_rr_a4 *rr, const in_addr *addr)
{
  for (int i = 0; i < rr->dnsa4_nrr; i++)
  {
    if (!std::memcmp(&rr->dnsa4_addr[i], addr, sizeof(*addr)))
    {
      rdns_ = rr->dnsa4_qname;
      break;
    }
  }
}

template<>
void Reverse_Resolver::find_rr(const dns_rr_a6 *rr, const in6_addr *addr)
{
  for (int i = 0; i < rr->dnsa6_nrr; i++)
  {
    if (!std::memcmp(&rr->dnsa6_addr[i], addr, sizeof(*addr)))
    {
      rdns_ = rr->dnsa6_qname;
      break;
    }
  }
}

template const std::string &Reverse_Resolver::resolve(const in_addr *addr);
template const std::string &Reverse_Resolver::resolve(const in6_addr *addr);

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
