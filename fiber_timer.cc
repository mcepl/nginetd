/*	-*- C++ -*-
 * Copyright (C) 2011-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "fiber_timer.h"

#include "async_handler.h"
#include "fiber_handler.h"

#include <time.h>


Trace_Stream Fiber_Timer::logger("Fiber_Timer");


Fiber_Timer::Fiber_Timer(Async_Handler &handler)
  : async_instance_(new Async_Timer(handler)),
    timer_(*async_instance_)
{ }

Fiber_Timer::Fiber_Timer(Async_Timer &timer)
  : timer_(timer)
{ }

Fiber_Timer::~Fiber_Timer()
{ }

void Fiber_Timer::sleep(unsigned int secs)
{
  completed_ = false;
  void * const fiber = Fiber_Handler::current_fiber();

  TRACE(logger.debug(), "sleep " << secs << " seconds");
  {
    std::lock_guard<std::mutex> guard(sync_);
    timer_.register_timer(secs, [this, fiber] (const Async_Timer::reg_t reg)
			  { on_timer(fiber, reg); });
    if (!completed_)
    {
      Fiber_Handler::create_fiber([this] () { on_wait(); }, 2048);
    }
  }

  TRACE(logger.debug(), "sleep done");
}

void Fiber_Timer::on_wait()
{
  sync_.unlock();
}

void Fiber_Timer::on_timer(void * const fiber, const Async_Timer::reg_t reg)
{
  timer_.unregister_timer(reg);

  if (fiber == Fiber_Handler::current_fiber())
  {
    completed_ = true;
  }
  else
  {
    sync_.lock();
    completed_ = true;
    Fiber_Handler::schedule_fiber(fiber);
  }
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
