/*	-*- C++ -*-
 * Copyright (C) 2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__SCRIPTING__H
#define INCLUDED__SCRIPTING__H

#include "trace.h"

#include <memory>
#include <mutex>
#include <string>
#include <vector>

extern "C"
{
#include <lua.h>
}


class Scripting
{
  static Trace_Stream logger;

  struct Lua_Deleter
  {
    void operator () (lua_State *l) const
    {
      ::lua_close(l);
    }
  };

  typedef std::unique_ptr<lua_State, Lua_Deleter> interp_ptr_t;

 public:
  class Script;

  class Interp
  {
    friend class Script;

    Script &script_;
    interp_ptr_t interp_ptr_;

  protected:
    inline Interp(Script &script, interp_ptr_t interp_ptr)
      : script_(script), interp_ptr_(std::move(interp_ptr))
    { }

    Interp(Interp const &) = delete;
    Interp &operator =(Interp const &) = delete;

  public:
    inline ~Interp()
    {
      script_.release(std::move(interp_ptr_));
    }

    inline lua_State *get()
    {
      return interp_ptr_.get();
    }
  };

  typedef std::unique_ptr<Interp> interp_t;

  class Script
  {
    friend class Interp;
    friend class Scripting;

    const static unsigned int max_idle_ = 3;
    const std::string name_;
    std::mutex sync_;
    std::vector<interp_ptr_t> interp_list_;

   protected:
    inline explicit Script(const std::string &name)
      : name_(name)
    {
      TRACE(logger.debug(), "Script(" << name << ')');
    }

    Script(Script const &) = delete;
    Script &operator =(Script const &) = delete;

    interp_ptr_t create();

    void release(interp_ptr_t state);

   public:
    ~Script();

    interp_t get();
  };

  typedef std::shared_ptr<Script> script_t;

  static script_t get(const std::string &name)
  {
    return script_t(new Script(name));
  }
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
