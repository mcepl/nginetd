/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__TRACE__H
#define INCLUDED__TRACE__H

#include <fstream>
#include <list>
#include <memory>
#include <ostream>
#include <streambuf>
#include <string>
#include <utility>


enum Trace_Levels
{
  TRACE_NONE,
  TRACE_CRITICAL,
  TRACE_ERROR,
  TRACE_WARN,
  TRACE_INFO,
  TRACE_DEBUG
};


class Trace_Writer
{
  friend class Trace_Stream;

  class Stream_Buffer
    : public std::streambuf
  {
   public:
    inline explicit Stream_Buffer(Trace_Writer &trace_writer)
      : trace_writer_(trace_writer),
	current_trace_level_(TRACE_DEBUG)
    {
      setp(pbuf_, pbuf_ + sizeof(pbuf_));
      buflst_.push_back(std::make_pair(pbuf_, sizeof(pbuf_)));
    }

    virtual ~Stream_Buffer();


    inline void level(Trace_Levels trace_level)
    {
      current_trace_level_ = trace_level;
    }


   protected:
    void cleanup_buffer();

    virtual void write_trace(
      const std::list<std::pair<char *, size_t> > &buf);

    virtual int overflow(int c = traits_type::eof());
    virtual int sync();

    virtual int underflow();


    static const size_t BUF_SIZE = 1024;


   private:
    Trace_Writer &trace_writer_;

    char pbuf_[BUF_SIZE];
    std::list<std::pair<char *, size_t> > buflst_;

    Trace_Levels current_trace_level_;
  };

 public:
  inline explicit Trace_Writer(Trace_Levels output_level)
    : output_level_(output_level)
  { }

  inline ~Trace_Writer()
  { }


  virtual void trace(Trace_Levels trace_level,
		     const std::list<std::pair<char *, size_t> > &buf) = 0;


 public:
  inline Trace_Levels output_level() const
  {
    return output_level_;
  }

  inline void output_level(Trace_Levels trace_level)
  {
    output_level_ = trace_level;
  }


 private:
  Trace_Levels output_level_;
};


class Trace_Stream
{
 public:
  inline explicit Trace_Stream(const char *module_name)
    : module_name_(module_name), module_level_(TRACE_DEBUG)
  { }

  Trace_Stream(Trace_Stream const &) = delete;
  Trace_Stream &operator =(Trace_Stream const &) = delete;

  inline ~Trace_Stream()
  { }


  std::ostream &stream(const Trace_Levels level,
		       const bool reset = true) const;

  inline std::ostream &debug(const bool reset = true) const
  {
    if ((module_level_ >= TRACE_DEBUG) && (global_level_ >= TRACE_DEBUG))
      return stream(TRACE_DEBUG, reset);
    else
      return null_stream;
  }

  inline std::ostream &info(const bool reset = true) const
  {
    if ((module_level_ >= TRACE_INFO) && (global_level_ >= TRACE_INFO))
      return stream(TRACE_INFO, reset);
    else
      return null_stream;
  }

  inline std::ostream &warn(const bool reset = true) const
  {
    if ((module_level_ >= TRACE_WARN) && (global_level_ >= TRACE_WARN))
      return stream(TRACE_WARN, reset);
    else
      return null_stream;
  }

  inline std::ostream &error(const bool reset = true) const
  {
    if ((module_level_ >= TRACE_ERROR) && (global_level_ >= TRACE_ERROR))
      return stream(TRACE_ERROR, reset);
    else
      return null_stream;
  }

  inline std::ostream &critical(const bool reset = true) const
  {
    if ((module_level_ >= TRACE_CRITICAL) && (global_level_ >= TRACE_CRITICAL))
      return stream(TRACE_CRITICAL, reset);
    else
      return null_stream;
  }

  inline std::ostream &fatal(const bool reset = true) const
  {
    if (module_level_ >= TRACE_NONE)
      return stream(TRACE_NONE, reset);
    else
      return null_stream;
  }


 protected:
  static std::ostream &trace_stream(bool reset,
				    Trace_Levels trace_level = TRACE_NONE);


 private:
  typedef std::pair<std::unique_ptr<std::ostream>,
		    std::unique_ptr<Trace_Writer::Stream_Buffer> > Stream_Buffer_Pair;

  const std::string module_name_;

  static Trace_Writer &trace_writer_;
  static thread_local std::unique_ptr<Stream_Buffer_Pair> thread_specific_trace_;

  Trace_Levels module_level_;
  static Trace_Levels global_level_;

 public:
  static std::ostream null_stream;

  static inline void set_global_level(Trace_Levels level)
  {
    global_level_ = level;
  }
};


extern Trace_Writer &trace_file_writer;
extern Trace_Writer &trace_console_writer;

#define TRACE(stream, msg) \
	{ std::ostream &_os = (stream); \
	  if (_os != Trace_Stream::null_stream) \
	  { _os << msg << std::endl; }	 \
	}

#endif


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
