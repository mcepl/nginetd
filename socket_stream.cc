/*	-*- C++ -*-
 * Copyright (C) 2003-2007, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "socket_stream.h"

#include "fiber_socket.h"

#include <iostream>


Socket_Stream_Buffer::Socket_Stream_Buffer(Fiber_Socket &socket)
  : socket_(socket)
{
  setp(write_buffer_, write_buffer_ + BUF_SIZE);
  setg(read_buffer_, read_buffer_ + BUF_SIZE + 1, read_buffer_ + BUF_SIZE + 1);
}

Socket_Stream_Buffer::~Socket_Stream_Buffer()
{ }


int Socket_Stream_Buffer::overflow(int c)
{
  if (c != EOF)
  {
    // add the given character to the buffer
    // we can do this as we always reserve one extra character for
    // this operation
    *pptr() = char(c);
    pbump(1);
  }

  // send data and reset buffer
  const unsigned long res_send = socket_.send(pbase(), pptr() - pbase());
  setp(write_buffer_, write_buffer_ + BUF_SIZE);


  return res_send ? EOF : 0;
}

int Socket_Stream_Buffer::underflow()
{
  size_t len = BUF_SIZE + 1;
  const unsigned long res_recv = socket_.recv(read_buffer_, len);

  if (res_recv || len == 0)
  {
    return EOF;
  }

  setg(read_buffer_, read_buffer_, read_buffer_ + len);

  return read_buffer_[0];
}

int Socket_Stream_Buffer::sync()
{
  socket_.send(pbase(), pptr() - pbase());

  // reset the buffer
  setp(write_buffer_, write_buffer_ + BUF_SIZE);


  return 0;
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
