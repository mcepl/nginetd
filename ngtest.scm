(define bind '( ( "127.0.0.1" . 12345 )
		( "127.0.0.2" . 12345 )
		( "::1" . 12345 ) ) )

(define dnsbl-ipv4 '( "proxies.dnsbl.sorbs.net"
		      "cbl.abuseat.org" ) )
(define dnsbl-ipv6 '( "virbl.dnsbl.bit.nl" ) )
(define (dnsbl addr)
  (if (is-ipv4-address addr) dnsbl-ipv4
      (if (is-ipv6-address addr) dnsbl-ipv6)))

(define (check-acl bl)
  (if (>= (hash-fold (lambda (k v r) (+ 1 r)) 0 bl) 1)
      "500 Access denied.\r\n")
  )
