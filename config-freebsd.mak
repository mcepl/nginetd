#	-*- Makefile -*-
# Copyright (C) 2012, Christof Meerwald
# http://cmeerw.org
#

DIR_LUA=/usr/local/include/lua51
LIB_LUA=lua-5.1
CFLAGS_COMPILER=-pthread -I/usr/local/include -L/usr/local/lib
CFLAGS_CONFIG=-O -g

DEFINES=POSIX HAVE_UDNS HAVE_KQUEUE
