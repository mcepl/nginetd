require('nginet')

ngtest = Service('ngtest', 12345)

function ngtest:bind()
   return { { '127.0.0.1', self._port },
	    { '127.0.0.2', self._port },
	    { '::1', self._port } }
end

function ngtest:dnsbl(proto, addr, port)
   if proto == 'IPv4' then
      return { 'proxies.dnsbl.sorbs.net', 'cbl.abuseat.org' }
   elseif proto == 'IPv6' then
      return { 'virbl.dnsbl.bit.nl' }
   end
end

function ngtest:check_accept(conn, dnsbl)
   if next(dnsbl) then
      return { send='500 Access denied.\r\n' }
   else
      conn.count = 3
      return { send='220 Hello\r\n',
	       receive={ ' ', self.check_data, 10 } }
   end
end

function ngtest:check_data(conn, data)
   print('check_data: ' .. data)
   if conn.count == 0 then
      return { send='221 Bye.\r\n' }
   else
      conn.count = conn.count - 1
      return { receive={ ' ', self.check_data, 10 } }
   end
end

return ngtest
