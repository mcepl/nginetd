#	-*- Makefile -*-
# Copyright (C) 2003-2011, Christof Meerwald
# http://cmeerw.org
#

# include customised configuration
ifdef CONFIG
-include config-$(CONFIG).mak
else
-include config.mak
endif

# include default configuration
include config-default.mak


INCLUDES:=$(DIR_BOOST) $(DIR_LUA)

CFLAGS:=$(CFLAGS_COMPILER) $(CFLAGS_CONFIG) \
	$(foreach def,$(DEFINES),-D'$(def)') \
	$(foreach incl,$(INCLUDES),-I'$(incl)')

COMMON_SRCS:=async_handler.cc async_resolver.cc async_socket.cc \
	async_timer.cc async_tls.cc async_worker.cc dnsbl_resolver.cc \
	fiber_handler.cc fiber_resolver.cc fiber_socket.cc fiber_timer.cc \
	reverse_resolver.cc socket_stream.cc trace.cc
SCRIPTING_SRCS:=scripting.cc

NGINETD_SRCS:=nginetd-lua.cc
ASYNCSRV_SRCS:=asyncsrv.cc
FIBERSRV_SRCS:=fibersrv.cc
WSSRV_SRCS:=wssrv.cc util/base64.cc util/sha1.cc

ASIOSRV_SRCS:=test/asiosrv.cc
EPTEST_SRCS:=test/eptest.cc
KQTEST_SRCS:=test/kqtest.cc
MTTEST_SRCS:=test/mttest.cc
RESOLVETEST_SRCS:=test/resolvetest.cc

COMMON_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(COMMON_SRCS))
SCRIPTING_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(SCRIPTING_SRCS))
ASYNCSRV_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(ASYNCSRV_SRCS))
FIBERSRV_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(FIBERSRV_SRCS))
NGINETD_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(NGINETD_SRCS))
WSSRV_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(WSSRV_SRCS))

ASIOSRV_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(ASIOSRV_SRCS))
EPTEST_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(EPTEST_SRCS))
KQTEST_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(KQTEST_SRCS))
MTTEST_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(MTTEST_SRCS))
RESOLVETEST_OBJS:=$(patsubst %.cc,%$(SUFFIX_OBJ),$(RESOLVETEST_SRCS))


%$(SUFFIX_PICOBJ):	%.cc
	$(call RULE_PICOBJ,$<,$@)

%$(SUFFIX_OBJ):	%.cc
	$(call RULE_OBJ,$<,$@)


all:	$(call NAME_EXE,nginetd) $(call NAME_EXE,asyncsrv) \
	$(call NAME_EXE,fibersrv) $(call NAME_EXE,asiosrv) \
	$(call NAME_EXE,mttest) $(call NAME_EXE,resolvetest) \
	$(call NAME_EXE,wssrv)

$(call NAME_EXE,nginetd):	$(COMMON_OBJS) $(SCRIPTING_OBJS) $(NGINETD_OBJS)
	$(call RULE_EXE,$^,$@,udns $(LIB_LUA) gnutls)

$(call NAME_EXE,asyncsrv):	$(COMMON_OBJS) $(ASYNCSRV_OBJS)
	$(call RULE_EXE,$^,$@,udns gnutls)

$(call NAME_EXE,fibersrv):	$(COMMON_OBJS) $(FIBERSRV_OBJS)
	$(call RULE_EXE,$^,$@,udns gnutls)

$(call NAME_EXE,asiosrv):	$(ASIOSRV_OBJS)
	$(call RULE_EXE,$^,$@,boost_thread boost_system boost_system)

$(call NAME_EXE,eptest):	$(EPTEST_OBJS)
	$(call RULE_EXE,$^,$@,)

$(call NAME_EXE,kqtest):	$(KQTEST_OBJS)
	$(call RULE_EXE,$^,$@,)

$(call NAME_EXE,mttest):	$(MTTEST_OBJS)
	$(call RULE_EXE,$^,$@,)

$(call NAME_EXE,resolvetest):	$(COMMON_OBJS) $(RESOLVETEST_OBJS)
	$(call RULE_EXE,$^,$@,udns gnutls)

$(call NAME_EXE,wssrv):	$(COMMON_OBJS) $(WSSRV_OBJS)
	$(call RULE_EXE,$^,$@,udns gnutls)


Makefile.deps:	$(COMMON_SRCS) $(NGINETD_SRCS)
	touch Makefile.deps
	makedepend $(foreach def,$(DEFINES),-D'$(def)') -fMakefile.deps -Y $^

depend:	Makefile.deps

-include Makefile.deps
