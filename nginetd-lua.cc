/*	-*- C++ -*-
 * Copyright (C) 2003-2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "address_info.h"
#include "async_handler.h"
#include "async_timer.h"
#include "dnsbl_resolver.h"
#include "fiber_handler.h"
#include "fiber_resolver.h"
#include "fiber_socket.h"
#include "fiber_timer.h"
#include "reverse_resolver.h"
#include "scripting.h"
#include "socket.h"
#include "socket_stream.h"
#include "trace.h"
#include "msg_dispatcher.h"
#include "nginetd.h"

#include <algorithm>
#include <cstring>
#include <list>
#include <map>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

#if defined(_WIN32)
#include <io.h>
#else
#include <unistd.h>
#include <sys/mman.h>
#include <sys/un.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#if defined(_WIN32)
#include <winsock2.h>
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

extern "C"
{
#include <lua.h>
}


#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }

namespace msg = nginetd::msg;


class Inetd_Server
{
 private:
  class Client;

  static Trace_Stream logger;
  static const unsigned int buffer_size = 4096;

  Async_Handler &handler_;
  Async_Resolver async_resolver_;
  Async_Timer timer_;
  const socket_t listener_sock_;
  const std::string sockname_;

 protected:
  inline Async_Handler &handler()
  { return handler_; }

  inline Async_Resolver &async_resolver()
  { return async_resolver_; }

  inline Async_Timer &async_timer()
  { return timer_; }

 public:
  Inetd_Server(Async_Handler &handler, const char * const sockname)
    : handler_(handler),
      sockname_(sockname),
      listener_sock_(::socket(PF_UNIX, SOCK_SEQPACKET, 0)),
      async_resolver_(handler_), timer_(handler_)
  {
    // set up TCP/IP listener socket
#if defined(_WIN32)
    if (listener_sock_ != INVALID_SOCKET)
#else
    if (listener_sock_ != -1)
#endif
    {
      ::unlink(sockname_.c_str());

      struct sockaddr_un addr;
      addr.sun_family = AF_UNIX;
      strcpy(addr.sun_path, sockname);
      const int res_bind =
	::bind(listener_sock_, (struct sockaddr *) &addr, SUN_LEN(&addr));
      if (!res_bind)
      {
	handler_.attach_listener(listener_sock_,
				 [this] (const socket_t sock, const unsigned long error)
				 { on_accept(sock, error); });
      }
      else
      {
	TRACE(logger.error(), "bind failed");
	RETRY_EINTR(::close(listener_sock_));
      }
    }
    else
    {
      TRACE(logger.error(), "listener socket creation failed");
    }
  }

  ~Inetd_Server()
  {
    if (listener_sock_ != -1)
    {
      TRACE(logger.debug(), "cleaning up listener socket " << listener_sock_);
      handler_.detach_listener(listener_sock_);
      RETRY_EINTR(::close(listener_sock_));
      ::unlink(sockname_.c_str());
    }
  }


 protected:
  void on_accept(const socket_t sock, const unsigned long error)
  {
    TRACE(logger.debug(), "on_accept(error=" << error << ')');

    if (!error)
    {
      Fiber_Handler::create_fiber([this, sock] ()
				  { handle_connection(new Fiber_Socket(handler_, timer_, sock)); });
    }
  }

  void handle_connection(Fiber_Socket * const _sock);
};


class Inetd_Server::Client
{
 private:
  typedef void (Client::*MSG_HANDLER)(const char *, size_t);
  typedef ::Msg_Dispatcher<uint16_t, MSG_HANDLER> Msg_Dispatcher;

  static Msg_Dispatcher::Entry dispatch_table[];
  static const Msg_Dispatcher dispatcher;
  static Trace_Stream logger;

  Inetd_Server &server_;
  Fiber_Socket &sock_;

  std::list<socket_t> listeners_;

 protected:
  inline Async_Handler &handler()
  { return server_.handler(); }

  inline Async_Resolver &async_resolver()
  { return server_.async_resolver(); }

  inline Fiber_Socket &sock()
  { return sock_; }

  inline Async_Timer &async_timer()
  { return server_.async_timer(); }

 public:
  inline Client(Inetd_Server &server, Fiber_Socket &sock)
    : server_(server), sock_(sock)
  { }

  ~Client()
  {
    TRACE(logger.debug(), "cleaning up listener sockets");
    while (!listeners_.empty())
    {
      const socket_t s = listeners_.back();
      listeners_.pop_back();

      handler().detach_listener(s);
      RETRY_EINTR(::close(s));
    }
  }


  void operator ()()
  {
#if defined(__linux__)
    struct ucred peer_cred;
    socklen_t crlen = sizeof(peer_cred);
    const int rc_peercred =
      sock_.getsockopt(SOL_SOCKET, SO_PEERCRED, &peer_cred, &crlen);

    const uid_t peer_uid = (rc_peercred == 0) ? peer_cred.uid : -1;
    const gid_t peer_gid = (rc_peercred == 0) ? peer_cred.gid : -1;

    TRACE(logger.debug(), "Peer's uid=" << peer_uid
	  << ", gid=" << peer_gid);
#endif

    union
    {
      void *ptr;
      char buffer[256];
    } data;

    union
    {
      struct cmsghdr cmsg;
      char buffer[CMSG_SPACE(sizeof(int))];
    } cdata;

    int flags = 0;

    try
    {
      while (true)
      {
	size_t len = sizeof(data.buffer);
	size_t clen = sizeof(cdata.buffer);
	const int rc_recvmsg =
	  sock_.recvmsg(data.buffer, len, cdata.buffer, clen, flags);
	TRACE(logger.debug(), "recvmsg returned rc=" << rc_recvmsg
	      << ", len=" << len << ", clen=" << clen
	      << ", flags=" << flags);

	if (len == 0)
	{
	  // connection closed
	  break;
	}

	const msg::Header &hdr = msg::Header_Proxy::get(data.buffer, len);
	(this->*dispatcher(hdr.msg_type()))(data.buffer, len);
      }
    }
    catch (const std::invalid_argument &iaexc)
    {
      TRACE(logger.error(), "got invalid argument exception");
    }

    sock_.shutdown(SHUT_RDWR);
  }

 protected:
  class Tcp_Connection
  {
   private:
    static Trace_Stream logger;

    Client &client_;
    Scripting::script_t script_;
    Fiber_Socket &sock_;
    Fiber_Resolver resolver_;

    size_t recvd_;
    char buffer_[Inetd_Server::buffer_size];

   protected:
    inline Async_Handler &handler()
    { return client_.handler(); }

    inline Fiber_Socket &client_sock()
    { return client_.sock(); }


    static int lua_receive(lua_State * const lua_interp)
    {
      Tcp_Connection & self =
	*static_cast<Tcp_Connection *>(lua_touserdata(lua_interp, lua_upvalueindex(1)));

      std::string delim;
      unsigned int timeout = 0;

      if (lua_isstring(lua_interp, 1))
      {
	delim.assign(lua_tostring(lua_interp, 1),
		     lua_strlen(lua_interp, 1));
      }

      if ((lua_gettop(lua_interp) >= 2) && lua_isnumber(lua_interp, 2))
      {
	timeout = lua_tointeger(lua_interp, 2);
      }

      if (!delim.empty())
      {
	TRACE(logger.debug(), "receive timeout=" << timeout
	      << ", received=" << self.recvd_);

	// receive socket data until we find delim
	size_t pos = 0;
	const char *iter = self.buffer_ + self.recvd_;
	      
	while (((iter = std::search(self.buffer_ + pos,
				    self.buffer_ + self.recvd_,
				    delim.begin(), delim.end()))
		== self.buffer_ + self.recvd_)
	       && self.recvd_ < sizeof(self.buffer_))
	{
	  size_t len = sizeof(self.buffer_) - self.recvd_;
	  if (self.sock_.recv(self.buffer_ + self.recvd_, len, 0, timeout) ||
	      !len)
	  {
	    break;
	  }

	  TRACE(logger.debug(), "received " << len);
	  pos = (self.recvd_ >= delim.length()) ?
	    self.recvd_ - delim.length() + 1 : 0;
	  self.recvd_ += len;
	}

	if (iter != self.buffer_ + self.recvd_)
	{
	  // found the delim
	  TRACE(logger.debug(), "found delimiter at " << (iter - self.buffer_));

	  lua_pushlstring(lua_interp, self.buffer_, iter - self.buffer_);

	  pos = iter - self.buffer_ + delim.length();
	  std::copy(self.buffer_ + pos, self.buffer_ + self.recvd_,
		    self.buffer_);
	  self.recvd_ -= pos;

	  return 1;
	}
      }

      return 0;
    }

    static int lua_send(lua_State * const lua_interp)
    {
      Tcp_Connection & self =
	*static_cast<Tcp_Connection *>(lua_touserdata(lua_interp, lua_upvalueindex(1)));

      if (lua_isstring(lua_interp, 1))
      {
	TRACE(logger.debug(), "send");

	self.sock_.send(lua_tostring(lua_interp, 1),
			lua_strlen(lua_interp, 1));
      }

      return 0;
    }


    void create_conn_table(lua_State * const lua_interp,
			   const char * const proto, const char * addr_str,
			   const int port)
    {
      lua_createtable(lua_interp, 0, 5); // conn

      lua_pushstring(lua_interp, "receive");
      lua_pushlightuserdata(lua_interp, this);
      lua_pushcclosure(lua_interp, &Tcp_Connection::lua_receive, 1);
      lua_rawset(lua_interp, -3);

      lua_pushstring(lua_interp, "send");
      lua_pushlightuserdata(lua_interp, this);
      lua_pushcclosure(lua_interp, &Tcp_Connection::lua_send, 1);
      lua_rawset(lua_interp, -3);

      lua_pushstring(lua_interp, "proto");
      lua_pushstring(lua_interp, proto);
      lua_rawset(lua_interp, -3);

      lua_pushstring(lua_interp, "proto");
      lua_pushstring(lua_interp, addr_str);
      lua_rawset(lua_interp, -3);

      lua_pushstring(lua_interp, "port");
      lua_rawseti(lua_interp, -2, port);
    }


   public:
    Tcp_Connection(Client &client, Scripting::script_t script,
		   Fiber_Socket &sock)
      : client_(client), script_(script), sock_(sock),
	resolver_(client.async_resolver()),
	recvd_(0)
    { }

    ~Tcp_Connection()
    { }


    void operator ()()
    {
      struct sockaddr_storage addr_storage;
      struct sockaddr &addr =
	reinterpret_cast<struct sockaddr &>(addr_storage);
      socklen_t addr_len = sizeof(addr_storage);
      if (const int rc_peername = sock_.getpeername(&addr, &addr_len))
      {
	const int last_error = errno;
	TRACE(logger.warn(), "unable to get peer name, errno=" << last_error);
	sock_.shutdown(SHUT_WR);

	size_t len = sizeof(buffer_);
	while (!sock_.recv(buffer_, len, 0) && len)
	{
	  len = sizeof(buffer_);
	}

	return;
      }

      const bool is_ipv4_mapped =
	(addr.sa_family == AF_INET6) &&
	IN6_IS_ADDR_V4MAPPED(&reinterpret_cast<const sockaddr_in6 &>(addr).sin6_addr);

      const struct in6_addr * const addr_ipv6 =
	(addr.sa_family == AF_INET6) ?
	&reinterpret_cast<const struct sockaddr_in6 &>(addr).sin6_addr : NULL;
      const struct in_addr * const addr_ipv4 =
	is_ipv4_mapped ?
	reinterpret_cast<const struct in_addr *>(&addr_ipv6->s6_addr[12]) :
	((addr.sa_family == AF_INET) ?
	 &reinterpret_cast<const struct sockaddr_in &>(addr).sin_addr : NULL);

      if (!addr_ipv4 && !addr_ipv6)
      {
	TRACE(logger.warn(), "unable to get peer address");
	sock_.shutdown(SHUT_WR);

	size_t len = sizeof(buffer_);
	while (!sock_.recv(buffer_, len, 0) && len)
	{
	  len = sizeof(buffer_);
	}

	return;
      }

      char addr_str[INET6_ADDRSTRLEN] = "";
      const bool have_addr_str =
	addr_ipv4 ?
	!inet_ntop(AF_INET, addr_ipv4, addr_str, sizeof(addr_str)) :
	!inet_ntop(AF_INET6, addr_ipv6, addr_str, sizeof(addr_str));

      TRACE(logger.debug(), "operator () ipv4=" << (addr_ipv4 != NULL)
	    << ", ipv6=" << (addr_ipv6 != NULL) << ", addr=" << addr_str);

      std::list<std::string> dnsbl;
      {
	Scripting::interp_t lua_handle = script_->get();
	lua_State * const lua_interp = lua_handle->get();

	// get dnsbl list
	lua_getfield(lua_interp, -1, "dnsbl");
	lua_pushvalue(lua_interp, -2); // self
	lua_pushstring(lua_interp, addr_ipv4 ? "IPv4" : "IPv6"); // proto
	lua_pushstring(lua_interp, have_addr_str ? addr_str : ""); // addr
	lua_pushinteger(lua_interp, 0); // port

	if (!lua_pcall(lua_interp, 4, 1, 0) &&
	    lua_istable(lua_interp, -1))
	{
	  lua_pushnil(lua_interp);
	  while (lua_next(lua_interp, -2))
	  {
	    const std::string entry = lua_tostring(lua_interp, -1);
	    TRACE(logger.debug(), "got dnsbl entry \"" << entry << '"');
	    dnsbl.push_back(entry);

	    lua_pop(lua_interp, 1);
	  }
	}
	else
	{
	  TRACE(logger.warn(), "get dnsbl failed: "
		<< lua_tostring(lua_interp, -1));
	}
	lua_pop(lua_interp, 1);
      }

      Dnsbl_Resolver dnsbl_res(resolver_.async());
      for (std::list<std::string>::const_iterator iter = dnsbl.begin();
	   iter != dnsbl.end();
	   ++iter)
      {
	const std::string &blname = *iter;

	std::ostringstream dnsbl_ostr;
	if (addr_ipv4)
	{
	  const unsigned char * const addr_bytes =
	    reinterpret_cast<const unsigned char *>(addr_ipv4);
	  dnsbl_ostr << int(addr_bytes[3]) << '.'
		     << int(addr_bytes[2]) << '.'
		     << int(addr_bytes[1]) << '.'
		     << int(addr_bytes[0]) << '.' << blname;
	}
	else if (addr_ipv6)
	{
	  const unsigned char * const addr_bytes =
	    reinterpret_cast<const unsigned char *>(addr_ipv6);
	  std::ostringstream dnsbl_ostr;
	  dnsbl_ostr << std::hex;
	  for (int idx = 0x0f; idx >= 0; idx--)
	  {
	    dnsbl_ostr << int(addr_bytes[idx] & 0x0f) << '.'
		       << int(addr_bytes[idx] & 0xf0) << '.';
	  }
	  dnsbl_ostr << blname;
	}

	const std::string &dnsbl_addr = dnsbl_ostr.str();
	if (!dnsbl_addr.empty())
	{
	  TRACE(logger.debug(), "operator () dnsbl lookup " << dnsbl_addr);
	  dnsbl_res.add(dnsbl_addr, blname);
	}
      }

      Reverse_Resolver reverse(resolver_);
      std::string rdns;
      if (addr_ipv4)
      {
	rdns = reverse.resolve(addr_ipv4);
      }
      else if (addr_ipv6)
      {
	rdns = reverse.resolve(addr_ipv6);
      }

      typedef Dnsbl_Resolver::Dnsbl_Map Dnsbl_Map;
      const Dnsbl_Map &dnsbl_records = dnsbl_res.complete();

      // check accept
      Scripting::interp_t lua_handle = script_->get();
      lua_State * const lua_interp = lua_handle->get();

      create_conn_table(lua_interp,
			addr_ipv4 ? "IPv4" : "IPv6",
			have_addr_str ? addr_str : "",
			0);

      lua_getfield(lua_interp, -2, "check_accept");
      lua_pushvalue(lua_interp, -3); // self
      lua_pushvalue(lua_interp, -3); // conn

      lua_createtable(lua_interp, 0, dnsbl_records.size()); // dnsbl
      for (Dnsbl_Map::const_iterator
	     iter = dnsbl_records.begin(),
	     end = dnsbl_records.end();
	   iter != end; ++iter)
      {
	lua_pushstring(lua_interp, iter->first.c_str());

	lua_createtable(lua_interp, iter->second.size(), 0);
	unsigned int idx = 0;
	for (Dnsbl_Map::mapped_type::const_iterator
	       liter = iter->second.begin(),
	       end = iter->second.end();
	     liter != end; ++liter, ++idx)
	{
	  lua_pushstring(lua_interp, liter->c_str());
	  lua_rawseti(lua_interp, -2, idx);
	}

	lua_rawset(lua_interp, -3);
      }

      if (!lua_pcall(lua_interp, 3, 1, 0))
      {
	if (lua_toboolean(lua_interp, -1))
	{
	  TRACE(logger.debug(), "accepting connection");

	  union
	  {
	    struct cmsghdr cmsg;
	    char buffer[CMSG_SPACE(sizeof(int))];
	  } cdata;

	  cdata.cmsg.cmsg_len = CMSG_LEN(sizeof(int));
	  cdata.cmsg.cmsg_level = SOL_SOCKET;
	  cdata.cmsg.cmsg_type = SCM_RIGHTS;

	  *((int *) CMSG_DATA(&cdata.cmsg)) = sock_.socket();

	  msg::Accept_Event_Builder accept_event;
	  accept_event.rdns(rdns);
	  client_sock().sendmsg(accept_event.iov(), accept_event.iovlen(),
				cdata.buffer, sizeof(cdata), MSG_EOR);
	}
	else
	{
	  TRACE(logger.info(), "refusing connection");
	  TRACE(logger.debug(), "shutting down client socket");
	  sock_.shutdown(SHUT_WR);

	  size_t len = sizeof(buffer_);
	  while (!sock_.recv(buffer_, len, 0) && len)
	  {
	    len = sizeof(buffer_);
	  }
	}
      }
      else
      {
	TRACE(logger.warn(), "callback failed: "
	      << lua_tostring(lua_interp, -1));
      }

      lua_pop(lua_interp, 2);
    }
  };

  void handle_connection(Scripting::script_t script, Fiber_Socket *_sock)
  {
    std::unique_ptr<Fiber_Socket> sock(_sock);
    Tcp_Connection(*this, script, *_sock)();
  }

  void on_accept(const Scripting::script_t &script, const socket_t sock,
		 const unsigned long error)
  {
    TRACE(logger.debug(), "on_accept(error=" << error << ')');

    if (!error)
    {
      Fiber_Handler::create_fiber([this, &script, sock] ()
				  { handle_connection(script, new Fiber_Socket(handler(), async_timer(), sock)); });
    }
  }

  void hello_req(const char *buffer, const size_t len)
  {
    const msg::Hello_Request_Proxy &hello =
      msg::Hello_Request_Proxy::get(buffer, len);
    TRACE(logger.debug(), "hello msg_type=" << hello.msg_type()
	  << ", version=" << hello.version());
  }


  static void bind_addresses(const Scripting::script_t &script,
			     const std::function<bool (Scripting::script_t, socket_t)> &cb)
  {
    std::list<std::pair<std::string, unsigned short> > bindl;

    Scripting::interp_t lua_handle = script->get();
    lua_State * const lua_interp = lua_handle->get();

    lua_getfield(lua_interp, -1, "bind");
    lua_pushvalue(lua_interp, -2);

    if (!lua_pcall(lua_interp, 1, 1, 0) && lua_istable(lua_interp, -1))
    {
      lua_pushnil(lua_interp);
      while (lua_next(lua_interp, -2))
      {
	if (lua_istable(lua_interp, -1))
	{
	  lua_pushnil(lua_interp);
	  if (lua_next(lua_interp, -2))
	  {
	    const std::string hostname = lua_tostring(lua_interp, -1);
	    lua_pop(lua_interp, 1);

	    if (lua_next(lua_interp, -2))
	    {
	      const unsigned short port = lua_tointeger(lua_interp, -1);
	      bindl.push_back(std::make_pair(hostname, port));

	      lua_pop(lua_interp, 2);
	    }
	  }
	}

	lua_pop(lua_interp, 1);
      }
    }
    else
    {
      TRACE(logger.warn(), "get bind failed: "
	    << lua_tostring(lua_interp, -1));
    }

    lua_pop(lua_interp, 1);

    for (std::list<std::pair<std::string, unsigned short> >::const_iterator iter = bindl.begin();
	 iter != bindl.end();
	 ++iter)
    {
      const std::string &hostname = iter->first;
      std::ostringstream port_str;
      port_str << iter->second;

      TRACE(logger.debug(), "bind_addresses addrinfo for hostname=" << hostname
	    << ", port=" << iter->second);

      Address_Info ainfo(hostname.c_str(), port_str.str().c_str(),
			 AI_PASSIVE | AI_ADDRCONFIG | AI_NUMERICHOST,
			 AF_UNSPEC, SOCK_STREAM);
      if (ainfo)
      {
	for (Address_Info::const_iterator iter = ainfo.begin();
	     iter != ainfo.end();
	     ++iter)
	{
	  struct sockaddr *addr = iter->ai_addr;
	  const socket_t s = ::socket(addr->sa_family, SOCK_STREAM, 0);

	  const int res_bind = ::bind(s, addr, iter->ai_addrlen);
	  if (!res_bind)
	  {
	    if (cb(script, s))
	    {
	      break;
	    }
	  }
	  else
	  {
	    const int last_error = errno;
	    TRACE(logger.error(), "bind failed, errno=" << last_error);
	    RETRY_EINTR(::close(s));
	  }
	}
      }
      else
      {
	TRACE(logger.warn(), "unable to resolve "
	      << hostname << ", error=" << ainfo.error());
      }
    }
  }


  static bool bind_socket_cb(std::list<socket_t> &sockl,
			     const Scripting::script_t &script,
			     const socket_t sock)
  {
    sockl.push_back(sock);

    return true;
  }

  void bind_req(const char *buffer, const size_t len)
  {
    const msg::Bind_Request_Proxy &bind_req =
      msg::Bind_Request_Proxy::get(buffer, len);
    TRACE(logger.debug(), "bind_req msg_type=" << bind_req.msg_type()
	  << ", service=" << bind_req.service());

    std::list<socket_t> sockl;
    Scripting::script_t script = Scripting::get(bind_req.service());
    bind_addresses(script, [&sockl] (const Scripting::script_t &script, const socket_t sock) { return bind_socket_cb(sockl, script, sock); });

    std::vector<char> cmsgbuf;
    cmsgbuf.resize(CMSG_SPACE(sizeof(int)) - sizeof(int));

    for (std::list<socket_t>::const_iterator iter = sockl.begin();
	 iter != sockl.end();
	 ++iter)
    {
      const size_t off = cmsgbuf.size();
      cmsgbuf.resize(cmsgbuf.size() + sizeof(int));
      reinterpret_cast<int &>(cmsgbuf[off]) = *iter;
    }

    cmsghdr &cmsg = reinterpret_cast<cmsghdr &>(cmsgbuf[0]);
    cmsg.cmsg_len = CMSG_LEN(sockl.size() * sizeof(int));
    cmsg.cmsg_level = SOL_SOCKET;
    cmsg.cmsg_type = SCM_RIGHTS;

    msg::Bind_Reply_Builder bind_reply;
    bind_reply.error(0);
    sock_.sendmsg(bind_reply.iov(), bind_reply.iovlen(),
		  &cmsgbuf[0], cmsgbuf.size(), MSG_EOR);

    while (!sockl.empty())
    {
      const socket_t s = sockl.back();
      sockl.pop_back();
      RETRY_EINTR(::close(s));
    }
  }


  bool listen_socket_cb(const Scripting::script_t &script, const socket_t sock)
  {
    listeners_.push_back(sock);
    server_.handler().attach_listener(sock, [this, &script] (const socket_t sock, const unsigned long error) { return on_accept(script, sock, error); });

    return true;
  }

  void listen_req(const char *buffer, const size_t len)
  {
    const msg::Listen_Request_Proxy &listen_req =
      msg::Listen_Request_Proxy::get(buffer, len);
    TRACE(logger.debug(), "listen_req msg_type=" << listen_req.msg_type()
	  << ", service=" << listen_req.service());

    Scripting::script_t script = Scripting::get(listen_req.service());
    bind_addresses(script, [this] (const Scripting::script_t &script, const socket_t sock) { return listen_socket_cb(script, sock); });
  }
};


Inetd_Server::Client::Msg_Dispatcher::Entry
Inetd_Server::Client::dispatch_table[] =
{
  { msg::Hello_Request::MSG, &Inetd_Server::Client::hello_req },
  { msg::Bind_Request::MSG, &Inetd_Server::Client::bind_req },
  { msg::Listen_Request::MSG, &Inetd_Server::Client::listen_req }
};
const Inetd_Server::Client::Msg_Dispatcher Inetd_Server::Client::dispatcher(
  dispatch_table, sizeof(dispatch_table) / sizeof(*dispatch_table));

Trace_Stream logger("server");
Trace_Stream Inetd_Server::logger("Inetd_Server");
Trace_Stream Inetd_Server::Client::logger("Inetd_Server::Client");
Trace_Stream Inetd_Server::Client::Tcp_Connection::logger("Inetd_Server::Client::Tcp_Connection");


void Inetd_Server::handle_connection(Fiber_Socket *_sock)
{
  std::unique_ptr<Fiber_Socket> sock(_sock);
  Client client(*this, *_sock);

  TRACE(logger.debug(), "handle_connection");
  client();
}


int main(int argc, char *argv[])
{
  Trace_Stream::set_global_level(TRACE_DEBUG);

  //trace_file_writer.output_level(TRACE_INFO);
  //trace_console_writer.output_level(TRACE_INFO);

#if defined(_WIN32)
  // initialize WinSock2
  WSADATA info;
  if (WSAStartup(MAKEWORD(2,0), &info) != 0)
  {
    TRACE(logger.critical(), "can't initialize WinSock2");
    return 1;
  }
#endif

  Async_Handler handler(3, Fiber_Handler::get_wrapper());

  Inetd_Server inetd(handler, "/tmp/nginetd.sock");

#if defined(_WIN32)
  Sleep(3600000);
#else
  sleep(3600);
#endif

  return 0;
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
