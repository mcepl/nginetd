/*	-*- C++ -*-
 * Copyright (C) 2009-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ASYNC_TIMER__H
#define INCLUDED__ASYNC_TIMER__H

#include "async_handler.h"
#include "socket.h"
#include "trace.h"

#include <functional>
#include <list>
#include <map>
#include <mutex>

#include <time.h>

class Async_Timer
{
  static Trace_Stream logger;

  friend class Async_Handler;
  friend class Async_Worker;

  struct Info;

 public:
  typedef Info *reg_t;
  typedef std::function<void (reg_t)> handler_t;

 private:
  typedef std::multimap<time_t, reg_t> queue_t;

  struct Info
  {
    explicit Info(handler_t handler)
      : handler_(handler), scheduled_(false)
    { }

    handler_t handler_;
    queue_t::iterator iter_;
    bool scheduled_;
  };

 public:
  explicit Async_Timer(Async_Handler &handler);
  Async_Timer(Async_Timer const &) = delete;
  Async_Timer &operator =(Async_Timer const &) = delete;
  ~Async_Timer();

  reg_t register_timer(const handler_t handler = handler_t());
  reg_t register_timer(const time_t t, const handler_t handler);
  void update_timer(const reg_t reg, const time_t t);
  void update_timer(const reg_t reg, const time_t t, const handler_t handler);
  void disable_timer(const reg_t reg);
  void unregister_timer(const reg_t reg);

 protected:
#if defined(__linux__)
  inline socket_t socket() const
  { return socket_; }
#elif defined(HAVE_KQUEUE)
  inline int timer_id() const
  { return timer_id_; }
#endif

#if defined(__linux__) || defined(HAVE_KQUEUE)
  inline const Event_Data *event_data() const
  { return &event_data_; }

  void on_timer() const;
#endif

 private:
#if defined(__linux__)
  const socket_t socket_;
#elif defined(HAVE_KQUEUE)
  Async_Handler &handler_;
  const int timer_id_;
#endif
  mutable std::mutex sync_;
  mutable queue_t pqueue_;

#if defined(__linux__) || defined(HAVE_KQUEUE)
  const Event_Data event_data_;
#endif
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
