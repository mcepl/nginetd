/*	-*- C++ -*-
 * Copyright (C) 2009, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__MSG_DISPATCHER__H
#define INCLUDED__MSG_DISPATCHER__H

#include <algorithm>
#include <stdexcept>

template<class MSG_T, class FUNC_PTR>
class Msg_Dispatcher
{
 public:
  struct Entry
  {
    MSG_T msg_type;
    FUNC_PTR func;
  };

 private:
  Entry * const table_;
  const size_t len_;

 protected:
  static inline bool lessthan(const Entry &left, const Entry &right)
  {
    return (left.msg_type < right.msg_type);
  }

 public:
  inline Msg_Dispatcher(Entry * const table, const size_t len)
    : table_(table), len_(len)
  {
    std::sort(table_, table_ + len, lessthan);
  }

  inline FUNC_PTR operator ()(const MSG_T msg_type) const
  {
    const Entry value = {msg_type, NULL};
    const Entry * const iter =
      std::lower_bound(table_, table_ + len_, value, lessthan);

    if ((iter != table_ + len_) && (iter->msg_type == msg_type))
    {
      return iter->func;
    }


    throw std::invalid_argument("msg_type");
  }
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
