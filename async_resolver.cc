/*	-*- C++ -*-
 * Copyright (C) 2009-2010, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "async_resolver.h"

#include <algorithm>
#include <cstring>
#include <memory>

#if defined(POSIX)
#include <netinet/in.h>
#endif


Trace_Stream Async_Resolver::logger("Async_Resolver");
Trace_Stream Async_Resolver::Request::logger("Async_Resolver::Request");


#if defined(HAVE_UDNS)
namespace
{
const bool udns_initialised = dns_init(NULL, false);

extern "C" void query_a_fn(dns_ctx *ctx, struct dns_rr_a4 *result,
			   void *data)
{
  const std::unique_ptr<std::function<void (const dns_rr_a4 *)> > cb(
    static_cast<std::function<void (const dns_rr_a4 *)> *>(data));
  (*cb)(result);
}

extern "C" void query_aaaa_fn(dns_ctx *ctx, struct dns_rr_a6 *result,
			      void *data)
{
  const std::unique_ptr<std::function<void (const dns_rr_a6 *)> > cb(
    static_cast<std::function<void (const dns_rr_a6 *)> *>(data));
  (*cb)(result);
}

extern "C" void query_ptr_fn(dns_ctx *ctx, struct dns_rr_ptr *result,
			     void *data)
{
  const std::unique_ptr<std::function<void (const dns_rr_ptr *)> > cb(
    static_cast<std::function<void (const dns_rr_ptr *)> *>(data));
  (*cb)(result);
}
}


Async_Resolver::Request::~Request()
{
  if (owned_)
  {
    TRACE(logger.debug(), "~Request type=" << type_);

    switch (type_)
    {
     case A:
      delete [] u.a.name;
      break;

     case AAAA:
      delete [] u.aaaa.name;
      break;

     case PTR_A:
     case PTR_AAAA:
      break;
    }
  }
}

Async_Resolver::Request Async_Resolver::Request::make_a(
  const char * const name,
  std::function<void (const dns_rr_a4 *)> * const cb,
  const bool search)
{
  Request req(A);
  const size_t l = std::strlen(name) + 1;
  req.u.a.name =
    static_cast<const char *>(std::memcpy(new char[l], name, l));
  req.u.a.cb = cb;
  req.u.a.search = search;

  return req;
}

Async_Resolver::Request Async_Resolver::Request::make_aaaa(
  const char * const name,
  std::function<void (const dns_rr_a6 *)> * const cb,
  const bool search)
{
  Request req(AAAA);
  const size_t l = std::strlen(name) + 1;
  req.u.aaaa.name =
    static_cast<const char *>(std::memcpy(new char[l], name, l));
  req.u.aaaa.cb = cb;
  req.u.aaaa.search = search;

  return req;
}

Async_Resolver::Request Async_Resolver::Request::make_ptr_a(
  const in_addr * const addr,
  std::function<void (const dns_rr_ptr *)> * const cb)
{
  Request req(PTR_A);
  req.u.ptr_a.addr = new in_addr(*addr);
  req.u.ptr_a.cb = cb;

  return req;
}

Async_Resolver::Request Async_Resolver::Request::make_ptr_aaaa(
  const in6_addr * const addr,
  std::function<void (const dns_rr_ptr *)> * const cb)
{
  Request req(PTR_AAAA);
  req.u.ptr_aaaa.addr = new in6_addr(*addr);
  req.u.ptr_aaaa.cb = cb;

  return req;
}

void Async_Resolver::Request::submit(dns_ctx * const ctx) const
{
  TRACE(logger.debug(), "submit type=" << type_);

  dns_query *query = NULL;

  switch (type_)
  {
   case Request::A:
    query = dns_submit_a4(ctx, u.a.name,
			  u.a.search ? 0 : DNS_NOSRCH,
			  &query_a_fn, u.a.cb);
    if (!query)
    {
      delete u.a.cb;
    }
    break;

   case Request::AAAA:
    query = dns_submit_a6(ctx, u.aaaa.name,
			  u.aaaa.search ? 0 : DNS_NOSRCH,
			  &query_aaaa_fn, u.a.cb);
    if (!query)
    {
      delete u.aaaa.cb;
    }
    break;

   case Request::PTR_A:
    query = dns_submit_a4ptr(ctx, u.ptr_a.addr,
			     &query_ptr_fn, u.ptr_a.cb);
    if (!query)
    {
      delete u.ptr_a.cb;
    }
    break;

   case Request::PTR_AAAA:
    query = dns_submit_a6ptr(ctx, u.ptr_aaaa.addr,
			     &query_ptr_fn, u.ptr_aaaa.cb);
    if (!query)
    {
      delete u.ptr_aaaa.cb;
    }
    break;
  }

  if (!query)
  {
    TRACE(logger.warn(), "submit failed");
  }
}


Async_Resolver::Async_Resolver(Async_Handler &handler)
  : handler_(handler), timer_(handler), ctx_(dns_new(NULL)),
    timer_reg_(timer_.register_timer([this] (Async_Timer::reg_t reg)
				     { on_timeout(reg); })),
    processing_(false), queued_recv_(false), queued_send_(false)
#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
  , event_data_(this)
#endif
{
  dns_open(ctx_);
  handler_.attach(*this);
}

Async_Resolver::~Async_Resolver()
{
  timer_.unregister_timer(timer_reg_);
  dns_free(ctx_);
}


void Async_Resolver::resolve_a(
  const char *name,
  const std::function<void (const dns_rr_a4 *)> &cb,
  bool search)
{
  std::unique_ptr<std::function<void (const dns_rr_a4 *)> > data(
    new std::function<void (const dns_rr_a4 *)>(cb));

  std::unique_lock<std::mutex> guard(processing_sync_);
  if (!processing_)
  {
    processing_ = true;
    guard.unlock();

    dns_query * const query =
      dns_submit_a4(ctx_, name, search ? 0 : DNS_NOSRCH, &query_a_fn,
		    data.get());

    guard.lock();
    processing_ = false;
    guard.unlock();

    if (!query)
    {
      TRACE(logger.warn(), "resolve_a dns_submit_a4 failed");
    }
    else
    {
      data.release();
    }

    process(false, query);
  }
  else
  {
    Request req = Request::make_a(name, data.get(), search);
    data.release();
    queued_req_.push_back(req);
  }
}

void Async_Resolver::resolve_aaaa(
  const char *name,
  const std::function<void (const dns_rr_a6 *)> &cb,
  bool search)
{
  std::unique_ptr<std::function<void (const dns_rr_a6 *)> > data(
    new std::function<void (const dns_rr_a6 *)>(cb));

  std::unique_lock<std::mutex> guard(processing_sync_);
  if (!processing_)
  {
    processing_ = true;
    guard.unlock();

    dns_query * const query =
      dns_submit_a6(ctx_, name, search ? 0 : DNS_NOSRCH, &query_aaaa_fn,
		    data.get());

    guard.lock();
    processing_ = false;
    guard.unlock();

    if (!query)
    {
      TRACE(logger.warn(), "resolve_aaaa dns_submit_a6 failed");
    }
    else
    {
      data.release();
    }

    process(false, query);
  }
  else
  {
    Request req = Request::make_aaaa(name, data.get(), search);
    data.release();
    queued_req_.push_back(req);
  }
}

void Async_Resolver::resolve_ptr(
  const struct in_addr *addr,
  const std::function<void (const dns_rr_ptr *)> &cb)
{
  std::unique_ptr<std::function<void (const dns_rr_ptr *)> > data(
    new std::function<void (const dns_rr_ptr *)>(cb));

  std::unique_lock<std::mutex> guard(processing_sync_);
  if (!processing_)
  {
    processing_ = true;
    guard.unlock();

    dns_query * const query =
      dns_submit_a4ptr(ctx_, addr, &query_ptr_fn, data.get());

    guard.lock();
    processing_ = false;
    guard.unlock();

    if (!query)
    {
      TRACE(logger.warn(), "resolve_ptr dns_submit_a4ptr failed");
    }
    else
    {
      data.release();
    }

    process(false, query);
  }
  else
  {
    Request req = Request::make_ptr_a(addr, data.get());
    data.release();
    queued_req_.push_back(req);
  }
}

void Async_Resolver::resolve_ptr(
  const struct in6_addr *addr,
  const std::function<void (const dns_rr_ptr *)> &cb)
{
  std::unique_ptr<std::function<void (const dns_rr_ptr *)> > data(
    new std::function<void (const dns_rr_ptr *)>(cb));

  std::unique_lock<std::mutex> guard(processing_sync_);
  if (!processing_)
  {
    processing_ = true;
    guard.unlock();

    dns_query * const query =
      dns_submit_a6ptr(ctx_, addr, &query_ptr_fn, data.get());

    guard.lock();
    processing_ = false;
    guard.unlock();

    if (!query)
    {
      TRACE(logger.warn(), "resolve_ptr dns_submit_a6ptr failed");
    }
    else
    {
      data.release();
    }

    process(false, query);
  }
  else
  {
    Request req = Request::make_ptr_aaaa(addr, data.get());
    data.release();
    queued_req_.push_back(req);
  }
}

void Async_Resolver::on_recv() const
{
  TRACE(logger.debug(), "on_recv");
  process(true, false);
}

void Async_Resolver::on_send() const
{
  TRACE(logger.debug(), "on_send");
  process(false, true);
}

void Async_Resolver::process(const bool need_recv, const bool need_send) const
{
  std::unique_lock<std::mutex> guard(processing_sync_);
  queued_recv_ |= need_recv;
  queued_send_ |= need_send;

  while (queued_recv_ || queued_send_ || !queued_req_.empty())
  {
    if (processing_)
    {
      return;
    }

    const bool do_recv = queued_recv_;
    queued_recv_ = false;
    const bool do_send = queued_send_ | !queued_req_.empty();
    queued_send_ = false;

    std::deque<Request> reqs;
    std::swap(reqs, queued_req_);

    processing_ = true;
    guard.unlock();

    for (std::deque<Request>::const_iterator
	   iter = reqs.begin(), end = reqs.end();
	 iter != end; ++iter)
    {
      const Request &req = *iter;
      req.submit(ctx_);
    }

    if (do_recv)
    {
      dns_ioevent(ctx_, 0);
    }

    if (do_send)
    {
      const int timeout = dns_timeouts(ctx_, -1, 0);
      TRACE(logger.debug(), "process timeout=" << timeout);

      if (timeout != -1)
      {
	timer_.update_timer(timer_reg_, timeout);
      }
      else
      {
	timer_.disable_timer(timer_reg_);
      }
    }

    guard.lock();
    processing_ = false;
  }
}

void Async_Resolver::on_timeout(Async_Timer::reg_t reg)
{
  TRACE(logger.debug(), "on_timeout");

  std::unique_lock<std::mutex> guard(processing_sync_);
  if (!processing_)
  {
    processing_ = true;
    guard.unlock();

    const int timeout = dns_timeouts(ctx_, -1, 0);
    TRACE(logger.debug(), "on_timeout timeout=" << timeout);

    if (timeout != -1)
    {
      timer_.update_timer(timer_reg_, timeout);
    }
    else
    {
      timer_.disable_timer(timer_reg_);
    }

    guard.lock();
    processing_ = false;
  }
  else
  {
    queued_send_ = true;
  }
}

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
