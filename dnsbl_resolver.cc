/*	-*- C++ -*-
 * Copyright (C) 2010-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "dnsbl_resolver.h"

#if defined(_WIN32)
#include <winsock2.h>
#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#endif


Trace_Stream Dnsbl_Resolver::logger("Dnsbl_Resolver");

Dnsbl_Resolver::Dnsbl_Resolver(Async_Resolver &resolver)
  : resolver_(resolver),
    fiber_(Fiber_Handler::current_fiber()),
    outstanding_(1)
{ }

Dnsbl_Resolver::~Dnsbl_Resolver()
{ }


void Dnsbl_Resolver::add(const std::string &dnsbl_addr,
			 const std::string &blname)
{
  std::lock_guard<std::mutex> guard(sync_);
  outstanding_++;

  resolver_.resolve_a(
    dnsbl_addr.c_str(),
    [this, &blname] (const dns_rr_a4 *rr) { complete_a(blname, rr); },
    false);
}

const Dnsbl_Resolver::Dnsbl_Map &Dnsbl_Resolver::complete()
{
  std::lock_guard<std::mutex> guard(sync_);
  if (--outstanding_)
  {
    Fiber_Handler::create_fiber([this] () { on_wait(); }, 2048);
  }

  return dnsbl_;
}

void Dnsbl_Resolver::on_wait()
{
  sync_.unlock();
}

void Dnsbl_Resolver::complete_a(const std::string &bl, const dns_rr_a4 *rr)
{
  TRACE(logger.debug(), "complete_a " << bl << ", " << outstanding_);

  const bool is_current_fiber = (fiber_ == Fiber_Handler::current_fiber());
  if (!is_current_fiber)
  {
    sync_.lock();
  }

  if (rr)
  {
    std::list<std::string> &l = dnsbl_[bl];

    for (int i = 0; i < rr->dnsa4_nrr; i++)
    {
      char buffer[INET6_ADDRSTRLEN];
      if (inet_ntop(AF_INET, &rr->dnsa4_addr[i], buffer, sizeof(buffer)))
      {
	TRACE(logger.debug(), "complete_a " << buffer);
	l.push_back(buffer);
      }
    }
  }

  free(const_cast<dns_rr_a4 *>(rr));
  outstanding_--;

  if (!is_current_fiber)
  {
    if (!outstanding_)
    {
      Fiber_Handler::schedule_fiber(fiber_);
    }
    else
    {
      sync_.unlock();
    }
  }
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
