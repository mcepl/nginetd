/*	-*- C++ -*-
 * Copyright (C) 2010-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__DNSBL_RESOLVER__H
#define INCLUDED__DNSBL_RESOLVER__H

#include "trace.h"

#include "async_handler.h"
#include "async_resolver.h"
#include "fiber_handler.h"

#include <map>
#include <mutex>
#include <string>


class Dnsbl_Resolver
{
 public:
  typedef std::map<std::string, std::list<std::string> > Dnsbl_Map;

 private:
  static Trace_Stream logger;

  Dnsbl_Map dnsbl_;
  Async_Resolver &resolver_;
  void * const fiber_;
  std::mutex sync_;
  unsigned int outstanding_;

 public:
  Dnsbl_Resolver(Async_Resolver &resolver);
  Dnsbl_Resolver(Dnsbl_Resolver const &) = delete;
  Dnsbl_Resolver &operator =(Dnsbl_Resolver const &) = delete;
  ~Dnsbl_Resolver();

  void add(const std::string &dnsbl_addr, const std::string &blname);

  const Dnsbl_Map &complete();

protected:
  void on_wait();
  void complete_a(const std::string &bl, const dns_rr_a4 *rr);
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
