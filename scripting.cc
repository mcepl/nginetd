/*	-*- C++ -*-
 * Copyright (C) 2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "scripting.h"

extern "C"
{
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}


Trace_Stream Scripting::logger("Scripting");

Scripting::interp_ptr_t Scripting::Script::create()
{
  TRACE(logger.debug(), "create interpreter for \"" << name_ << '"');

  interp_ptr_t state(lua_open());
  luaL_openlibs(state.get());

  if (!luaL_dofile(state.get(), (name_ + ".lua").c_str()) &&
      lua_istable(state.get(), -1))
  {
    return state;
  }

  return { };
}

void Scripting::Script::release(interp_ptr_t state)
{
  if (state)
  {
    std::lock_guard<std::mutex> guard(sync_);
    if (interp_list_.size() < max_idle_)
    {
      interp_list_.push_back(std::move(state));
    }
    else
    {
      TRACE(logger.debug(), "destroying interpreter for \"" << name_ << '"');
    }
  }
}

Scripting::Script::~Script()
{
  TRACE(logger.debug(), "~Script(" << name_ << ')');
}

Scripting::interp_t Scripting::Script::get()
{
  std::unique_lock<std::mutex> guard(sync_);

  interp_ptr_t interp_ptr;
  if (interp_list_.empty())
  {
    guard.unlock();
    interp_ptr = create();
  }
  else
  {
    interp_ptr = std::move(interp_list_.back());
    interp_list_.pop_back();
  }

  return interp_t(new Interp(*this, std::move(interp_ptr)));
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
