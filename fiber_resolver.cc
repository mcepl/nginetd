/*	-*- C++ -*-
 * Copyright (C) 2009-2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "fiber_resolver.h"

#include "fiber_handler.h"


Trace_Stream Fiber_Resolver::logger("Fiber_Resolver");

namespace
{
template<typename dns_rr>
class Resolve_Record_Info
{
  static Trace_Stream logger;

 public:
  inline Resolve_Record_Info()
    : completed_(false), result_(NULL)
  { }

  void complete()
  {
    if (!completed_)
    {
      Fiber_Handler::create_fiber([this] () { on_wait(); }, 2048);
    }
  }

  void on_complete(void *fiber, const dns_rr *rr)
  {
    TRACE(logger.debug(), "on_complete rr" << (rr ? "!=" : "==") << "NULL");
    result_ = rr;

    if (fiber == Fiber_Handler::current_fiber())
    {
      completed_ = true;
    }
    else
    {
      completed_sync_.lock();
      Fiber_Handler::schedule_fiber(fiber);
    }
  }

  std::mutex &sync()
  {
    return completed_sync_;
  }

  inline const dns_rr *result() const
  {
    return result_;
  }

 protected:
  void on_wait()
  {
    completed_sync_.unlock();
  }

 private:
  mutable std::mutex completed_sync_;
  bool completed_;
  const dns_rr *result_;
};

template<typename dns_rr>
Trace_Stream Resolve_Record_Info<dns_rr>::logger("Resolve_Record_Info");

#if defined(HAVE_UDNS)
typedef Resolve_Record_Info<dns_rr_ptr> Resolve_Ptr_Info;
typedef Resolve_Record_Info<dns_rr_a4> Resolve_A_Info;
typedef Resolve_Record_Info<dns_rr_a6> Resolve_AAAA_Info;
#endif
}


Fiber_Resolver::Fiber_Resolver(Async_Handler &handler)
  : async_instance_(new Async_Resolver(handler)),
    resolver_(*async_instance_)
{ }

Fiber_Resolver::Fiber_Resolver(Async_Resolver &resolver)
  : resolver_(resolver)
{ }

Fiber_Resolver::~Fiber_Resolver()
{ }


#if defined(HAVE_UDNS)
const dns_rr_a4 *Fiber_Resolver::resolve_a(const char *name)
{
  Resolve_A_Info info;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(info.sync());
    resolver_.resolve_a(name, [&info, fiber] (const dns_rr_a4 *rr)
			{ info.on_complete(fiber, rr); });
    info.complete();
  }

  return info.result();
}

const dns_rr_a6 *Fiber_Resolver::resolve_aaaa(const char *name)
{
  Resolve_AAAA_Info info;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(info.sync());
    resolver_.resolve_aaaa(name, [&info, fiber] (const dns_rr_a6 *rr)
			   { info.on_complete(fiber, rr); });
    info.complete();
  }

  return info.result();
}

const dns_rr_ptr *Fiber_Resolver::resolve_ptr(const struct in_addr *addr)
{
  Resolve_Ptr_Info info;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(info.sync());
    resolver_.resolve_ptr(addr, [&info, fiber] (const dns_rr_ptr *rr)
			  { info.on_complete(fiber, rr); });
    info.complete();
  }

  return info.result();
}

const dns_rr_ptr *Fiber_Resolver::resolve_ptr(const struct in6_addr *addr)
{
  Resolve_Ptr_Info info;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(info.sync());
    resolver_.resolve_ptr(addr, [&info, fiber] (const dns_rr_ptr *rr)
			  { info.on_complete(fiber, rr); });
    info.complete();
  }

  return info.result();
}
#endif


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
