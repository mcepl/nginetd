/*	-*- C++ -*-
 * Copyright (C) 2012-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_timer.h"
#include "../async_handler.h"

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <mutex>


namespace
{
  class Synced_Sequence
  {
   private:
    std::mutex seq_sync_;
    int seq_;

   public:
    inline Synced_Sequence()
      : seq_(0)
    { }

    inline int operator ++()
    {
      std::lock_guard<std::mutex> guard(seq_sync_);
      return ++seq_;
    }
  } next_timer_id;
}


Async_Timer::Async_Timer(Async_Handler &handler)
  : handler_(handler), timer_id_(++next_timer_id),
    event_data_(this)
{ }

Async_Timer::~Async_Timer()
{
  if (!pqueue_.empty())
  {
    handler_.detach(*this);
  }
}

Async_Timer::reg_t
Async_Timer::register_timer(const Async_Timer::handler_t handler)
{
  reg_t reg = new Info(handler);
  reg->scheduled_ = false;
  return reg;
}

Async_Timer::reg_t
Async_Timer::register_timer(const time_t t,
			    const Async_Timer::handler_t handler)
{
  reg_t reg = new Info(handler);
  const time_t now = time(NULL);

  std::lock_guard<std::mutex> guard(sync_);

  const queue_t::iterator qiter = pqueue_.insert(std::make_pair(now + t, reg));
  reg->iter_ = qiter;
  reg->scheduled_ = true;
  if (qiter == pqueue_.begin())
  {
    handler_.attach(*this, t);
  }

  return reg;
}

void Async_Timer::update_timer(const Async_Timer::reg_t reg,
			       const time_t t)
{
  const time_t now = time(NULL);
  std::lock_guard<std::mutex> guard(sync_);

  bool update_remove = false;
  if (reg->scheduled_)
  {
    update_remove = (reg->iter_ == pqueue_.begin());
    pqueue_.erase(reg->iter_);
  }

  const queue_t::iterator qiter = pqueue_.insert(std::make_pair(now + t, reg));
  reg->iter_ = qiter;
  reg->scheduled_ = true;
  const bool update_insert = (qiter == pqueue_.begin());

  if (update_remove || update_insert)
  {
    handler_.attach(*this, pqueue_.begin()->first - now);
  }
}

void Async_Timer::update_timer(const Async_Timer::reg_t reg,
			       const time_t t, const handler_t handler)
{
  const time_t now = time(NULL);
  std::lock_guard<std::mutex> guard(sync_);

  bool update_remove = false;
  if (reg->scheduled_)
  {
    update_remove = (reg->iter_ == pqueue_.begin());
    pqueue_.erase(reg->iter_);
  }

  const queue_t::iterator qiter = pqueue_.insert(std::make_pair(now + t, reg));
  reg->handler_ = handler;
  reg->iter_ = qiter;
  reg->scheduled_ = true;
  const bool update_insert = (qiter == pqueue_.begin());

  if (update_remove || update_insert)
  {
    handler_.attach(*this, pqueue_.begin()->first - now);
  }
}

void Async_Timer::disable_timer(const Async_Timer::reg_t reg)
{
  const time_t now = time(NULL);
  std::lock_guard<std::mutex> guard(sync_);

  bool update_remove = false;
  if (reg->scheduled_)
  {
    update_remove = (reg->iter_ == pqueue_.begin());
    pqueue_.erase(reg->iter_);
    reg->scheduled_ = false;
  }

  if (update_remove)
  {
    if (pqueue_.empty())
    {
      handler_.detach(*this);
    }
    else
    {
      handler_.attach(*this, pqueue_.begin()->first - now);
    }
  }
}

void Async_Timer::unregister_timer(const reg_t reg)
{
  const time_t now = ::time(NULL);
  std::lock_guard<std::mutex> guard(sync_);

  bool update_timer = false;
  if (reg->scheduled_)
  {
    update_timer = (reg->iter_ == pqueue_.begin());
    pqueue_.erase(reg->iter_);
  }
  delete reg;

  if (update_timer)
  {
    if (pqueue_.empty())
    {
      handler_.detach(*this);
    }
    else
    {
      handler_.attach(*this, pqueue_.begin()->first - now);
    }
  }
}

void Async_Timer::on_timer() const
{
  time_t now = ::time(NULL);
  TRACE(logger.debug(), "on_timer now=" << now);

  bool update_timer = false;
  std::unique_lock<std::mutex> guard(sync_);
  while (!pqueue_.empty() && (pqueue_.begin()->first <= now))
  {
    Info * const reg = pqueue_.begin()->second;
    reg->scheduled_ = false;
    pqueue_.erase(pqueue_.begin());
    update_timer = true;

    guard.unlock();
    reg->handler_(reg);
    guard.lock();
    now = ::time(NULL);
  }

  if (update_timer)
  {
    if (!pqueue_.empty())
    {
      handler_.attach(*this, pqueue_.begin()->first - now);
    }
  }
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
