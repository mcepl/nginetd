/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_worker.h"

#include "../async_handler.h"
#include "../async_resolver.h"
#include "../async_socket.h"
#include "../async_timer.h"

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <netinet/in.h>

#include <errno.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }


Async_Worker::Async_Worker(int kqueue_fd)
  : kqueue_fd_(kqueue_fd)
{ }


void Async_Worker::operator() ()
{
  bool cancel = false;
  const pthread_t tid = pthread_self();

  // this loop terminates when an OP_CANCEL is dequeued
  while (!cancel)
  {
    struct kevent events[16];

    TRACE(logger.debug(), "starting kevent tid=" << tid);
    int rc_kevent;
    RETRY_EINTR_RC(rc_kevent,
		   kevent(kqueue_fd_, NULL, 0, events,
			  sizeof(events) / sizeof(events[0]), NULL));
    TRACE(logger.debug(), "done kevent tid=" << tid);

    if (rc_kevent > 0)
    {
      for (int i = 0; i < rc_kevent; i++)
      {
	const int ev = events[i].filter;
	const Event_Data * const event_data =
	  static_cast<Event_Data *>(Event_Data::from_native_handle(events[i].udata));
	const Event_Data::Event_Operation op = event_data->operation;

        TRACE(logger.debug(), "got event " << ev << ", operation=" << op
	      << ", data=" << static_cast<const void *>(event_data));

	if (op == Event_Data::OP_CANCEL)
	{
	  cancel = true;
	  break;
	}
	else if (op == Event_Data::OP_ACCEPT)
	{
	  // TODO: accept
	  on_accept(event_data->accept.listener_socket,
		    event_data->accept.callback);
	}
	else if (op == Event_Data::OP_DATA)
	{
	  const Async_Socket * const async_socket =
	    event_data->io_linux.async_socket;

	  if (ev == EVFILT_READ)
	  {
	    async_socket->on_recv();
	  }
	  else if (ev == EVFILT_WRITE)
	  {
	    async_socket->on_send();
	  }
	}
	else if (op == Event_Data::OP_DNS)
	{
	  const Async_Resolver * const async_resolver =
	    event_data->dns_linux.async_resolver;

	  if (ev == EVFILT_READ)
	  {
	    async_resolver->on_recv();
	  }
	  else if (ev == EVFILT_WRITE)
	  {
	    async_resolver->on_send();
	  }
	}
	else if (op == Event_Data::OP_TIMER)
	{
	  event_data->timer.timer->on_timer();
	}
	else
	{
	  TRACE(logger.fatal(), "got invalid operation " << op);
	}
      }
    }
    else
    {
      TRACE(logger.error(), "kevent failed error=" << errno);
    }
  }
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
