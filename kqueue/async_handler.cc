/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_handler.h"

#include "../async_resolver.h"
#include "../async_socket.h"
#include "../async_timer.h"
#include "../async_worker.h"

#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <errno.h>
#include <pthread.h>
#include <ucontext.h>
#include <unistd.h>

#include <algorithm>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }


Async_Handler::Async_Handler(int nr_workers,
			     std::function<void (std::function<void ()> const &)> const &func)
  : kqueue_fd_(kqueue())
{
  if (kqueue_fd_ < 0)
  {
    TRACE(logger.critical(), "failed to create kqueue");
  }
  else
  {
    ::pipe(pipe_fds_);
    static const Event_Data cancel_event_data(Event_Data::OP_CANCEL);
    struct kevent const event = {
      pipe_fds_[0], EVFILT_READ, EV_ADD, 0, 0,
      cancel_event_data.native_handle()
    };
    kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);

    workers_.reserve(nr_workers);
    worker_threads_.reserve(nr_workers);

    for (int i = 0; i < nr_workers; i++)
    {
      workers_.push_back(Async_Worker(kqueue_fd_));
      worker_threads_.emplace_back([this, func] ()
				   {
				     if (func)
				     {
				       func([this] () { return workers_.back()(); });
				     }
				     else
				     {
				       workers_.back()();
				     }
				   });
    }
  }
}


Async_Handler::~Async_Handler()
{
  RETRY_EINTR(::close(pipe_fds_[1]));
  TRACE(logger.debug(), "posted CANCEL for all worker threads");

  std::for_each(worker_threads_.begin(), worker_threads_.end(),
		[] (std::thread &t) { t.join(); });
  TRACE(logger.debug(), "all worker threads have terminated");

  for (auto iter = attached_listeners_.cbegin();
       iter != attached_listeners_.cend();
       ++iter)
  {
    struct Event_Data * const event_data = iter->second;

    if (event_data->operation != Event_Data::OP_ACCEPT)
    {
      TRACE(logger.fatal(), "expected operation==OP_ACCEPT");
    }

    delete event_data->accept.callback;
    delete event_data;
  }

  RETRY_EINTR(::close(kqueue_fd_));
  RETRY_EINTR(::close(pipe_fds_[0]));
}


std::unique_ptr<Async_Socket> Async_Handler::attach(socket_t socket)
{
  const int rc_nonblock = set_socket_nonblocking(socket);
  if (rc_nonblock < 0)
  {
    TRACE(logger.error(), "attach unable to set non-blocking I/O for fd="
	  << socket);
  }

  std::unique_ptr<Async_Socket> async_socket;

  {
    std::lock_guard<std::mutex> guard(socket_pool_sync_);
    if (socket_pool_.empty())
    {
      async_socket.reset(new Async_Socket(*this));
    }
    else
    {
      async_socket = std::move(socket_pool_.front());
      socket_pool_.pop_front();
    }
  }

  async_socket->init(socket);

  struct kevent const events[] = {
    { socket, EVFILT_READ, EV_ADD | EV_DISABLE | EV_CLEAR, 0, 0,
      async_socket->event_data()->native_handle() },
    { socket, EVFILT_WRITE, EV_ADD | EV_DISABLE | EV_CLEAR, 0, 0,
      async_socket->event_data()->native_handle() }
  };
  const int rc =
    kevent(kqueue_fd_, events, sizeof(events)/sizeof(*events), NULL, 0, NULL);

  return async_socket;
}

void Async_Handler::register_recv(const Async_Socket &sock, bool enable)
{
  struct kevent const event = {
    sock.socket(), EVFILT_READ,
    EV_ADD | EV_CLEAR | (enable ? EV_ENABLE : EV_DISABLE), 0, 0,
    sock.event_data()->native_handle()
  };
  const int rc =
    kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);
}

void Async_Handler::register_send(const Async_Socket &sock, bool enable)
{
  struct kevent const event = {
    sock.socket(), EVFILT_WRITE,
    EV_ADD | EV_CLEAR | (enable ? EV_ENABLE : EV_DISABLE), 0, 0,
    sock.event_data()->native_handle()
  };
  const int rc =
    kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);
}

void Async_Handler::detach(std::unique_ptr<Async_Socket> async_socket)
{
  struct kevent const events[] = {
    { async_socket->socket(), EVFILT_READ, EV_DELETE, 0, 0, 0 },
    { async_socket->socket(), EVFILT_WRITE, EV_DELETE, 0, 0, 0 } 
  };
  const int rc =
    kevent(kqueue_fd_, events, sizeof(events)/sizeof(*events), NULL, 0, NULL);

  release(std::move(async_socket));
}

void Async_Handler::attach(const Async_Resolver &async_resolver)
{
  struct kevent const events[] = {
    { async_resolver.socket(), EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0,
      async_resolver.event_data()->native_handle() },
    { async_resolver.socket(), EVFILT_WRITE, EV_ADD | EV_CLEAR, 0, 0,
      async_resolver.event_data()->native_handle() }
  };
  const int rc =
    kevent(kqueue_fd_, events, sizeof(events)/sizeof(*events), NULL, 0, NULL);
}

void Async_Handler::attach(const Async_Timer &timer, unsigned int timeout)
{
  struct kevent const event = {
    timer.timer_id(), EVFILT_TIMER,
    EV_ADD | EV_ONESHOT, 0, timeout * 1000,
    timer.event_data()->native_handle()
  };
  const int rc =
    kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);
}

bool Async_Handler::detach(const Async_Timer &async_timer)
{
  struct kevent const event = {
    async_timer.timer_id(), EVFILT_TIMER, EV_DELETE, 0, 0, 0
  };
  const int rc =
    kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);

  return (rc == 0);
}


void
Async_Handler::attach_listener(socket_t listener_socket,
			       const accept_handler &cb)
{
  const int rc_nonblock = set_socket_nonblocking(listener_socket);
  if (rc_nonblock < 0)
  {
    TRACE(logger.error(), "unable to set non-blocking I/O");
  }

  int rc_listen = ::listen(listener_socket, SOMAXCONN);
  if (rc_listen)
  {
    TRACE(logger.error(), "listen failed: " << errno);
  }

  TRACE(logger.debug(), "attach_listener fd=" << listener_socket);

  struct Event_Data * const event_data = new Event_Data;
  event_data->operation = Event_Data::OP_ACCEPT;
  event_data->accept.listener_socket = listener_socket;
  event_data->accept.callback = new accept_handler(cb);

  {
    std::lock_guard<std::mutex> guard(attached_listeners_sync_);
    attached_listeners_[listener_socket] = event_data;
  }

  struct kevent const event = {
    listener_socket, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0,
    event_data->native_handle()
  };
  const int rc =
    kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);
}

bool
Async_Handler::detach_listener(socket_t listener_socket)
{
  struct Event_Data *event_data = NULL;

  {
    std::lock_guard<std::mutex> guard(attached_listeners_sync_);

    Attached_Listeners_Map::iterator iter =
      attached_listeners_.find(listener_socket);
    if (iter != attached_listeners_.end())
    {
      event_data = iter->second;
      attached_listeners_.erase(iter);
    }
  }

  if (event_data != NULL)
  {
    if (event_data->operation != Event_Data::OP_ACCEPT)
    {
      TRACE(logger.fatal(), "expected operation==OP_ACCEPT");
    }

    struct kevent const event = {
      event_data->accept.listener_socket, 0, EV_DELETE, 0, 0, 0 };
    const int rc =
      kevent(kqueue_fd_, &event, 1, NULL, 0, NULL);

    // TODO: think about race condition
    delete event_data->accept.callback;
    delete event_data;
    return true;
  }

  return false;
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
