#	-*- Makefile -*-
# Copyright (C) 2003-2007, Christof Meerwald
# http://cmeerw.org
#

# Configuration Defaults
PLATFORM?=posix
SUFFIX_OBJ?=.o
SUFFIX_PICOBJ?=.lo
SUFFIX_EXE?=
NAME_SO?=lib$(1).so
NAME_EXE=$(1)$(SUFFIX_EXE)

DIR_LUA?=/usr/include/lua5.1
LIB_LUA?=lua5.1

# the compiler to use
CXX?=g++
CFLAGS_COMPILER?=-pthread -std=c++11
CFLAGS_CONFIG?=-O -g

DEFINES?=POSIX HAVE_UDNS HAVE_EPOLL

RULE_OBJ?=$(CXX) -c $(CFLAGS) -o '$(2)' '$(1)'

RULE_PICOBJ?=$(CXX) -c $(CFLAGS) -fPIC -o '$(2)' '$(1)'

RULE_SO?=$(CXX) -shared -o '$(2)' $(foreach file,$(1),'$(file)') \
	$(LDFLAGS) $(foreach dir,$(LIBPATH),-L'$(dir)') \
	$(CFLAGS) $(foreach lib,$(3),-l'$(lib)')

RULE_EXE?=$(CXX) -o '$(2)' $(foreach file,$(1),'$(file)') \
	$(LDFLAGS) $(foreach dir,$(LIBPATH),-L'$(dir)') \
	$(CFLAGS) $(foreach lib,$(3),-l'$(lib)')
