/*	-*- C++ -*-
 * Copyright (C) 2003-2007, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_socket.h"

#include "../async_handler.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>


Async_Socket::Async_Socket(Async_Handler &handler, socket_t socket)
  : handler_(handler), socket_(socket)
{
  handler_.attach(*this);
}

Async_Socket::~Async_Socket()
{
  ::closesocket(socket_);
}

void Async_Socket::_send(const char *data, size_t len, int flags,
			 Async_Handler::send_handler *cb)
{
  OVERLAPPED_PLUS *ov_plus = new OVERLAPPED_PLUS;
  ov_plus->operation = OVERLAPPED_PLUS::OP_SEND;
  ov_plus->send_win32.socket = socket_;
  ov_plus->send_win32.callback = cb;

  WSABUF send_buffer = {len, const_cast<char *>(data)};
  DWORD bytes_transferred;

  const HRESULT res_send = WSASend(socket_, &send_buffer, 1,
				   &bytes_transferred, flags,
				   &ov_plus->overlapped, NULL);

  if (res_send == SOCKET_ERROR)
  {
    const DWORD last_error = WSAGetLastError();
    if (last_error != WSA_IO_PENDING)
    {
      delete ov_plus;

      TRACE(logger.error(), "initiate_tcp_write: WSASend error="
	    << last_error);

      // TODO: throw exception
    }
  }
}


void Async_Socket::recv(char *buffer, size_t len, int flags,
			const Async_Handler::recv_handler &cb)
{
  OVERLAPPED_PLUS *ov_plus = new OVERLAPPED_PLUS;
  ov_plus->operation = OVERLAPPED_PLUS::OP_RECV;
  ov_plus->recv_win32.socket = socket_;
  ov_plus->recv_win32.callback = new Async_Handler::recv_handler(cb);

  DWORD bytes_transferred;
  DWORD wsaflags = flags;
  WSABUF receive_buffer = {len, buffer};

  TRACE(logger.debug(), "initiating WSARecv");
  const HRESULT res_send = WSARecv(socket_, &receive_buffer, 1,
				   &bytes_transferred, &wsaflags,
				   &ov_plus->overlapped, NULL);

  if (res_send == SOCKET_ERROR)
  {
    const DWORD last_error = WSAGetLastError();
    if (last_error != WSA_IO_PENDING)
    {
      delete ov_plus;

      TRACE(logger.error(), "initiate_tcp_write: WSASend error="
	    << last_error);

      // TODO: throw exception
    }
  }
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
