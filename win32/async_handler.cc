/*	-*- C++ -*-
 * Copyright (C) 2003-2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_handler.h"

#include "../async_socket.h"
#include "../async_worker.h"

#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>


Async_Handler::Async_Handler(int nr_workers)
  : completion_port_(CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0))
{
  if (!completion_port_)
  {
    TRACE(logger.critical(), "failed to create I/O completion port");
  }
  else
  {
    main_fiber(ConvertThreadToFiber(NULL));
    workers_.reserve(nr_workers);

    for (int i = 0; i < nr_workers; i++)
    {
      workers_.push_back(new Async_Worker(completion_port_));
      worker_threads_.create_thread(boost::bind(&Async_Worker::operator(),
						workers_.back()));
    }
  }
}


Async_Handler::~Async_Handler()
{
  for (size_t i = 0; i < workers_.size(); i++)
  {
    OVERLAPPED_PLUS *ov_plus = new OVERLAPPED_PLUS();

    ov_plus->operation = OVERLAPPED_PLUS::OP_CANCEL;

    PostQueuedCompletionStatus(completion_port_, 0, 0,
			       &ov_plus->overlapped);
  }
  TRACE(logger.debug(), "posted CANCEL for all Completion Port worker threads");

  worker_threads_.join_all();
  TRACE(logger.debug(), "all Completion Port worker threads have terminated");

  while (!workers_.empty())
  {
    delete workers_.back();
    workers_.pop_back();
  }

  CloseHandle(completion_port_);
}


void Async_Handler::worker_thread_wrapper(Async_Handler *handler,
					  Async_Worker *worker)
{
  main_fiber(ConvertThreadToFiber(NULL));

  (*worker)();
}


void Async_Handler::attach(const Async_Socket &async_socket)
{
  CreateIoCompletionPort(reinterpret_cast<HANDLE>(async_socket.socket()),
			 completion_port_, 0, 0);
}


void
Async_Handler::attach_listener(socket_t listener_socket,
			       const accept_handler &cb)
{
  int res_listen = listen(listener_socket, 100);
  if (res_listen)
  {
    TRACE(logger.error(), "listen failed: " << WSAGetLastError());
  }

  if (!CreateIoCompletionPort(reinterpret_cast<HANDLE>(listener_socket),
			      completion_port_, 0, 0))
  {
    TRACE(logger.error(), "CreateIoCompletionPort, error: " << GetLastError());
  }

  OVERLAPPED_PLUS *ov_plus = new OVERLAPPED_PLUS();

  ov_plus->operation = OVERLAPPED_PLUS::OP_ACCEPT;
  ov_plus->accept.listener_socket = listener_socket;
  ov_plus->accept.callback = new accept_handler(cb);
  ov_plus->accept_win32.socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  ov_plus->accept_win32.output_buffer =
    new char[2*(sizeof(sockaddr_in) + 16)];

  if (!AcceptEx(listener_socket,
		ov_plus->accept_win32.socket,
		ov_plus->accept_win32.output_buffer,
		0, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16,
		&ov_plus->accept_win32.bytes_received,
		&ov_plus->overlapped))
  {
    const DWORD last_error = WSAGetLastError();
    if (last_error != WSA_IO_PENDING)
    {
      TRACE(logger.error(), "AcceptEx failed: " << last_error);
    }
  }
}


struct fiber_wrapper_param_t
{
  const boost::function0<void> *func;
  const Async_Handler *handler;
};

static void CALLBACK start_fiber_wrapper(PVOID ptr)
{
  const fiber_wrapper_param_t &param(*static_cast<fiber_wrapper_param_t *>(ptr));

  (*param.func)();

  param.handler->destroy_fiber();
}

void Async_Handler::yield() const
{
  SwitchToFiber(main_fiber());
}

void Async_Handler::schedule_fiber(void *fiber)
{
  if (fiber != GetCurrentFiber())
  {
    SwitchToFiber(fiber);
  }
}

void Async_Handler::create_fiber(const boost::function0<void> &func,
				 const size_t stack_size) const
{
  TRACE(logger.debug(), "creating fiber, stack size=" << stack_size);

  fiber_wrapper_param_t param = { &func, this };
  SwitchToFiber(CreateFiberEx(2048, stack_size, 0, start_fiber_wrapper,
			      static_cast<void *>(&param)));
}

void Async_Handler::destroy_fiber() const
{
  yield();
}


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
