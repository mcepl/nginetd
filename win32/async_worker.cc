/*	-*- C++ -*-
 * Copyright (C) 2003-2007, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_worker.h"

#include "../async_handler.h"

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>


Async_Worker::Async_Worker(HANDLE completion_port)
  : completion_port_(completion_port)
{ }


Async_Worker::~Async_Worker()
{ }


void Async_Worker::operator() ()
{
  // this loop terminates when an OVERLAPPED_CANCEL is dequeued
  while (true)
  {
    DWORD bytes_transferred;
    ULONG completion_key;
    OVERLAPPED *overlapped;

    TRACE(logger.debug(), "starting GetQueuedCompletionStatus");
    BOOL res = GetQueuedCompletionStatus(completion_port_,
					 &bytes_transferred,
					 &completion_key,
					 &overlapped,
					 INFINITE);
    TRACE(logger.debug(), "done GetQueuedCompletionStatus");

    if (overlapped)
    {
      OVERLAPPED_PLUS * const ov_plus =
	CONTAINING_RECORD(overlapped, OVERLAPPED_PLUS, overlapped);
      const Event_Data::Event_Operation operation =
	ov_plus->operation;
      const ULONG error = res ? 0 : GetLastError();


      if (operation == OVERLAPPED_PLUS::OP_CANCEL)
      {
	delete ov_plus;
	break;
      }
      else if (operation == OVERLAPPED_PLUS::OP_ACCEPT)
      {
	on_accept(ov_plus, error);
      }
      else if (operation == OVERLAPPED_PLUS::OP_RECV)
      {
	Async_Handler::recv_handler * const callback =
	  ov_plus->recv_win32.callback;
	delete ov_plus;

	if (callback)
	{
	  (*callback)(error, bytes_transferred);
	}

	delete callback;
      }
      else if (operation == OVERLAPPED_PLUS::OP_SEND)
      {
	Async_Handler::send_handler * const callback =
	  ov_plus->send_win32.callback;
	delete ov_plus;

	if (callback)
	{
	  (*callback)(error);
	}

	delete callback;
      }
    }
    else
    {
      TRACE(logger.error(), "GetQueuedCompletionStatus didn't return an "
	    << "overlapped record, res=" << res
	    << ", error=" << GetLastError());
    }
  }
}


void Async_Worker::on_accept(OVERLAPPED_PLUS *ov_plus, ULONG error)
{
  TRACE(logger.debug(), "on_accept " << error);

  sockaddr *local_addr, *remote_addr;
  int local_len, remote_len;

  GetAcceptExSockaddrs(ov_plus->accept_win32.output_buffer,
		       0, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16,
		       reinterpret_cast<SOCKADDR **>(&local_addr),
		       &local_len,
		       reinterpret_cast<SOCKADDR **>(&remote_addr),
		       &remote_len);

  {
    Async_Handler::accept_handler *callback = ov_plus->accept.callback;
    (*callback)(ov_plus->accept_win32.socket, error);
  }


  // finally, initiate another AcceptEx operation
  ov_plus->accept_win32.socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (!AcceptEx(ov_plus->accept.listener_socket,
		ov_plus->accept_win32.socket,
		ov_plus->accept_win32.output_buffer,
		0, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16,
		&ov_plus->accept_win32.bytes_received,
		&ov_plus->overlapped))
  {
    const DWORD last_error = WSAGetLastError();
    if (last_error != WSA_IO_PENDING)
    {
      TRACE(logger.error(), "AcceptEx failed: " << last_error);
    }
  }
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
