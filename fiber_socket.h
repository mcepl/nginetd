/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__FIBER_SOCKET__H
#define INCLUDED__FIBER_SOCKET__H

#include "trace.h"

#include "async_socket.h"
#include "async_timer.h"

#include <memory>
#include <mutex>

// forward declarations
class Async_Timer;

#if defined(POSIX)
struct iovec;
#endif


class Fiber_Socket
{
  static Trace_Stream logger;

 public:
  Fiber_Socket(Async_Handler &handler, Async_Timer &timer, socket_t socket);
  Fiber_Socket(Fiber_Socket const &) = delete;
  Fiber_Socket &operator =(Fiber_Socket const &) = delete;
  ~Fiber_Socket();

 public:
  inline socket_t socket()
  {
    return async_->socket();
  }

  inline Async_Socket &async()
  { return *async_; }

  int send(const char *data, size_t len, int flags = 0,
	   unsigned int timeout = 0);

#if defined(POSIX)
  int sendmsg(const struct iovec *iov, int iov_len,
	      const char *cdata, size_t clen, int flags = 0,
	      unsigned int timeout = 0);

  int sendmsg(const char *data, size_t len,
	      const char *cdata, size_t clen, int flags = 0,
	      unsigned int timeout = 0);
#endif

  int recv(char *buffer, size_t &len, int flags = 0, unsigned int timeout = 0);

#if defined(POSIX)
  int recvmsg(char *buffer, size_t &len,
	      char *cbuffer, size_t &clen, int &flags,
	      unsigned int timeout = 0);
#endif

  inline int shutdown(int how)
  {
    return async_->shutdown(how);
  }

  inline int getsockopt(int level, int option_name,
#if defined(_WIN32)
			char *option_value,
			int *option_len
#else
			void *option_value,
			socklen_t *option_len
#endif
    )
  {
    return async_->getsockopt(level, option_name,
			      option_value, option_len);
  }

  inline int setsockopt(int level, int option_name,
#if defined(_WIN32)
			char *option_value,
			int option_len
#else
			void *option_value,
			socklen_t option_len
#endif
    )
  {
    return async_->setsockopt(level, option_name,
			      option_value, option_len);
  }

  inline int getsockname(struct sockaddr *addr,
#if defined(_WIN32)
			 int *addr_len
#else
			 socklen_t *addr_len
#endif
    )
  {
    return async_->getsockname(addr, addr_len);
  }

  inline int getpeername(struct sockaddr *addr,
#if defined(_WIN32)
			 int *addr_len
#else
			 socklen_t *addr_len
#endif
    )
  {
    return async_->getpeername(addr, addr_len);
  }


 protected:
  void on_send_complete(void *fiber, int error);
  void on_send_timeout(void *fiber, Async_Socket::req_id_t req_id);
  void on_send_wait();

  void on_recv_complete(void *fiber, int error, size_t len);
  void on_recv_timeout(void *fiber, Async_Socket::req_id_t req_id);
  void on_recv_wait();

#if defined(POSIX)
  void on_recvmsg_complete(void *fiber, int error, size_t len, size_t clen,
			   int flags);
#endif


 private:
  Async_Handler &handler_;
  Async_Timer &timer_;
  std::unique_ptr<Async_Socket> async_;

  Async_Timer::reg_t recv_timer_;
  Async_Timer::reg_t send_timer_;

  mutable std::mutex close_completed_sync_;
  bool close_completed_;

  mutable std::mutex send_completed_sync_;
  bool send_completed_;
  int send_error_;

  mutable std::mutex recv_completed_sync_;
  bool recv_completed_;
  int recv_error_;
  size_t recv_len_;
#if defined(POSIX)
  size_t recv_clen_;
  int recv_flags_;
#endif
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
