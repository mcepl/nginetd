/*	-*- C++ -*-
 * Copyright (C) 2009-2011, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__FIBER_RESOLVER__H
#define INCLUDED__FIBER_RESOLVER__H

#include "trace.h"

#include "async_handler.h"
#include "async_resolver.h"

#include <memory>


class Fiber_Resolver
{
  static Trace_Stream logger;

 public:
  explicit Fiber_Resolver(Async_Handler &handler);
  explicit Fiber_Resolver(Async_Resolver &resolver);
  Fiber_Resolver(Fiber_Resolver const &) = delete;
  Fiber_Resolver &operator =(Fiber_Resolver const &) = delete;
  ~Fiber_Resolver();

  inline Async_Resolver &async()
  { return resolver_; }

#if defined(HAVE_UDNS)
  const dns_rr_a4 *resolve_a(const char *name);
  const dns_rr_a6 *resolve_aaaa(const char *name);
  const dns_rr_ptr *resolve_ptr(const struct in_addr *addr);
  const dns_rr_ptr *resolve_ptr(const struct in6_addr *addr);
#endif

 private:
  std::unique_ptr<Async_Resolver> async_instance_;

  Async_Resolver &resolver_;
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
