require('nginet')

smtp = Service('smtp', 2525)

function smtp:bind()
   return { { '127.0.0.1', self._port },
	    { '127.0.0.2', self._port },
	    { '::1', self._port } }
end

function smtp:dnsbl(proto, addr, port)
   if proto == 'IPv4' then
      return { 'proxies.dnsbl.sorbs.net', 'cbl.abuseat.org' }
   elseif proto == 'IPv6' then
      return { 'virbl.dnsbl.bit.nl' }
   end
end

function smtp:check_accept(conn, dnsbl)
   conn.send('220 Hello\r\n')

   local data = conn.receive('\r\n', 10)
   if data then
      if not next(dnsbl) then
	 return true
      else
	 conn.send('500 Bye.\r\n')
      end
   end
end

return smtp
