function Class()
   local cls = {}
   cls.__index = cls

   local mt = {}
   mt.__call = function(cls, ...)
		  local obj = {}
		  obj = setmetatable(obj, cls)
		  if (cls._init) then
		     cls._init(obj, ...)
		  end
		  return obj
	       end
   setmetatable(cls, mt)
   return cls
end

Service = Class()

function Service:_init(name, port)
   self._name = name
   self._port = port
end

function Service:dump()
   print('service', self._name)
   for i, addr in ipairs(self._bind) do
      print('bind', addr.addr, addr.port)
   end
end

function Service:bind()
   return { }
end

function Service:dnsbl(proto, addr, port)
   return { }
end

function Service:check_accept(conn, dnsbl)
   return { }
end
