/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ASYNC_WORKER__H
#define INCLUDED__ASYNC_WORKER__H

#include "trace.h"

#if defined(_WIN32)
#include <winsock2.h>

struct OVERLAPPED_PLUS;
#else
#include "async_handler.h"
#include "socket.h"
#endif


// forward declaration
class Async_Handler;


/// worker thread for handling overlapped I/O requests
class Async_Worker
{
  static Trace_Stream logger;

  friend class Async_Handler;
  friend class Fiber_Handler;

 public:
  Async_Worker(Async_Worker &&) = default;
  Async_Worker &operator =(Async_Worker &&) = default;
  ~Async_Worker();

 protected:
#if defined(_WIN32)
  Async_Worker(HANDLE completion_port);
#elif defined(HAVE_EPOLL)
  Async_Worker(int epoll_fd);
#elif defined(HAVE_KQUEUE)
  Async_Worker(int kqueue_fd);
#endif

  Async_Worker(Async_Worker const &) = delete;
  Async_Worker &operator =(Async_Worker const &) = delete;

  void operator() ();


 protected:
#if defined(_WIN32)
  void on_accept(OVERLAPPED_PLUS *ov_plus, ULONG error);
#else
  void on_accept(socket_t socket, Async_Handler::accept_handler *callback);
#endif


 private:
#if defined(_WIN32)
  HANDLE completion_port_;
#elif defined(HAVE_EPOLL)
  int epoll_fd_;
#elif defined(HAVE_KQUEUE)
  int kqueue_fd_;
#endif
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
