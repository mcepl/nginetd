#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/thread_time.hpp>

#include <stdio.h>
#include <sys/resource.h>
#include <sys/time.h>

#include <pthread.h>

#include <vector>


namespace
{
  pthread_mutex_t outstanding_sync = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t outstanding_cond = PTHREAD_COND_INITIALIZER;
  int nr_outstanding = 0;


  void print_duration(struct timeval const &start)
  {
    struct timeval end;
    gettimeofday(&end, NULL);

    end.tv_sec -= start.tv_sec;
    if (end.tv_usec < start.tv_usec)
    {
      end.tv_usec += 1000000;
      --end.tv_sec;
    }
    end.tv_usec -= start.tv_usec;

    printf("duration %ld.%06ld s\n", end.tv_sec, end.tv_usec);
  }

  void print_rusage()
  {
    struct rusage usage;
    if (!getrusage(RUSAGE_SELF, &usage))
    {
      printf("vol/invol cs %ld/%ld\n", usage.ru_nvcsw, usage.ru_nivcsw);
    }
  }
}


class Async_Server
{
  const int nr_iter_;
  boost::asio::local::datagram_protocol::socket s1_;
  boost::asio::local::datagram_protocol::socket s2_;

  union
  {
    char buf_[sizeof(int)];
    int seq_nr_;
  };

public:
  struct Server_Functor
  {
    inline explicit Server_Functor(Async_Server &self)
      : self_(self)
    { }

  protected:
    Async_Server &self_;
  };

  struct Recv_Functor
    : Server_Functor
  {
    inline Recv_Functor(Async_Server &self,
			boost::asio::local::datagram_protocol::socket &s)
      : Server_Functor(self), s_(s)
    { }

    inline void operator () (const boost::system::error_code& error,
			     std::size_t bytes_transferred) const
    {
      self_.on_recv(s_, error, bytes_transferred);
    }

  protected:
    boost::asio::local::datagram_protocol::socket &s_;
  };

  struct Send_Functor
    : Server_Functor
  {
    inline explicit Send_Functor(Async_Server &self)
      : Server_Functor(self)
    { }

    inline void operator () (const boost::system::error_code& error,
			     std::size_t bytes_transferred) const
    {
      self_.on_send(error, bytes_transferred);
    }
  };

  Async_Server(boost::asio::io_service& io_service, int nr_iter)
    : nr_iter_(nr_iter), s1_(io_service), s2_(io_service)
  {
    boost::asio::local::connect_pair(s1_, s2_);

    s1_.async_receive(boost::asio::buffer(buf_, sizeof(buf_)),
		      Recv_Functor(*this, s1_));
    s2_.async_receive(boost::asio::buffer(buf_, sizeof(buf_)),
		      Recv_Functor(*this, s2_));
  }

  void start()
  {
    seq_nr_ =0;
    s1_.async_send(boost::asio::buffer(buf_, sizeof(buf_)),
		   Send_Functor(*this));
  }

private:
  void on_send(const boost::system::error_code& error,
	       std::size_t bytes_transferred)
  { }

  void on_recv(boost::asio::local::datagram_protocol::socket &s,
	       const boost::system::error_code& error,
	       std::size_t bytes_transferred)
  {
    s.async_receive(boost::asio::buffer(buf_, sizeof(buf_)),
		    Recv_Functor(*this, s));

    if (++seq_nr_ < nr_iter_)
    {
      s.async_send(boost::asio::buffer(buf_, sizeof(buf_)),
		   Send_Functor(*this));
    }
    else if (seq_nr_ == nr_iter_)
    {
      pthread_mutex_lock(&outstanding_sync);
      if (!--nr_outstanding)
      {
	pthread_cond_broadcast(&outstanding_cond);
      }
      pthread_mutex_unlock(&outstanding_sync);
    }
  }
};


int main(int argc, char *argv[])
{
  const int nr_threads = (argc > 1) ? atoi(argv[1]) : 1;
  const int nr_iter = (argc > 2) ? atoi(argv[2]) : 1;
  const int nr_pairs = (argc > 3) ? atoi(argv[3]) : 1;

  boost::asio::io_service io_service;
  std::vector<Async_Server *> srvs;

  for (int i = 0; i < nr_pairs; ++i)
  {
    srvs.push_back(new Async_Server(io_service, nr_iter));
    ++nr_outstanding;
  }

  boost::thread_group worker_threads;
  for (int i = 0; i < nr_threads; ++i)
  {
    worker_threads.create_thread(boost::bind(&boost::asio::io_service::run,
					     &io_service));
  }

  struct timeval start;
  gettimeofday(&start, NULL);

  for (std::vector<Async_Server *>::iterator iter = srvs.begin();
       iter != srvs.end();
       ++iter)
  {
    (*iter)->start();
  }

  pthread_mutex_lock(&outstanding_sync);
  while (nr_outstanding)
  {
    pthread_cond_wait(&outstanding_cond, &outstanding_sync);
  }
  pthread_mutex_unlock(&outstanding_sync);

  print_duration(start);
  print_rusage();

  io_service.stop();
  worker_threads.join_all();
}
