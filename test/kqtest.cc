#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/event.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <pthread.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }


namespace
{
  pthread_mutex_t outstanding_sync = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t outstanding_cond = PTHREAD_COND_INITIALIZER;
  int nr_outstanding = 0;


  void print_duration(struct timeval const &start)
  {
    struct timeval end;
    gettimeofday(&end, NULL);

    end.tv_sec -= start.tv_sec;
    if (end.tv_usec < start.tv_usec)
    {
      end.tv_usec += 1000000;
      --end.tv_sec;
    }
    end.tv_usec -= start.tv_usec;

    printf("duration %ld.%06ld s\n", end.tv_sec, end.tv_usec);
  }

  int set_socket_nonblocking(int socket)
  {
    int rc_nonblock;
    int flags = ::fcntl(socket, F_GETFL, 0);
    if (flags >= 0)
    {
      rc_nonblock = ::fcntl(socket, F_SETFL, flags | O_NONBLOCK);
    }
    else
    {
      rc_nonblock = flags;
    }

    return rc_nonblock;
  }

  struct Thread_Data
  {
    int kqid;
    int nr_iter;
  };

  extern "C" void *worker(void *arg)
  {
    Thread_Data *data = static_cast<Thread_Data *>(arg);
    const int kqid = data->kqid;
    const int nr_iter = data->nr_iter;

    union
    {
      char buf[4096];
      int seq_nr;
    };

    struct kevent events[16];
    bool done = false;

    while (!done)
    {
      int rc_kevent;
      RETRY_EINTR_RC(rc_kevent,
		     kevent(kqid, NULL, 0, events, sizeof(events)/sizeof(*events), NULL));
      if (rc_kevent < 0) break;

      for (int i = 0; i < rc_kevent; ++i)
      {
	if (events[i].flags & EV_EOF)
	{
	  done = true;
	  break;
	}

	const int s = events[i].ident;

	while (true)
	{
	  int rc_recv;
	  RETRY_EINTR_RC(rc_recv, recv(s, buf, sizeof(buf), 0));
	  if ((rc_recv < 0) && ((errno == EWOULDBLOCK) || (errno == EAGAIN)))
	  {
	    break;
	  }

	  if (++seq_nr < nr_iter)
	  {
	    int rc_send;
	    RETRY_EINTR_RC(rc_send, send(s, buf, sizeof(int), 0));
	  }
	  else if (seq_nr == nr_iter)
	  {
	    pthread_mutex_lock(&outstanding_sync);
	    if (!--nr_outstanding)
	    {
	      pthread_cond_broadcast(&outstanding_cond);
	    }
	    pthread_mutex_unlock(&outstanding_sync);
	  }
	}
      }
    }
  }
}


int main(int argc, const char * const argv[])
{
  const int nr_threads = (argc > 1) ? atoi(argv[1]) : 1;
  const int nr_iter = (argc > 2) ? atoi(argv[2]) : 1;
  const int nr_pairs = (argc > 3) ? atoi(argv[3]) : 1;

  const int kqid(kqueue());

  int syncsockets[2];
  socketpair(AF_UNIX, SOCK_STREAM, 0, syncsockets);
  {
    struct kevent const events[] = {
      { syncsockets[1], EVFILT_READ, EV_ADD, 0, 0, 0 },
    };
    kevent(kqid, events, 1, NULL, 0, NULL);
  }

  int *sockets = new int[2 * nr_pairs];
  for (int i = 0; i < nr_pairs; ++i)
  {
    socketpair(AF_UNIX, SOCK_DGRAM, 0, sockets + 2*i);
    set_socket_nonblocking(sockets[2*i]);
    set_socket_nonblocking(sockets[2*i + 1]);

    struct kevent const events[] = {
      { sockets[2*i], EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, 0 },
      { sockets[2*i + 1], EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, 0 }
    };
    kevent(kqid, events, 2, NULL, 0, NULL);

    ++nr_outstanding;
  }

  pthread_t *threads = new pthread_t[nr_threads];
  Thread_Data thread_arg = { kqid, nr_iter };
  for (int i = 0; i < nr_threads; ++i)
  {
    pthread_create(threads + i, NULL, worker, &thread_arg);
  }

  struct timeval start;
  gettimeofday(&start, NULL);

  int buf = 0;
  for (int i = 0; i < nr_pairs; ++i)
  {
    send(sockets[2*i], &buf, sizeof(buf), 0);
  }

  pthread_mutex_lock(&outstanding_sync);
  while (nr_outstanding)
  {
    pthread_cond_wait(&outstanding_cond, &outstanding_sync);
  }
  pthread_mutex_unlock(&outstanding_sync);

  print_duration(start);

  close(syncsockets[0]);

  for (int i = 0; i < nr_threads; ++i)
  {
    pthread_join(threads[i], NULL);
  }

  for (int i = 0; i < nr_pairs; ++i)
  {
    close(sockets[2*i]);
    close(sockets[2*i + 1]);
  }
  close(syncsockets[1]);
  close(kqid);

  delete [] threads;
  delete [] sockets;

  return 0;
}
