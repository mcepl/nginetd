#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/fcntl.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <pthread.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }


namespace
{
  pthread_mutex_t outstanding_sync = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t outstanding_cond = PTHREAD_COND_INITIALIZER;
  int nr_outstanding = 0;


  void print_duration(struct timeval const &start)
  {
    struct timeval end;
    gettimeofday(&end, NULL);

    end.tv_sec -= start.tv_sec;
    if (end.tv_usec < start.tv_usec)
    {
      end.tv_usec += 1000000;
      --end.tv_sec;
    }
    end.tv_usec -= start.tv_usec;

    printf("duration %ld.%06ld s\n", end.tv_sec, end.tv_usec);
  }

  void print_rusage()
  {
    struct rusage usage;
    if (!getrusage(RUSAGE_SELF, &usage))
    {
      printf("vol/invol cs %ld/%ld\n", usage.ru_nvcsw, usage.ru_nivcsw);
    }
  }

  struct Thread_Data
  {
    int sockets[2];
    int nr_iter;
  };

  extern "C" void *worker(void *arg)
  {
    Thread_Data *data = static_cast<Thread_Data *>(arg);
    const int *sockets = data->sockets;
    const int nr_iter = data->nr_iter;

    union
    {
      char buf[4096];
      int seq_nr;
    };

    int which = 1;
    bool done = false;

    while (!done)
    {
      int s = sockets[which];
      int rc_recv;
      RETRY_EINTR_RC (rc_recv, recv(s, buf, sizeof(buf), 0));

      which = (which + 1) & 1;

      if (++seq_nr < nr_iter)
      {
	int rc_send;
	RETRY_EINTR_RC(rc_send, send(s, buf, sizeof(int), 0));
      }
      else if (seq_nr == nr_iter)
      {
	pthread_mutex_lock(&outstanding_sync);
	if (!--nr_outstanding)
	{
	  pthread_cond_broadcast(&outstanding_cond);
	}
	pthread_mutex_unlock(&outstanding_sync);

	done = true;
      }
    }

    return NULL;
  }
}


int main(int argc, const char * const argv[])
{
  const int nr_threads = (argc > 1) ? atoi(argv[1]) : 1;
  const int nr_iter = (argc > 2) ? atoi(argv[2]) : 1;

  int *sockets = new int[2 * nr_threads];
  for (int i = 0; i < nr_threads; ++i)
  {
    socketpair(AF_UNIX, SOCK_DGRAM, 0, sockets + 2*i);
    ++nr_outstanding;
  }

  pthread_t *threads = new pthread_t[nr_threads];
  Thread_Data *thread_arg = new Thread_Data[nr_threads];
  for (int i = 0; i < nr_threads; ++i)
  {
    thread_arg[i].sockets[0] = sockets[2*i];
    thread_arg[i].sockets[1] = sockets[2*i + 1];
    thread_arg[i].nr_iter = nr_iter;
    pthread_create(threads + i, NULL, worker, thread_arg + i);
  }

  struct timeval start;
  gettimeofday(&start, NULL);

  int buf = 0;
  for (int i = 0; i < nr_threads; ++i)
  {
    send(sockets[2*i], &buf, sizeof(buf), 0);
  }

  pthread_mutex_lock(&outstanding_sync);
  while (nr_outstanding)
  {
    pthread_cond_wait(&outstanding_cond, &outstanding_sync);
  }
  pthread_mutex_unlock(&outstanding_sync);

  print_duration(start);
  print_rusage();

  for (int i = 0; i < nr_threads; ++i)
  {
    pthread_join(threads[i], NULL);
  }

  for (int i = 0; i < nr_threads; ++i)
  {
    close(sockets[2*i]);
    close(sockets[2*i + 1]);
  }

  delete [] thread_arg;
  delete [] threads;
  delete [] sockets;

  return 0;
}
