#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/epoll.h>
#include <sys/fcntl.h>
#include <sys/socket.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }


namespace
{
  int set_socket_nonblocking(int socket)
  {
    int rc_nonblock;
    int flags = ::fcntl(socket, F_GETFL, 0);
    if (flags >= 0)
    {
      rc_nonblock = ::fcntl(socket, F_SETFL, flags | O_NONBLOCK);
    }
    else
    {
      rc_nonblock = flags;
    }

    return rc_nonblock;
  }
}


int main(int argc, const char * const argv[])
{
  const int nr_iter = (argc > 1) ? atoi(argv[1]) : 1;

  const int epollfd(epoll_create(1024));

  int sockets[2];
  socketpair(AF_UNIX, SOCK_DGRAM, 0, sockets);
  set_socket_nonblocking(sockets[0]);
  set_socket_nonblocking(sockets[1]);

  struct epoll_event event = {
    EPOLLIN | EPOLLOUT | EPOLLET, 0
  };

  event.data.fd = sockets[0];
  epoll_ctl(epollfd, EPOLL_CTL_ADD, sockets[0], &event);

  event.data.fd = sockets[1];
  epoll_ctl(epollfd, EPOLL_CTL_ADD, sockets[1], &event);


  union
  {
    char buf[4096];
    int seq_nr;
  };

  int got_epollout = 0;

  struct epoll_event events[16];
  bool done = false;
  bool got_data = true;

  seq_nr = -1;

  while (!done)
  {
    if (got_data)
    {
      got_data = false;

      if (++seq_nr < nr_iter)
      {
	int rc_send;
	RETRY_EINTR_RC(rc_send, send(sockets[0], buf, sizeof(int), 0));
      }
      else if (seq_nr == nr_iter)
      {
	done = true;
	break;
      }
    }

    int rc_epoll;
    RETRY_EINTR_RC(rc_epoll,
		   epoll_wait(epollfd, events, sizeof(events) / sizeof(*events), -1));
    if (rc_epoll < 0) break;

    for (int i = 0; i < rc_epoll; ++i)
    {
      if (events[i].events & EPOLLHUP)
      {
	done = true;
	break;
      }

      const int s = events[i].data.fd;

      if (events[i].events == EPOLLOUT)
      {
	++got_epollout;
      }
      else if (events[i].events & EPOLLIN)
      {
	while (true)
	{
	  int rc_recv;
	  RETRY_EINTR_RC(rc_recv, recv(s, buf, sizeof(buf), 0));
	  if ((rc_recv < 0) && ((errno == EWOULDBLOCK) || (errno == EAGAIN)))
	  {
	    break;
	  }

	  got_data = true;
	}
      }
    }
  }


  printf("iterations: %d, useless EPOLLOUT events: %d\n",
	 nr_iter, got_epollout);

  close(sockets[0]);
  close(sockets[1]);
  close(epollfd);

  return 0;
}
