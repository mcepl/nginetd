/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "../async_handler.h"
#include "../async_resolver.h"
#include "../trace.h"

#include <netinet/in.h>

#include <condition_variable>
#include <iostream>
#include <mutex>


namespace
{
  unsigned int outstanding = 0;
  std::mutex mtx;
  std::condition_variable cond;

  void resolved_a(const dns_rr_a4 *a4)
  {
    std::cout << "resolved_a: " << a4->dnsa4_addr->s_addr << '\n'; 

    std::lock_guard<std::mutex> guard (mtx);
    --outstanding;

    if (! outstanding)
    {
      cond.notify_one();
    }
  }
}


int main(int argc, char *argv[])
{
  Trace_Stream::set_global_level(TRACE_DEBUG);

  Async_Handler handler(1);
  Async_Resolver resolver(handler);

  outstanding = argc - 1;

  for (int i = 1; i < argc; ++i)
  {
    resolver.resolve_a(argv[i], resolved_a);
  }

  {
    std::unique_lock<std::mutex> guard (mtx);
    while (outstanding)
    {
      cond.wait(guard);
    }
  }
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
