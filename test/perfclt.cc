#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sys/socket.h>

#include <errno.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#include <boost/thread.hpp>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }


int setup_connection(struct sockaddr_in6 *addr)
{
  int s = socket(PF_INET6, SOCK_STREAM, 0);

  const int rc = connect(s, (struct sockaddr *) addr, sizeof(*addr));
  if (rc < 0) return rc;

  int flag = 1;
  setsockopt(s, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(flag));

  return s;
}


void worker(struct sockaddr_in6 *addr, int nr_conn, int nr_blocks)
{
  char buf[8*1024];
  size_t len = sizeof(buf);

  int *socks = new int[nr_conn];

  for (int i = 0; i < nr_conn; ++i)
  {
    if ((socks[i] = setup_connection(addr)) < 0)
    {
      return;
    }
  }

  for (int i = 0; i < nr_blocks; ++i)
  {
    for (int j = 0; j < nr_conn; ++j)
    {
      int snr;

      snr = j;
      RETRY_EINTR(send(socks[snr], buf, sizeof(buf), 0));
      snr = nr_conn - j - 1;
      RETRY_EINTR(send(socks[snr], buf, sizeof(buf), 0));

      snr = nr_conn - j - 1;
      len = sizeof(buf);
      while (len != 0)
      {
	int rc_recv;
	RETRY_EINTR_RC(rc_recv, recv(socks[snr], buf, len, 0));
	if (rc_recv <= 0) break;
	len -= rc_recv;
      }

      snr = j;
      len = sizeof(buf);
      while (len != 0)
      {
	int rc_recv;
	RETRY_EINTR_RC(rc_recv, recv(socks[snr], buf, len, 0));
	if (rc_recv <= 0) break;
	len -= rc_recv;
      }
    }
  }

  for (int i = 0; i < nr_conn; ++i)
  {
    shutdown(socks[i], SHUT_WR);
  }

  for (int i = 0; i < nr_conn; ++i)
  {
    recv(socks[i], buf, sizeof(buf), 0);
  }
}


int main(int argc, const char * const argv[])
{
  if (argc > 4)
  {
    struct sockaddr_in6 addr;
    std::memset(&addr, 0, sizeof(addr));
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(12345);
    if (inet_pton(AF_INET6, argv[1], &addr.sin6_addr) != 1)
    {
      return 1;
    }

    const int nr_threads = atoi(argv[2]);
    const int nr_conn = atoi(argv[3]);
    const int nr_blocks = atoi(argv[4]);

    boost::thread_group worker_threads;
    for (int i = 0; i < nr_threads; ++i)
    {
      worker_threads.create_thread(boost::bind(&worker, &addr, nr_conn, nr_blocks));
    }
    worker_threads.join_all();
  }

  return 0;
}
