#include <cstdlib>
#include <cstddef>
#include <stdio.h>

#include <vector>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/aligned_storage.hpp>

#include <sys/time.h>
#include <sys/resource.h>


namespace
{
  void print_duration(struct timeval const &start)
  {
    struct timeval end;
    gettimeofday(&end, NULL);

    end.tv_sec -= start.tv_sec;
    if (end.tv_usec < start.tv_usec)
    {
      end.tv_usec += 1000000;
      --end.tv_sec;
    }
    end.tv_usec -= start.tv_usec;

    printf("duration %ld.%06ld s\n", end.tv_sec, end.tv_usec);
  }

  void print_rusage()
  {
    struct rusage usage;
    if (!getrusage(RUSAGE_SELF, &usage))
    {
      printf("vol/invol cs %ld/%ld\n", usage.ru_nvcsw, usage.ru_nivcsw);
    }
  }

  boost::mutex outstanding_sync;
  boost::condition_variable outstanding_cond;
  int nr_outstanding = 0;

// See http://www.boost.org/doc/libs/1_49_0/doc/html/boost_asio/example/allocation/server.cpp
  class handler_allocator
  {
    typedef handler_allocator this_type;

    handler_allocator(const this_type&);
    this_type& operator=(const this_type&);

  public:
    handler_allocator()
      : in_use_(false)
    {
    }

    void* allocate(std::size_t size)
    {
      if (!in_use_ && size <= storage_.size)
      {
	in_use_ = true;
	return storage_.address();
      }
      else
      {
	return ::operator new(size);
      }
    }

    void deallocate(void* pointer)
    {
      if (pointer == storage_.address())
      {
	in_use_ = false;
      }
      else
      {
	::operator delete(pointer);
      }
    }

  private:
    boost::aligned_storage<256> storage_;
    bool in_use_;
  }; // handler_allocator

  class async_server
  {
    const int nr_iter_;

    typedef async_server this_type;  

    // See http://www.boost.org/doc/libs/1_49_0/doc/html/boost_asio/example/allocation/server.cpp
    template <typename Handler>
    class custom_alloc_handler
    {
      typedef custom_alloc_handler<Handler> this_type;

    public:
      custom_alloc_handler(handler_allocator& a, Handler h)
	: allocator_(a)
	, handler_(h)
      {
      }

      template <typename Arg1, typename Arg2>
      void operator()(Arg1 arg1, Arg2 arg2)
      {
	handler_(arg1, arg2);
      }

      friend void* asio_handler_allocate(std::size_t size,
					 this_type* this_handler)
      {
	return this_handler->allocator_.allocate(size);
      }

      friend void asio_handler_deallocate(void* pointer, std::size_t /*size*/,
					  this_type* this_handler)
      {
	this_handler->allocator_.deallocate(pointer);
      }

    private:
      handler_allocator& allocator_;
      Handler handler_;
    }; // custom_alloc_handler

    // See http://www.boost.org/doc/libs/1_49_0/doc/html/boost_asio/example/allocation/server.cpp
    template <typename Handler>
    static custom_alloc_handler<Handler> make_custom_alloc_handler(
      handler_allocator& a, Handler h)
    {
      return custom_alloc_handler<Handler>(a, h);
    }

    // Connection's socket, data and state
    struct connection
    {
      explicit connection(boost::asio::io_service& io_service,
			  handler_allocator& ra, handler_allocator& sa)
	: seq_nr(0)
	, socket(io_service)
	, strand(io_service)
	, send_in_progress(false)
	, has_data_to_send(false)
	, recv_allocator(ra)
	, send_allocator(sa)
      {
      }

      // Before socket because of the order of destructors.
      // Destructor of socket closes socket implicitly.
      int seq_nr;

      boost::asio::local::datagram_protocol::socket socket;
      boost::asio::io_service::strand strand;    
      bool send_in_progress;
      bool has_data_to_send;
      handler_allocator& recv_allocator;
      handler_allocator& send_allocator;
    }; // struct connection

    connection connection1_;
    connection connection2_;

  public:  
    async_server(boost::asio::io_service& io_service, int nr_iter,
		 handler_allocator& recv_allocator1, handler_allocator& send_allocator1,
		 handler_allocator& recv_allocator2, handler_allocator& send_allocator2)
      : nr_iter_(nr_iter)
      , connection1_(io_service, recv_allocator1, send_allocator1)
      , connection2_(io_service, recv_allocator2, send_allocator2)
    {
      boost::asio::local::connect_pair(connection1_.socket, connection2_.socket);
      start_async_recv(connection1_);
      start_async_recv(connection2_);
    }

    void start()
    {
      connection1_.strand.dispatch(boost::bind(&this_type::start_async_send,
					       this, boost::ref(connection1_)));
    }

  private:
    void start_async_recv(connection& c)
    {
      c.socket.async_receive(boost::asio::buffer(&c.seq_nr, sizeof(c.seq_nr)),
			     c.strand.wrap(make_custom_alloc_handler(c.recv_allocator,
								     boost::bind(&this_type::on_recv, this, boost::ref(c),
										 boost::asio::placeholders::error))));
    }

    void start_async_send(connection& c)
    {
      c.socket.async_send(boost::asio::buffer(&c.seq_nr, sizeof(c.seq_nr)),
			  c.strand.wrap(make_custom_alloc_handler(c.send_allocator,
								  boost::bind(&this_type::on_send, this, boost::ref(c),
									      boost::asio::placeholders::error))));
      c.send_in_progress = true;
    }

    void on_send(connection& c, const boost::system::error_code& /*error*/)
    {    
      c.send_in_progress = false;
      if (c.has_data_to_send)
      {
	start_async_send(c);
	c.has_data_to_send = false;
      }
    }

    void on_recv(connection& c, const boost::system::error_code& /*error*/)
    {
      start_async_recv(c);
      if (++c.seq_nr < nr_iter_)
      {
	if (c.send_in_progress)
	{
	  c.has_data_to_send = true;
	}
	else
	{
	  start_async_send(c);
	}
      }
      else if (nr_iter_ == c.seq_nr)
      {
	boost::unique_lock<boost::mutex> lock(outstanding_sync);
	if (!--nr_outstanding)
	{        
	  outstanding_cond.notify_all();
	}
      }
    }
  }; // async_server

  struct server_allocator
  {
    handler_allocator recv_allocator1;
    handler_allocator send_allocator1;
    handler_allocator recv_allocator2;
    handler_allocator send_allocator2;
  };
} // anonymous namespace

int main(int argc, char* argv[])
{
  const int nr_threads = (argc > 1) ? atoi(argv[1]) : 1;
  const int nr_iter    = (argc > 2) ? atoi(argv[2]) : 1;
  const int nr_pairs   = (argc > 3) ? atoi(argv[3]) : 1;


  typedef boost::shared_ptr<server_allocator> server_allocator_ptr;
  typedef std::vector<server_allocator_ptr> server_allocator_vector;

  // Note: before io_service for the right order of destructors
  server_allocator_vector allocators;
  for (int i = 0; i != nr_pairs; ++i)
  {
    allocators.push_back(boost::make_shared<server_allocator>());
  }

  boost::asio::io_service io_service;

  typedef boost::shared_ptr<async_server> async_server_ptr;
  typedef std::vector<async_server_ptr> async_server_vector;

  async_server_vector servers;  
  for (int i = 0; i != nr_pairs; ++i)
  {
    servers.push_back(boost::shared_ptr<async_server>(
			new async_server(
			  boost::ref(io_service), nr_iter, 
			  allocators[i]->recv_allocator1,
			  allocators[i]->send_allocator1,
			  allocators[i]->recv_allocator2,
			  allocators[i]->send_allocator2)));
  }
  nr_outstanding = nr_pairs;

  boost::thread_group worker_threads;
  for (int i = 0; i < nr_threads; ++i)
  {
    worker_threads.create_thread(
      boost::bind(&boost::asio::io_service::run, &io_service));
  }

  struct timeval start;
  gettimeofday(&start, NULL);

  for (async_server_vector::const_iterator i = servers.begin(),
	 end = servers.end(); i != end; ++i)
  {
    (*i)->start();
  }

  {
    boost::unique_lock<boost::mutex> lock(outstanding_sync);
    while (nr_outstanding)
    {      
      outstanding_cond.wait(lock);
    }
  }

  print_duration(start);
  print_rusage();

  io_service.stop();
  worker_threads.join_all();

  return EXIT_SUCCESS;
}
