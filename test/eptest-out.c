#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/fcntl.h>
#include <sys/socket.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR)); \
    }


int set_socket_nonblocking(int socket)
{
  int rc_nonblock;
  int flags = fcntl(socket, F_GETFL, 0);
  if (flags >= 0)
  {
    rc_nonblock = fcntl(socket, F_SETFL, flags | O_NONBLOCK);
  }
  else
  {
    rc_nonblock = flags;
  }

  return rc_nonblock;
}


void prepare_pipe(int fds[2])
{
  pipe(fds);
}

void prepare_socketpair(int fds[2], int type)
{
  socketpair(AF_UNIX, type, 0, fds);
}

void prepare_udp(int fds[2])
{
  fds[0] = socket(AF_INET, SOCK_DGRAM, 0);
  fds[1] = socket(AF_INET, SOCK_DGRAM, 0);

  struct sockaddr_in a1, a2;
  memset(&a1, 0, sizeof(a1));
  memset(&a2, 0, sizeof(a2));

  a1.sin_family = a2.sin_family = AF_INET;
  a1.sin_port = htons(12345);
  a2.sin_port = htons(12346);
  a1.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
  a2.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

  bind(fds[1], &a2, sizeof(a2));
  bind(fds[0], &a1, sizeof(a1));

  connect(fds[0], &a2, sizeof(a2));
  connect(fds[1], &a1, sizeof(a1));
}

void prepare_tcp(int fds[2])
{
  int listener = socket(AF_INET, SOCK_STREAM, 0);

  fds[0] = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in a;
  memset(&a, 0, sizeof(a));

  a.sin_family = AF_INET;
  a.sin_port = htons(12345);
  a.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

  bind(listener, &a, sizeof(a));
  listen(listener, 1);

  connect(fds[0], &a, sizeof(a));
  fds[1] = accept(listener, NULL, NULL);
  close(listener);
}

void run_test(int nr_iter, int fds[2])
{
  const int epollfd = epoll_create(1024);

  set_socket_nonblocking(fds[1]);

  struct epoll_event event = {
    EPOLLIN | EPOLLOUT | EPOLLET, 0
  };

  event.data.fd = fds[1];
  epoll_ctl(epollfd, EPOLL_CTL_ADD, fds[1], &event);


  int seq_nr = -1;
  int got_epollout = 0;

  struct epoll_event events[16];

  while (1)
  {
    if (++seq_nr < nr_iter)
    {
      int rc_write;
      RETRY_EINTR_RC(rc_write, write(fds[1], &seq_nr, sizeof(int)));
    }
    else
    {
      break;
    }

    {
      int rc_epoll;
      RETRY_EINTR_RC(rc_epoll,
          epoll_wait(epollfd, events, sizeof(events) / sizeof(*events), 0));

      int i;
      for (i = 0; i < rc_epoll; ++i)
      {
        if (events[i].events == EPOLLOUT)
        {
          ++got_epollout;
        }
      }
    }

    {
      int rc_read;
      RETRY_EINTR_RC(rc_read, read(fds[0], &seq_nr, sizeof(int)));
      if (rc_read < 0)
      {
        break;
      }
    }

    {
      int rc_epoll;
      RETRY_EINTR_RC(rc_epoll,
          epoll_wait(epollfd, events, sizeof(events) / sizeof(*events), 0));

      int i;
      for (i = 0; i < rc_epoll; ++i)
      {
        if (events[i].events == EPOLLOUT)
        {
          ++got_epollout;
        }
      }
    }
  }


  printf("iterations: %d, useless EPOLLOUT events: %d\n",
	 nr_iter, got_epollout);
  close(epollfd);
}

int main(int argc, const char * const argv[])
{
  int fds[2];
  const int nr_iter = (argc > 1) ? atoi(argv[1]) : 1;

  printf("UNIX pipes\n");
  prepare_pipe(fds);
  run_test(nr_iter, fds);
  close(fds[0]);
  close(fds[1]);


  printf("\n" "UNIX domain sockets (DGRAM)\n");
  prepare_socketpair(fds, SOCK_DGRAM);
  run_test(nr_iter, fds);
  close(fds[0]);
  close(fds[1]);

  printf("\n" "UNIX domain sockets (STREAM)\n");
  prepare_socketpair(fds, SOCK_STREAM);
  run_test(nr_iter, fds);
  close(fds[0]);
  close(fds[1]);


  printf("\n" "UDP\n");
  prepare_udp(fds);
  run_test(nr_iter, fds);
  close(fds[0]);
  close(fds[1]);


  printf("\n" "TCP\n");
  prepare_tcp(fds);
  run_test(nr_iter, fds);
  close(fds[0]);
  close(fds[1]);


  return 0;
}
