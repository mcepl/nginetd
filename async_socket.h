/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ASYNC_SOCKET__H
#define INCLUDED__ASYNC_SOCKET__H

#include "trace.h"

#include "async_handler.h"
#include "socket.h"

#include <functional>

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
#include <deque>
#include <mutex>
#endif


// forward declarations
#if defined(POSIX)
struct iovec;
#endif


class Async_Socket
{
  static Trace_Stream logger;

  friend class Async_Handler;
  friend class Async_Worker;

 public:
  typedef unsigned int req_id_t;
  static const req_id_t NO_REQ_ID = ~req_id_t();

  ~Async_Socket();


 protected:
  explicit Async_Socket(Async_Handler &handler);
  Async_Socket(Async_Socket const &) = delete;
  Async_Socket &operator =(Async_Socket const &) = delete;

  void init(socket_t socket);

 public:
  inline socket_t socket()
  {
    return socket_;
  }

  req_id_t send(const char *data, size_t len, int flags = 0,
		const Async_Handler::send_handler &cb = Async_Handler::send_handler());

  inline req_id_t send(const char *data, size_t len,
		       const Async_Handler::send_handler &cb)
  { return send(data, len, 0, cb); }

#if defined(POSIX)
  req_id_t sendmsg(const char *data, size_t len,
		   const char *cdata, size_t clen, int flags = 0,
		   const Async_Handler::sendmsg_handler &cb = Async_Handler::sendmsg_handler());

  inline req_id_t sendmsg(const char *data, size_t len,
			  const char *cdata, size_t clen,
			  const Async_Handler::sendmsg_handler &cb = Async_Handler::sendmsg_handler())
  { return sendmsg(data, len, cdata, clen, 0, cb); }

  req_id_t sendmsg(const struct iovec *iov, int iov_len,
		   const char *cdata, size_t clen, int flags = 0,
		   const Async_Handler::sendmsg_handler &cb = Async_Handler::sendmsg_handler());

  inline req_id_t sendmsg(const struct iovec *iov, int iov_len,
			  const char *cdata, size_t clen,
			  const Async_Handler::sendmsg_handler &cb = Async_Handler::sendmsg_handler())
  { return sendmsg(iov, iov_len, cdata, clen, 0, cb); }

  inline req_id_t sendmsg(const struct iovec *iov, int iov_len,
			  const Async_Handler::sendmsg_handler &cb = Async_Handler::sendmsg_handler())
  { return sendmsg(iov, iov_len, nullptr, 0, 0, cb); }

  inline req_id_t sendmsg(const struct iovec *iov, int iov_len, int flags = 0,
			  const Async_Handler::sendmsg_handler &cb = Async_Handler::sendmsg_handler())
  { return sendmsg(iov, iov_len, nullptr, 0, flags, cb); }
#endif

  req_id_t recv(char *buffer, size_t len,
		const Async_Handler::recv_handler &cb,
		int flags = 0);

#if defined(POSIX)
  req_id_t recvmsg(char *buffer, size_t len,
		   char *cbuffer, size_t clen,
		   const Async_Handler::recvmsg_handler &cb,
		   int flags = 0);
#endif

  req_id_t connect(const struct sockaddr * addr, socklen_t addr_len,
		   const Async_Handler::connect_handler &cb);

  bool cancel_recv(req_id_t req_id);

  bool cancel_send(req_id_t req_id);

  bool cancel_connect(req_id_t req_id);

  inline int shutdown(int how)
  {
    return ::shutdown(socket_, how);
  }

  inline int getsockopt(int level, int option_name,
#if defined(_WIN32)
			char *option_value,
			int *option_len
#else
			void *option_value,
			socklen_t *option_len
#endif
    )
  {
    return ::getsockopt(socket_, level, option_name, option_value, option_len);
  }

  inline int setsockopt(int level, int option_name,
#if defined(_WIN32)
			char *option_value,
			int option_len
#else
			void *option_value,
			socklen_t option_len
#endif
    )
  {
    return ::setsockopt(socket_, level, option_name, option_value, option_len);
  }

  inline int getsockname(struct sockaddr *addr,
#if defined(_WIN32)
			 int *addr_len
#else
			 socklen_t *addr_len
#endif
    )
  {
    return ::getsockname(socket_, addr, addr_len);
  }

  inline int getpeername(struct sockaddr *addr,
#if defined(_WIN32)
			 int *addr_len
#else
			 socklen_t *addr_len
#endif
    )
  {
    return ::getpeername(socket_, addr, addr_len);
  }

  void close();


 protected:
  inline socket_t socket() const
  { return socket_; }

#if defined(_WIN32)
  void _send(const char *data, size_t len, int flags,
	     std::function<void (int)> *cb);
#endif

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
  void on_send() const;
  void on_recv() const;

  void reset_events() const;

  inline const Event_Data *event_data() const
  { return &event_data_; }
#endif


 private:
  Async_Handler &handler_;
  socket_t socket_;

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
  const Event_Data event_data_;

  struct recv_request
  {
    enum Msg_Type
    {
      Msg_Recv,
      Msg_RecvMsg
    };

    explicit
    recv_request(char * const buffer, const size_t len, const int flags,
		 const std::function<void (int, size_t)> &cb)
      : typ(Msg_Recv), cancelled(false),
	buffer(buffer), cbuffer(0), len(len), clen(0),
	flags(flags)
    {
      u.recv_cb = new std::function<void (int, size_t)>(cb);
    }

    explicit
    recv_request(char * const buffer, const size_t len,
		 char * const cbuffer, const size_t clen,
		 const int flags,
		 const std::function<void (int, size_t, size_t, int)> &cb)
      : typ(Msg_RecvMsg), cancelled(false),
	buffer(buffer), cbuffer(cbuffer), len(len), clen(clen), flags(flags)
    {
      u.recvmsg_cb = new std::function<void (int, size_t, size_t, int)>(cb);
    }

    const Msg_Type typ : 8;
    bool cancelled;
    char * const buffer;
    char * const cbuffer;
    const size_t len, clen;
    const int flags;

    union
    {
      std::function<void (int, size_t)> *recv_cb;
      std::function<void (int, size_t, size_t, int)> *recvmsg_cb;
    } u;
  };

  typedef std::deque<recv_request> recv_queue_t;

  struct send_request
  {
    enum Msg_Type
    {
      Msg_Send,
      Msg_SendMsg
    };

    explicit
    send_request(const char * const data, const size_t len, const int flags,
		 const std::function<void (int)> &cb)
      : typ(Msg_Send), cancelled(false),
	data(data), offset(0), len(len), clen(0), flags(flags),
	cb(cb ? new std::function<void (int)>(cb) : NULL)
    { }

    explicit
    send_request(const char * const data, const size_t len, const size_t clen,
		 const int flags, const std::function<void (int)> &cb)
      : typ(Msg_SendMsg), cancelled(false),
	data(data), offset(0), len(len), clen(clen), flags(flags),
	cb(cb ? new std::function<void (int)>(cb) : NULL)
    { }

    const Msg_Type typ : 8;
    bool cancelled;
    const char * const data;
    const size_t len, clen;
    const int flags;
    size_t offset;

    std::function<void (int)> *cb;
  };

  typedef std::deque<send_request> send_queue_t;

  mutable std::mutex recv_queue_sync_;
  mutable recv_queue_t recv_queue_;
  mutable req_id_t recv_req_id_;
  mutable bool recv_events_;

  mutable std::mutex send_queue_sync_;
  mutable send_queue_t send_queue_;
  mutable req_id_t send_req_id_;
  mutable bool send_events_;

  mutable std::vector<unsigned char> connect_addr_;
  mutable std::function<void (int)> connect_cb_;
  mutable bool connect_events_;
#endif
};

#if defined(_WIN32)
inline void Async_Socket::send(const char *data, size_t len, int flags,
			       const std::function<void (int)> &cb)
{
  _send(data, len, flags, new std::function<void (int)>(cb));
}

inline void Async_Socket::send(const char *data, size_t len, int flags)
{
  _send(data, len, flags, NULL);
}
#endif

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
