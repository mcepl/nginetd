/*	-*- C++ -*-
 * Copyright (C) 2009-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ASYNC_RESOLVER__H
#define INCLUDED__ASYNC_RESOLVER__H

#include "trace.h"

#include "async_handler.h"
#include "async_timer.h"
#include "socket.h"

#if defined(HAVE_UDNS)
#include <udns.h>
#endif

#include <deque>
#include <functional>
#include <mutex>


class Async_Resolver
{
  static Trace_Stream logger;

  friend class Async_Handler;
  friend class Async_Worker;

  class Request
  {
    static Trace_Stream logger;

    enum Type
    {
      A, AAAA, PTR_A, PTR_AAAA
    };

    struct Data_A
    {
      const char *name;
      std::function<void (const dns_rr_a4 *)> *cb;
      bool search;
    };

    struct Data_AAAA
    {
      const char *name;
      std::function<void (const dns_rr_a6 *)> *cb;
      bool search;
    };

    struct Data_PTR_A
    {
      const in_addr *addr;
      std::function<void (const dns_rr_ptr *)> *cb;
    };

    struct Data_PTR_AAAA
    {
      const in6_addr *addr;
      std::function<void (const dns_rr_ptr *)> *cb;
    };

    Type type_;
    union
    {
      Data_A a;
      Data_AAAA aaaa;
      Data_PTR_A ptr_a;
      Data_PTR_AAAA ptr_aaaa;
    } u;
    mutable bool owned_;

   protected:
    explicit Request(const Type type)
      : type_(type), owned_(true)
    {
      TRACE(logger.debug(), "Request type=" << type);
    }

   public:
    Request(const Request &src)
      : type_(src.type_), owned_(true)
    {
      u = src.u;
      src.owned_ = false;
    }

    Request &operator =(const Request &src)
    {
      u = src.u;
      src.owned_ = false;
      owned_ = true;
    }

    ~Request();

    static Request make_a(
      const char * const name,
      std::function<void (const dns_rr_a4 *)> * const cb,
      const bool search);
    static Request make_aaaa(
      const char * const name,
      std::function<void (const dns_rr_a6 *)> * const cb,
      const bool search);
    static Request make_ptr_a(
      const in_addr * const addr,
      std::function<void (const dns_rr_ptr *)> * const cb);
    static Request make_ptr_aaaa(
      const in6_addr * const addr,
      std::function<void (const dns_rr_ptr *)> * const cb);

    void submit(dns_ctx * const ctx) const;
  };

 public:
  explicit Async_Resolver(Async_Handler &handler);
  Async_Resolver(Async_Resolver const &) = delete;
  Async_Resolver &operator =(Async_Resolver const &) = delete;
  ~Async_Resolver();

#if defined(HAVE_UDNS)
  void resolve_a(const char *name,
                 const std::function<void (const dns_rr_a4 *)> &cb,
                 bool search = true);

  void resolve_aaaa(const char *name,
                    const std::function<void (const dns_rr_a6 *)> &cb,
                    bool search = true);

  void resolve_ptr(const struct in_addr *addr,
                   const std::function<void (const dns_rr_ptr *)> &cb);

  void resolve_ptr(const struct in6_addr *addr,
                   const std::function<void (const dns_rr_ptr *)> &cb);
#endif

 protected:
  void process(const bool need_recv, const bool need_send) const;

  void on_timeout(Async_Timer::reg_t reg);

#if defined(HAVE_UDNS)
  inline socket_t socket() const
  { return dns_sock(ctx_); }
#endif

  void on_recv() const;
  void on_send() const;

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
  inline const Event_Data *event_data() const
  { return &event_data_; }
#endif

 private:
  Async_Handler &handler_;

#if defined(HAVE_UDNS)
  dns_ctx * const ctx_;
#endif

  mutable Async_Timer timer_;
  mutable Async_Timer::reg_t timer_reg_;

  mutable std::mutex processing_sync_;

  mutable bool processing_;
  mutable bool queued_recv_;
  mutable bool queued_send_;

  mutable std::deque<Request> queued_req_;

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
  const Event_Data event_data_;
#endif
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
