#	-*- Makefile -*-
# Copyright (C) 2009, Christof Meerwald
# http://cmeerw.org
#

PLATFORM:=win32

SUFFIX_OBJ:=.obj
SUFFIX_PICOBJ:=.obj
SUFFIX_EXE:=.exe
SUFFIX_SO:=.dll

NAME_SO=$(1)$(SUFFIX_SO)
NAME_EXE=$(1)$(SUFFIX_EXE)

DIR_BOOST:=d:/util/boost_1_38_0

# the compiler to use
CXX:=cl
CXXLINK:=cl
CFLAGS_COMPILER:=-EHsc -MD
CFLAGS_CONFIG:=
DEFINES=

LDFLAGS:=mswsock.lib wsock32.lib ws2_32.lib
COMMONLIBS:=


RULE_OBJ=$(CXX) -c -Fo'$(2)' $(CFLAGS) '$(1)'
RULE_PICOBJ=$(CXX) -c -Fo'$(2)' $(CFLAGS) '$(1)'

RULE_EXE=$(CXX) -Fe'$(2)' $(foreach file,$(1),'$(file)') \
	$(CFLAGS) $(LDFLAGS) $(foreach dir,$(LIBPATH),//link /libpath:'$(dir)') \
	$(foreach lib,$(3),'lib$(lib).lib')
