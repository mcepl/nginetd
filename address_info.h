/*	-*- C++ -*-
 * Copyright (C) 2011-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ADDRESS_INFO__H
#define INCLUDED__ADDRESS_INFO__H

#include <cstring>

#include <sys/socket.h>
#include <netdb.h>

class Address_Info
{
  struct addrinfo *res_;
  int rc_;

 public:
  class const_iterator
  {
    const struct addrinfo *ptr_;

   public:
    inline explicit const_iterator(const struct addrinfo *ptr)
      : ptr_(ptr)
    { }

    inline const struct addrinfo *operator ->() const
    {
      return ptr_;
    }

    inline const struct addrinfo &operator *() const
    {
      return *ptr_;
    }

    inline const_iterator &operator ++()
    {
      ptr_ = ptr_->ai_next;
      return *this;
    }

    inline const_iterator operator ++(int)
    {
      return const_iterator(ptr_->ai_next);
    }

    inline bool operator ==(const const_iterator &iter)
    {
      return ptr_ == iter.ptr_;
    }

    inline bool operator !=(const const_iterator &iter)
    {
      return ptr_ != iter.ptr_;
    }
  };

  inline Address_Info(const char *hostname, const char *service)
    : res_(NULL), rc_(getaddrinfo(hostname, service, NULL, &res_))
  { }

  inline Address_Info(const char *hostname, const char *service,
		      const struct addrinfo &hints)
    : res_(NULL), rc_(getaddrinfo(hostname, service, &hints, &res_))
  { }

  inline Address_Info(const char *hostname, const char *service,
		      int flags, int family = AF_UNSPEC, int socktype = 0,
		      int protocol = 0)
    : res_(NULL)
  {
    addrinfo hints;
    std::memset(&hints, 0, sizeof(hints));

    hints.ai_flags = flags;
    hints.ai_family = family;
    hints.ai_socktype = socktype;
    hints.ai_protocol = protocol;

    rc_ = getaddrinfo(hostname, service, &hints, &res_);
  }

  inline ~Address_Info()
  {
    if (res_)
    {
      freeaddrinfo(res_);
    }
  }

  inline operator bool() const
  {
    return rc_ == 0;
  }

  inline int error() const
  {
    return rc_;
  }

  inline const_iterator begin() const
  {
    return const_iterator(res_);
  }

  inline const_iterator end() const
  {
    return const_iterator(NULL);
  }
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
