/*	-*- C++ -*-
 * Copyright (C) 2003-2007, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__SOCKET_STREAM__H
#define INCLUDED__SOCKET_STREAM__H

#include <iostream>
#include <streambuf>
#include <string>


// forward declaration
class Fiber_Socket;


/// thread-specific trace buffer
class Socket_Stream_Buffer
  : public std::streambuf
{
 public:
  explicit Socket_Stream_Buffer(Fiber_Socket &socket);

  virtual ~Socket_Stream_Buffer();


 protected:
  /**
   * Called by the framework on internal output buffer overflow.
   *
   * @param c optional character to add to the output stream (EOF if
   *        no character to add)
   * @return 0 if successful, EOF otherwise
   */
  virtual int overflow(int c);

  /**
   * Called by the framework on internal inupt buffer underflow.
   *
   * @return always returns EOF to indicate an error
   */
  virtual int underflow();

  /**
   * Called by the framework if buffers should be flushed.
   *
   * @return 0 if successful, EOF otherwise
   */
  virtual int sync();


  static const size_t BUF_SIZE = 2047;


 private:
  char read_buffer_[BUF_SIZE + 1];
  char write_buffer_[BUF_SIZE + 1];
  Fiber_Socket &socket_;
};

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
