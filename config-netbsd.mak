#	-*- Makefile -*-
# Copyright (C) 2012, Christof Meerwald
# http://cmeerw.org
#

DIR_LUA=
LIB_LUA=lua
CFLAGS_COMPILER=-pthread -I/usr/pkg/include -L/usr/pkg/lib \
	-Wl,-rpath=/usr/pkg/lib
CFLAGS_CONFIG=-O -g

DEFINES=POSIX HAVE_UDNS HAVE_KQUEUE
