require('nginet')

http = Service('http', 8080)

function http:bind()
   return { { '127.0.0.1', self._port },
	    { '127.0.0.2', self._port },
	    { '::1', self._port } }
end

function http:dnsbl(proto, addr, port)
   if proto == 'IPv4' then
      return { 'proxies.dnsbl.sorbs.net', 'cbl.abuseat.org' }
   elseif proto == 'IPv6' then
      return { 'virbl.dnsbl.bit.nl' }
   end
end

function http:check_accept(conn, dnsbl)
   local data = conn.receive('\r\n\r\n', 10)
   if data then
      local pos = string.find(data, '\r\n', 1, true)

      local req
      if pos then
	 req = string.sub(data, 1, pos - 1)
      else
	 req = data
      end

      pos = string.find(data, ' ', 1, true)
      local method = string.sub(data, 1, pos - 1)

      if not next(dnsbl) and method == 'GET' then
	 conn.send('HTTP/1.0 200 OK\r\n' ..
		   'Content-type: text/plain\r\n' ..
		   '\r\n' ..
		   'OK\n')
      else
	 conn.send('HTTP/1.0 403 Forbidden\r\n' ..
		   'Content-type: text/plain\r\n' ..
		   '\r\n' ..
		   'Forbidden\n')
      end
   end
end

return http
