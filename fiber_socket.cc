/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "fiber_socket.h"

#include "async_handler.h"
#include "fiber_handler.h"


Trace_Stream Fiber_Socket::logger("Fiber_Socket");


Fiber_Socket::Fiber_Socket(Async_Handler &handler, Async_Timer &timer,
			   socket_t socket)
  : handler_(handler), timer_(timer), async_(handler.attach(socket)),
    recv_timer_(timer_.register_timer()),
    send_timer_(timer_.register_timer())
{ }

Fiber_Socket::~Fiber_Socket()
{
  TRACE(logger.debug(), "~Fiber_Socket");
  async_->close();
  handler_.release(std::move(async_));
}


int Fiber_Socket::send(const char *data, size_t len, int flags, unsigned int timeout)
{
  send_completed_ = false;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(send_completed_sync_);
    Async_Socket::req_id_t req_id =
      async_->send(data, len, flags, [this, fiber] (int error)
		   { on_send_complete(fiber, error); });
    if (!send_completed_)
    {
      if (timeout)
      {
	timer_.update_timer(send_timer_, timeout,
			    [this, fiber, req_id] (Async_Timer::reg_t)
			    { on_send_timeout(fiber, req_id); });
      }

      Fiber_Handler::create_fiber([this] () { on_send_wait(); }, 2048);
    }
  }

  return send_error_;
}

void Fiber_Socket::on_send_complete(void *fiber, int error)
{
  timer_.disable_timer(send_timer_);

  send_error_ = error;

  if (fiber == Fiber_Handler::current_fiber())
  {
    send_completed_ = true;
  }
  else
  {
    send_completed_sync_.lock();
    send_completed_ = true;
    Fiber_Handler::schedule_fiber(fiber);
  }
}

void Fiber_Socket::on_send_timeout(void *fiber, Async_Socket::req_id_t req_id)
{
  if (async_->cancel_send(req_id))
  {
    if (fiber == Fiber_Handler::current_fiber())
    {
      send_error_ = ECANCELED;
      send_completed_ = true;
    }
    else
    {
      send_completed_sync_.lock();
      send_error_ = ECANCELED;
      send_completed_ = true;
      Fiber_Handler::schedule_fiber(fiber);
    }
  }
}

void Fiber_Socket::on_send_wait()
{
  send_completed_sync_.unlock();
}

#if defined(POSIX)
int Fiber_Socket::sendmsg(const struct iovec *iov, int iov_len,
			  const char *cdata, size_t clen,
			  int flags, unsigned int timeout)
{
  send_completed_ = false;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(send_completed_sync_);
    Async_Socket::req_id_t req_id =
      async_->sendmsg(iov, iov_len, cdata, clen, flags,
		      [this, fiber] (int err) { on_send_complete(fiber, err); });
    if (!send_completed_)
    {
      if (timeout)
      {
	timer_.update_timer(send_timer_, timeout,
			    [this, fiber, req_id] (Async_Timer::reg_t)
			    { on_send_timeout(fiber, req_id); });
      }

      Fiber_Handler::create_fiber([this] () { on_send_wait(); }, 2048);
    }
  }

  return send_error_;
}


int Fiber_Socket::sendmsg(const char *data, size_t len,
			  const char *cdata, size_t clen,
			  int flags, unsigned int timeout)
{
  send_completed_ = false;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(send_completed_sync_);
    Async_Socket::req_id_t req_id =
      async_->sendmsg(data, len, cdata, clen, flags,
		      [this, fiber] (int err) { on_send_complete(fiber, err); });
    if (!send_completed_)
    {
      if (timeout)
      {
	timer_.update_timer(send_timer_, timeout,
			    [this, fiber, req_id] (Async_Timer::reg_t)
			    { on_send_timeout(fiber, req_id); });
      }

      Fiber_Handler::create_fiber([this] () { on_send_wait(); }, 2048);
    }
  }

  return send_error_;
}
#endif


int Fiber_Socket::recv(char *buffer, size_t &len, int flags,
		       unsigned int timeout)
{
  recv_completed_ = false;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(recv_completed_sync_);
    Async_Socket::req_id_t req_id =
      async_->recv(buffer, len,
		   [this, fiber] (int err, size_t len)
		   { on_recv_complete(fiber, err, len); },
		   flags);

    if (!recv_completed_)
    {
      if (timeout)
      {
	timer_.update_timer(recv_timer_, timeout,
			    [this, fiber, req_id] (Async_Timer::reg_t)
			    { on_recv_timeout(fiber, req_id); });
      }

      Fiber_Handler::create_fiber([this] () { on_recv_wait(); }, 2048);
    }
  }

  len = recv_len_;
  return recv_error_;
}

void Fiber_Socket::on_recv_complete(void *fiber, int error, size_t len)
{
  timer_.disable_timer(recv_timer_);

  recv_error_ = error;
  recv_len_ = len;

  if (fiber == Fiber_Handler::current_fiber())
  {
    recv_completed_ = true;
  }
  else
  {
    recv_completed_sync_.lock();
    recv_completed_ = true;
    Fiber_Handler::schedule_fiber(fiber);
  }
}

void Fiber_Socket::on_recv_timeout(void *fiber, Async_Socket::req_id_t req_id)
{
  if (async_->cancel_recv(req_id))
  {
    if (fiber == Fiber_Handler::current_fiber())
    {
      recv_error_ = ECANCELED;
      recv_completed_ = true;
    }
    else
    {
      recv_completed_sync_.lock();
      recv_error_ = ECANCELED;
      recv_completed_ = true;
      Fiber_Handler::schedule_fiber(fiber);
    }
  }
}

void Fiber_Socket::on_recv_wait()
{
  recv_completed_sync_.unlock();
}

#if defined(POSIX)
int Fiber_Socket::recvmsg(char *buffer, size_t &len,
			  char *cbuffer, size_t &clen, int &flags,
			  unsigned int timeout)
{
  recv_completed_ = false;
  void * const fiber = Fiber_Handler::current_fiber();

  {
    std::lock_guard<std::mutex> guard(recv_completed_sync_);
    Async_Socket::req_id_t req_id =
      async_->recvmsg(buffer, len, cbuffer, clen,
		     [this, fiber] (int err, size_t len, size_t clen, int flags)
		     { on_recvmsg_complete(fiber, err, len, clen, flags); },
		     flags);
    if (!recv_completed_)
    {
      if (timeout)
      {
	timer_.update_timer(recv_timer_, timeout,
			    [this, fiber, req_id] (Async_Timer::reg_t)
			    { on_recv_timeout(fiber, req_id); });
      }

      Fiber_Handler::create_fiber([this] () { on_recv_wait(); }, 2048);
    }
  }

  len = recv_len_;
  clen = recv_clen_;
  flags = recv_flags_;
  return recv_error_;
}

void Fiber_Socket::on_recvmsg_complete(void *fiber, int error,
				       size_t len, size_t clen, int flags)
{
  TRACE(logger.debug(), "on_recv_complete");
  recv_error_ = error;
  recv_len_ = len;
  recv_clen_ = clen;
  recv_flags_ = flags;

  if (fiber == Fiber_Handler::current_fiber())
  {
    recv_completed_ = true;
  }
  else
  {
    recv_completed_sync_.lock();
    recv_completed_ = true;
    Fiber_Handler::schedule_fiber(fiber);
  }
}
#endif


/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
