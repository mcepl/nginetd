/*	-*- C++ -*-
 * Copyright (C) 2003-2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__ASYNC_HANDLER__H
#define INCLUDED__ASYNC_HANDLER__H

#include "trace.h"

#include "socket.h"

#if defined(POSIX)
#include <pthread.h>
#endif

#include <functional>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
#include <deque>
#include <map>
#endif


// forward declaration
class Async_Resolver;
class Async_Socket;
class Async_Timer;
class Async_Worker;
struct Event_Data_Base;
struct Event_Data;

class Async_Handler
{
  static Trace_Stream logger;

  struct Recursion_Guard
  {
    Recursion_Guard(unsigned int &depth)
      : depth_(depth), recurse_(depth_ < MAX_RECURSION_DEPTH)
    {
      if (recurse_)
      {
	++depth_;
      }
    }

    Recursion_Guard(Recursion_Guard const &) = delete;
    Recursion_Guard(Recursion_Guard &&src) = default;

    ~Recursion_Guard()
    {
      if (recurse_)
      {
	--depth_;
      }
    }

    operator bool() const
    {
      return recurse_;
    }

  private:
    static const unsigned int MAX_RECURSION_DEPTH = 8;

    unsigned int &depth_;
    bool const recurse_;
  };

 public:
  typedef std::function<void (socket_t, int)> accept_handler;
  typedef std::function<void (int)> connect_handler;
  typedef std::function<void (int, size_t)> recv_handler;
  typedef std::function<void (int)> send_handler;

#if defined(POSIX)
  typedef std::function<void (int, size_t, size_t, int)> recvmsg_handler;
  typedef send_handler sendmsg_handler;
#endif


  explicit Async_Handler(int nr_workers = 5,
			 std::function<void (std::function<void ()> const &)> const &func = std::function<void (std::function<void ()> const &)>());
  Async_Handler(Async_Handler const &) = delete;
  Async_Handler &operator =(Async_Handler const &) = delete;
  ~Async_Handler();


  inline Recursion_Guard recurse() const
  {
    return { recursion_depth_ };
  }

  std::unique_ptr<Async_Socket> attach(socket_t socket);
#if defined(HAVE_EPOLL)
  void register_events(const Async_Socket &sock, bool in, bool out);
#elif defined(HAVE_KQUEUE)
  void register_recv(const Async_Socket &sock, bool enable = true);
  void register_send(const Async_Socket &sock, bool enable = true);
#endif
  void detach(std::unique_ptr<Async_Socket> async_socket);
  void release(std::unique_ptr<Async_Socket> async_socket);

#if defined(HAVE_UDNS)
  void attach(const Async_Resolver &async_resolver);
#endif

#if defined(HAVE_KQUEUE)
  void attach(const Async_Timer &timer, unsigned int timeout);
#else
  void attach(const Async_Timer &timer);
#endif
  bool detach(const Async_Timer &timer);

  void attach_listener(socket_t socket, const accept_handler &cb);
  bool detach_listener(socket_t socket);

#if defined(_WIN32)
  inline DWORD tls_key() const
  { return tls_key_; }
#elif defined(HAVE_EPOLL)
  inline int epoll_fd() const
  { return epoll_fd_; }
#elif defined(HAVE_KQUEUE)
  inline int kqueue_fd() const
  { return kqueue_fd_; }
#endif

 private:
  static thread_local unsigned int recursion_depth_;

  std::vector<Async_Worker> workers_;
  std::vector<std::thread> worker_threads_;

#if defined(_WIN32)
  HANDLE completion_port_;
#else
  int pipe_fds_[2];
#endif

#if defined(HAVE_EPOLL)
  int epoll_fd_;
#elif defined(HAVE_KQUEUE)
  int kqueue_fd_;
#endif

#if defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
  std::mutex attached_listeners_sync_;
  typedef std::map<socket_t, Event_Data *> Attached_Listeners_Map;
  Attached_Listeners_Map attached_listeners_;

  std::mutex socket_pool_sync_;
  std::deque<std::unique_ptr<Async_Socket>> socket_pool_;
#endif
};


struct Event_Data_Base
{
  enum Event_Operation
  {
    OP_NONE    = 0x00,
    OP_CANCEL  = 0x01,
    OP_ACCEPT  = 0x02,
    OP_RECV    = 0x04,
    OP_SEND    = 0x08,
    OP_DATA    = OP_RECV | OP_SEND,
    OP_DNS     = 0x10,
    OP_TIMER   = 0x20
  };

  Event_Operation operation;

  struct accept_data
  {
    socket_t listener_socket;
    Async_Handler::accept_handler *callback;
  };

  struct timer_data
  {
    Async_Timer *timer;
  };

  union
  {
    struct accept_data accept;
    struct timer_data timer;
  };

  inline Event_Data_Base()
  { }

  inline explicit Event_Data_Base(Event_Operation operation)
    : operation(operation)
  { }

#if defined(HAVE_EPOLL) || defined(__FreeBSD__)
  inline void *native_handle() const
  {
    return const_cast<Event_Data_Base *>(this);
  }

  inline static Event_Data_Base *from_native_handle(void *ptr)
  {
    return static_cast<Event_Data_Base *>(ptr);
  }
#elif defined(__NetBSD__)
  inline uintptr_t native_handle() const
  {
    return reinterpret_cast<uintptr_t>(this);
  }

  inline static Event_Data_Base *from_native_handle(uintptr_t data)
  {
    return reinterpret_cast<Event_Data_Base *>(data);
  }
#endif
};

#if defined(_WIN32)
struct Event_Data
  : public Event_Data_Base
{
  Event_Data()
  {
    overlapped.hEvent = NULL;
  }

  OVERLAPPED overlapped;

  union
  {
    struct io_win32_data
    {
      socket_t socket;
    };

    struct recv_win32_data
      : public io_win32_data
    {
      Async_Handler::recv_handler *callback;
    } recv_win32;

    struct send_win32_data
      : public io_win32_data
    {
      Async_Handler::send_handler *callback;
    } send_win32;

    struct accept_win32_data
    {
      socket_t socket;
      char *output_buffer;
      DWORD bytes_received;
    } accept_win32;
  };
};
#elif defined(HAVE_EPOLL) || defined(HAVE_KQUEUE)
struct Event_Data
  : public Event_Data_Base
{
  inline Event_Data()
  { }

  inline explicit Event_Data(Event_Operation operation)
    : Event_Data_Base(operation)
  { }

  inline explicit Event_Data(const Async_Socket *async_socket)
    : Event_Data_Base(OP_DATA)
  {
    io_linux.async_socket = async_socket;
  }

  inline explicit Event_Data(const Async_Resolver *async_resolver)
    : Event_Data_Base(OP_DNS)
  {
    dns_linux.async_resolver = async_resolver;
  }

  inline explicit Event_Data(Async_Timer *async_timer)
    : Event_Data_Base(OP_TIMER)
  {
    timer.timer = async_timer;
  }

  struct io_linux_data
  {
    const Async_Socket *async_socket;
  };

  struct dns_linux_data
  {
    const Async_Resolver *async_resolver;
  };

  union
  {
    struct io_linux_data io_linux;
    struct dns_linux_data dns_linux;
  };
};
#endif

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
