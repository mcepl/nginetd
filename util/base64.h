#if !defined(BASE64_H__INCLUDED)
#define BASE64_H__INCLUDED

#include <string>

std::string base64_encode(unsigned char const* , unsigned int len);
std::string base64_decode(std::string const& s);

#endif
