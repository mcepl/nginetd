/*	-*- C++ -*-
 * Copyright (C) 2015, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#include "address_info.h"
#include "async_handler.h"
#include "async_resolver.h"
#include "async_socket.h"
#include "async_tls.h"
#include "trace.h"

#include "util/base64.h"
#include "util/sha1.h"

#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <algorithm>
#include <memory>
#include <vector>

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR)) \
    { }


namespace
{
  bool is_whitespace(char c)
  {
    return c == ' ' || c == '\t';
  }

  class Connection
    : public std::enable_shared_from_this<Connection>
  {
    static Trace_Stream logger;

    Async_Handler &handler_;
    Async_Resolver &resolver_;

    std::unique_ptr<Async_Tls> tls_sock_;
    std::unique_ptr<Async_Socket> srv_sock_;
    char buffer_[8192];
    size_t recvd_{ 0 };
    size_t line_end_{ 0 };
    size_t req_end_{ 0 };
    size_t hdr_begin_{ 0 };

    size_t offset_{ 0 };
    size_t hdr_len_{ 0 };
    bool parsed_msg_hdr_{ false };

    bool is_masked_{ false };
    uint8_t opcode_{ 0 };
    unsigned char masking_key_[4]{ };
    size_t masking_offset_{ 0 };
    uint64_t payload_len_{ 0 };
    time_t clt_timestamp_{ 0 };

    char srv_buffer_[8192];
    size_t srv_recvd_{ 0 };
    size_t srv_msg_begin_{ 0 };
    size_t srv_pos_{ 0 };
    bool srv_had_open_tag_{ false };
    bool srv_in_tag_{ false };
    bool srv_had_question_{ false };
    bool srv_had_slash_{ false };
    unsigned int srv_tag_depth_{ 0 };
    bool srv_continuation_{ false };

    std::string req_path_;
    std::string websocket_key_;
    bool connection_upgrade_{ false };
    bool upgrade_websocket_{ false };
    bool protocol_xmpp_{ false };

  public:
    explicit Connection(Async_Handler &handler, Async_Resolver &resolver,
			std::unique_ptr<Async_Socket> sock)
      : handler_(handler), resolver_(resolver),
	tls_sock_(new Async_Tls(std::move(sock)))
    { }

    ~Connection()
    {
      TRACE(logger.debug(), "~Connection");
    }

    void operator () ()
    {
      auto && self (shared_from_this());
      tls_sock_->handshake([self] (int err) { self->on_handshake(err); });
    }

  protected:
    void on_handshake(int err)
    {
      TRACE(logger.debug(), "on_handshake err=" << err);

      auto && self (shared_from_this());
      if (! err)
      {
	clt_timestamp_ = time(NULL);
	tls_sock_->recv(buffer_, sizeof(buffer_),
			[self] (int err, size_t len)
			{ self->on_receive<&Connection::recv_req_handler>(err, len); });
      }
      else
      {
	auto raw_sock(tls_sock_->detach());
	raw_sock->close();
	handler_.release(std::move(raw_sock));
      }
    }

    template<void (Connection::*hdlr) ()>
    void on_receive(int err, size_t len)
    {
      if (!err && len)
      {
	recvd_ += len;
	(this->*hdlr) ();
      }
      else
      {
	TRACE(logger.debug(), "on_receive connection closed, use_count="
	      << shared_from_this().use_count());

	if (srv_sock_)
	{
	  srv_sock_->shutdown(SHUT_WR);
	}

	auto raw_sock(tls_sock_->detach());
	raw_sock->close();
	handler_.release(std::move(raw_sock));
      }
    }

    template<void (Connection::*hdlr) ()>
    void on_srv_receive(int err, size_t len)
    {
      if (!err && len)
      {
	srv_recvd_ += len;
	(this->*hdlr) ();
      }
      else
      {
	TRACE(logger.debug(), "on_srv_receive connection closed, use_count="
	      << shared_from_this().use_count());

	if (tls_sock_)
	{
	  tls_sock_->shutdown(SHUT_WR);
	}

	srv_sock_->close();
	handler_.release(std::move(srv_sock_));
      }
    }

    void recv_req_handler()
    {
      static const char crlf[] = { '\r' , '\n' };

      auto const iter (std::search(buffer_, buffer_ + recvd_,
				   std::begin(crlf), std::end(crlf)));
      if (iter != buffer_ + recvd_)
      {
	line_end_ = req_end_ = iter - buffer_;
	hdr_begin_ = line_end_ + 2;
	TRACE(logger.debug(), "got request \"" << std::string(buffer_, iter)
	      << '"');

	auto const path_begin(std::find(buffer_, iter, ' '));
	if (path_begin != iter)
	{
	  auto const path_end(std::find(path_begin + 1, iter, ' '));
	  if ((path_end != iter) && (path_begin[1] == '/'))
	  {
	    req_path_.assign(path_begin + 1, path_end);
	  }
	}

	recv_hdr_handler();
      }
      else
      {
	auto && self (shared_from_this());
	tls_sock_->recv(buffer_ + recvd_, sizeof(buffer_) - recvd_,
			[self] (int err, size_t len)
			{ self->on_receive<&Connection::recv_req_handler>(err, len); });
      }
    }

    void process_header()
    {
      auto const begin_hdr(buffer_ + hdr_begin_);
      auto const end_hdr(buffer_ + line_end_);

      auto iter (std::find(begin_hdr, end_hdr, ':'));
      if (iter != end_hdr)
      {
	std::string const key (begin_hdr, iter);
	std::string const value (std::find_if_not(iter + 1, end_hdr,
						  is_whitespace),
				 end_hdr);

	TRACE(logger.debug(), "got header \"" << key << "\": \""
	      << value << '"');
	if (key == "Connection")
	{
	  connection_upgrade_ = (value == "Upgrade");
	}
	else if (key == "Upgrade")
	{
	  upgrade_websocket_ = (value == "websocket");
	}
	else if (key == "Sec-WebSocket-Key")
	{
	  websocket_key_ = value;
	}
	else if (key == "Sec-WebSocket-Protocol")
	{
	  protocol_xmpp_ = (value == "xmpp");
	}
      }

      hdr_begin_ = end_hdr + 2 - buffer_;
    }

    void recv_hdr_handler()
    {
      static const char crlf[] = { '\r' , '\n' };

      auto end_data (buffer_ + recvd_);
      while (true)
      {
	auto begin_data (buffer_ + line_end_ + 2);
	auto iter (std::search(begin_data, end_data,
			       std::begin(crlf), std::end(crlf)));
	if (iter != end_data)
	{
	  line_end_ = iter - buffer_;

	  if (iter == begin_data)
	  {
	    if (buffer_ + hdr_begin_ != begin_data)
	    {
	      process_header();
	    }

	    on_request_received();
	  }
	  else if (is_whitespace(begin_data[0]))
	  {
	    // this is a continuation line
	    continue;
	  }
	  else
	  {
	    process_header();
	    continue;
	  }
	}
	else
	{
	  auto && self (shared_from_this());
	  tls_sock_->recv(buffer_ + recvd_, sizeof(buffer_) - recvd_,
			  [self] (int err, size_t len)
			  { self->on_receive<&Connection::recv_hdr_handler>(err, len); });
	}

	break;
      }
    }

    void on_request_received()
    {
      auto && self (shared_from_this());

      if (upgrade_websocket_ && protocol_xmpp_ && // TODO connection_upgrade_ &&
	  !req_path_.empty() && !websocket_key_.empty())
      {
	std::string sha1_input(websocket_key_);
	sha1_input += "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

	unsigned char hash[20];
	sha1::calc(sha1_input.c_str(), sha1_input.length(), hash);

	std::string b64hash(base64_encode (hash, sizeof(hash)));
	TRACE(logger.debug(), "Sec-WebSocket-Accept: " << b64hash);

	std::string response("HTTP/1.1 101 Switching Protocols\r\n"
			     "Upgrade: websocket\r\n"
			     "Connection: Upgrade\r\n"
			     "Sec-WebSocket-Accept: ");
	response += b64hash;
	response += "\r\n" "\r\n";
	tls_sock_->send(response.c_str(), response.length(), MSG_NOSIGNAL);

	recvd_ = 0;

	resolver_.resolve_aaaa(req_path_.c_str() + 1,
			       [self] (const dns_rr_a6 *rr)
			       { self->on_resolved(rr); });
      }
      else
      {
	std::string response("HTTP/1.1 404 Not Found\r\n"
			     "\r\n");
	tls_sock_->send(response.c_str(), response.length(), MSG_NOSIGNAL,
			[self] (int err) { self->tls_sock_->shutdown(SHUT_RDWR); });
      }
    }

    void on_resolved(const dns_rr_a6 *rr)
    {
      TRACE(logger.debug(), "on_resolved");

      if (rr && rr->dnsa6_nrr)
      {
	srv_sock_ = handler_.attach(::socket(AF_INET6, SOCK_STREAM, 0));

	struct sockaddr_in6 addr{ };
	addr.sin6_family = AF_INET6;
	addr.sin6_addr = rr->dnsa6_addr[0];
	addr.sin6_port = htons(5222);

	auto && self (shared_from_this());
	srv_sock_->connect(reinterpret_cast<const struct sockaddr *>(&addr),
			   sizeof(addr),
			   [self] (int err)
			   { self->on_connect(err); });
      }
    }

    void on_connect(int err)
    {
      TRACE(logger.debug(), "on_connect");

      if (!err)
      {
	auto && self (shared_from_this());

	tls_sock_->recv(buffer_, sizeof(buffer_),
			[self] (int err, size_t len)
			{ self->on_receive<&Connection::recv_msg_handler>(err, len); });

	srv_sock_->recv(srv_buffer_, sizeof(srv_buffer_),
			[self] (int err, size_t len)
			{ self->on_srv_receive<&Connection::recv_srv_data>(err, len); });
      }
    }

    void recv_msg_handler()
    {
      auto && self (shared_from_this());

      while (true)
      {
	unsigned char *data(reinterpret_cast<unsigned char *>(buffer_) + offset_);

	if (!parsed_msg_hdr_ && recvd_ >= 2)
	{
	  is_masked_ = (data[1] & 0x80);
	  payload_len_ = (data[1] & 0x7f);

	  hdr_len_ = 2 +
	    (is_masked_ ? 4 : 0) +
	    ((payload_len_ == 126) ? 2 : 0) +
	    ((payload_len_ == 127) ? 8 : 0);

	  if (recvd_ >= hdr_len_)
	  {
	    opcode_ = data[0] & 0x0f;

	    if (is_masked_)
	    {
	      std::copy(data + hdr_len_ - 4, data + hdr_len_, masking_key_);
	      masking_offset_ = 0;
	    }

	    if (payload_len_ >= 126)
	    {
	      payload_len_ =
		((data[2] & 0xffULL) << 8) |
		((data[3] & 0xffULL));
	    }
	    else if (payload_len_ == 127)
	    {
	      payload_len_ =
		((data[2] & 0xffULL) << 56) |
		((data[3] & 0xffULL) << 48) |
		((data[4] & 0xffULL) << 40) |
		((data[5] & 0xffULL) << 32) |
		((data[6] & 0xffULL) << 24) |
		((data[7] & 0xffULL) << 16) |
		((data[8] & 0xffULL) << 8) |
		((data[9] & 0xffULL));
	    }

	    parsed_msg_hdr_ = true;
	    TRACE(logger.debug(), "parsed message header opcode="
		  << static_cast<unsigned int>(opcode_)
		  << ", payload length=" << payload_len_);
	  }
	}

	if (parsed_msg_hdr_ && !(opcode_ & 0x8))
	{
	  size_t const len(std::min(static_cast<uint64_t>(recvd_ - hdr_len_), payload_len_));

	  if (is_masked_)
	  {
	    unmask_data(data + hdr_len_, data + hdr_len_ + len);
	  }

	  TRACE(logger.debug(), "received payload \""
		<< std::string(data + hdr_len_, data + hdr_len_ + len) << '"');
	  srv_sock_->send(buffer_ + offset_ + hdr_len_, len, MSG_NOSIGNAL);

	  payload_len_ -= len;

	  if (!payload_len_)
	  {
	    recvd_ -= hdr_len_ + len;
	    offset_ += hdr_len_ + len;
	    parsed_msg_hdr_ = false;
	    continue;
	  }
	  else
	  {
	    offset_ = recvd_ = 0;
	  }
	}
	else if (parsed_msg_hdr_ && (opcode_ & 0x8) &&
		 (recvd_ >= hdr_len_ + payload_len_))
	{
	  if (is_masked_)
	  {
	    unmask_data(data + hdr_len_, data + hdr_len_ + payload_len_);
	  }

	  switch (opcode_)
	  {
	  case 0x8:
	   TRACE(logger.debug(), "received connection close");
	   {
	     unsigned char close_msg[] = { 0x8a, 0 };
	     tls_sock_->send(reinterpret_cast<char *>(close_msg), sizeof(close_msg),
			     MSG_NOSIGNAL,
			     [self] (int err)
			     {
			       if (self->srv_sock_)
			       {
				 self->srv_sock_->shutdown(SHUT_WR);
			       }

			       self->tls_sock_->shutdown(SHUT_WR);
			     });
	   }
	   break;

	  case 0x9:
	   TRACE(logger.debug(), "received ping \""
		 << std::string(data + hdr_len_, data + hdr_len_ + payload_len_)
		 << '"');

	   if (is_masked_)
	   {
	     std::copy(data, data + hdr_len_ - 4, data + 4);
	     data += 4;
	     hdr_len_ -= 4;
	     offset_ += 4;
	   }

	   data[0] = 0x8a;
	   data[1] = data[1] & 0x7f;

	   tls_sock_->send(buffer_ + offset_, hdr_len_ + payload_len_,
			   MSG_NOSIGNAL);
	   break;

	  case 0xa:
	   TRACE(logger.debug(), "received pong \""
		 << std::string(data + hdr_len_, data + hdr_len_ + payload_len_)
		 << '"');

	   break;
	  }

	  recvd_ -= hdr_len_ + payload_len_;
	  offset_ += hdr_len_ + payload_len_;
	  parsed_msg_hdr_ = false;
	  continue;
	}
	else if (offset_)
	{
	  std::copy(buffer_ + offset_, buffer_ + offset_ + recvd_, buffer_);
	  offset_ = 0;
	}

	break;
      }

      tls_sock_->recv(buffer_ + recvd_, sizeof(buffer_) - recvd_,
		      [self] (int err, size_t len)
		      { self->on_receive<&Connection::recv_msg_handler>(err, len); });
    }

    void recv_srv_data()
    {
      auto && self (shared_from_this());

      while (srv_pos_ != srv_recvd_)
      {
	const char c = srv_buffer_[srv_pos_];

	if (srv_had_open_tag_)
	{
	  if (c == '/')
	  {
	    --srv_tag_depth_;
	  }
	  else if (c == '?')
	  {
	    srv_tag_depth_ = 0;
	  }
	  else
	  {
	    ++srv_tag_depth_;
	  }

	  srv_had_open_tag_ = false;
	}
	else if (! srv_in_tag_)
	{
	  if (c == '<')
	  {
	    srv_had_open_tag_ = true;
	    srv_in_tag_ = true;
	    srv_had_slash_ = false;
	    srv_had_question_ = false;

	    if (srv_tag_depth_ == 1)
	    {
	      srv_msg_begin_ = srv_pos_;
	    }
	  }
	}
	else if (srv_in_tag_)
	{
	  if (c == '>')
	  {
	    if (srv_had_slash_)
	    {
	      --srv_tag_depth_;
	      srv_had_slash_ = false;
	    }

	    if (((srv_tag_depth_ == 0) && ! srv_had_question_) ||
		(srv_tag_depth_ == 1))
	    {
	      // send
	      TRACE(logger.debug(), "received server message \""
		    << std::string(srv_buffer_ + srv_msg_begin_,
				   srv_buffer_ + srv_pos_ + 1)
		    << '"');

	      send_message(srv_buffer_ + srv_msg_begin_,
			   srv_pos_ + 1 - srv_msg_begin_,
			   true /* fin */);
	      srv_msg_begin_ = srv_pos_ + 1;
	    }

	    srv_in_tag_ = false;
	  }
	  else
	  {
	    srv_had_slash_ = (c == '/');
	    srv_had_question_ = (c == '?');
	  }
	}


	++srv_pos_;
      }

      if (srv_pos_ == sizeof(srv_buffer_))
      {
	const size_t len{ srv_pos_ - srv_msg_begin_ };
	if (len > sizeof(srv_buffer_) / 4)
	{
	  send_message(srv_buffer_ + srv_msg_begin_,
		       len,
		       false /* fin */);
	  srv_pos_ = srv_recvd_ = 0;
	}
	else
	{
	  std::copy(srv_buffer_ + srv_msg_begin_,
		    srv_buffer_ + srv_pos_,
		    srv_buffer_);
	  srv_pos_ -= srv_msg_begin_;
	  srv_recvd_ = srv_pos_;
	}

	srv_msg_begin_ = 0;
      }

      if (clt_timestamp_ + 50 < time(NULL))
      {
	// send ping
	TRACE(logger.debug(), "sending ping to client");
	tls_sock_->send("\x89\x00", 2, MSG_NOSIGNAL);
      }

      srv_sock_->recv(srv_buffer_ + srv_pos_, sizeof(srv_buffer_) - srv_pos_,
		      [self] (int err, size_t len)
		      { self->on_srv_receive<&Connection::recv_srv_data>(err, len); });
    }

    void send_message(const char *data, size_t len, bool fin)
    {
      struct iovec iov[2];
      unsigned char hdr[4];

      hdr[0] = (srv_continuation_ ? 0x00 : 0x01) | (fin ? 0x80 : 0x00);
      if (len < 126)
      {
	hdr[1] = len & 0xff;

	iov[0].iov_base = hdr;
	iov[0].iov_len = 2;
      }
      else
      {
	hdr[1] = 126;
	hdr[2] = len >> 8;
	hdr[3] = len & 0xff;

	iov[0].iov_base = hdr;
	iov[0].iov_len = 4;
      }

      iov[1].iov_base = const_cast<char *>(data);
      iov[1].iov_len = len;

      TRACE(logger.debug(), "sending data to client, len=" << len);
      tls_sock_->sendmsg(iov, 2, MSG_NOSIGNAL);

      clt_timestamp_ = time(NULL);
      srv_continuation_ = ! fin;
    }

    void unmask_data(unsigned char *beg, unsigned char *end)
    {
      while (beg != end)
      {
	*beg = *beg ^ masking_key_[masking_offset_];
	++beg;
	masking_offset_ = (masking_offset_ + 1) % 4;
      }
    }
  };

  class Ws_Server
  {
    static Trace_Stream logger;

    Async_Handler &handler_;
    Async_Resolver &resolver_;
    socket_t listener_socket_;

  public:
    explicit Ws_Server(Async_Handler &handler, Async_Resolver &resolver)
      : handler_(handler), resolver_(resolver),
	listener_socket_(-1)
    { }

    void start()
    {
      Address_Info ainfo("::", "8443",
			 AI_PASSIVE | AI_ADDRCONFIG | AI_NUMERICHOST,
			 AF_UNSPEC, SOCK_STREAM);
      if (ainfo)
      {
	for (auto const & entry : ainfo)
	{
	  struct sockaddr * const addr = entry.ai_addr;
	  const socket_t s = ::socket(addr->sa_family, SOCK_STREAM, 0);

	  const int res_bind = ::bind(s, addr, entry.ai_addrlen);
	  if (!res_bind)
	  {
	    listener_socket_ = s;
	    handler_.attach_listener(s, [this] (socket_t sock, int err)
				     { on_accept(sock, err); });
	    break;
	  }
	  else
	  {
	    const int last_error = errno;
	    TRACE(logger.error(), "bind failed, errno=" << last_error);
	    RETRY_EINTR(::close(s));
	  }
	}
      }
    }

  protected:
    void on_accept(socket_t sock, int err)
    {
      if (!err)
      {
	auto && conn (std::make_shared<Connection> (handler_, resolver_,
						    handler_.attach(sock)));
	(*conn) ();
      }
    }
  };

  Trace_Stream Connection::logger("Connection");
  Trace_Stream Ws_Server::logger("Ws_Server");
}


int main(int argc, char *argv[])
{
  Trace_Stream::set_global_level(TRACE_DEBUG);

  const int nr_threads = (argc > 1) ? atoi(argv[1]) : 1;
  Async_Handler handler(nr_threads);
  Async_Resolver resolver(handler);
  Ws_Server srv(handler, resolver);
  srv.start();

#if defined(_WIN32)
  Sleep(3600000);
#else
  sleep(3600);
#endif
}

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
