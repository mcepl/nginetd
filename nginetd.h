/*	-*- C++ -*-
 * Copyright (C) 2008-2009, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef INCLUDED__NGINETD__H
#define INCLUDED__NGINETD__H

#include <stdexcept>
#include <string>
#include <stdint.h>

#include <sys/uio.h>

namespace nginetd
{
namespace msg
{
struct Header
{
 protected:
  typedef Header MSG_CLASS;
  const uint16_t msg_type_;

  inline explicit Header(const uint16_t msg_type)
    : msg_type_(msg_type)
  { }


 public:
  inline uint16_t msg_type() const
  {
    return msg_type_;
  }
};

struct Header_Proxy
  : Header
{
 protected:
  typedef Header_Proxy Proxy_Class;

 public:
  inline static const Proxy_Class &get(const char * const buffer,
				       const size_t length)
  {
    const Proxy_Class &self = *reinterpret_cast<const Proxy_Class *>(buffer);

    if (length < sizeof(MSG_CLASS))
    {
      throw std::invalid_argument("message buffer");
    }

    return self;
  }
};


struct Hello_Request
{
  static const uint16_t MSG = 0;

 protected:
  typedef Hello_Request MSG_CLASS;
  const uint16_t msg_type_;
  uint16_t version_;

  inline explicit Hello_Request(const uint16_t msg_type)
    : msg_type_(msg_type)
  { }

  inline Hello_Request(const Hello_Request &src)
    : msg_type_(src.msg_type_), version_(src.version_)
  { }


 public:
  inline uint16_t msg_type() const
  {
    return msg_type_;
  }

  inline uint16_t version() const
  {
    return version_;
  }
};

struct Hello_Request_Proxy
  : Hello_Request
{
 protected:
  typedef Hello_Request_Proxy Proxy_Class;

  inline bool validate(const char * const buffer, const size_t length) const
  {
    return true;
  }

 public:
  inline static const Proxy_Class &get(const char * const buffer,
				       const size_t length)
  {
    const Proxy_Class &self = *reinterpret_cast<const Proxy_Class *>(buffer);

    if ((length < sizeof(MSG_CLASS)) ||
	!self.validate(buffer, length))
    {
      throw std::invalid_argument("message buffer");
    }

    return self;
  }

  inline uint16_t version() const
  {
    return version_;
  }
};

struct Hello_Request_Builder
  : Hello_Request
{
 private:
  struct iovec iov_[1];

 public:
  Hello_Request_Builder()
    : Hello_Request(MSG)
  { }

  inline void version(const uint16_t value)
  {
    version_ = value;
  }

  inline iovec *iov()
  {
    iov_[0].iov_base = this;
    iov_[0].iov_len = sizeof(MSG_CLASS);

    return iov_;
  }

  inline int iovlen()
  {
    return sizeof(iov_)/sizeof(iov_[0]);
  }
};

struct Bind_Request
{
  static const uint16_t MSG = 1;

 protected:
  typedef Bind_Request MSG_CLASS;
  const uint16_t msg_type_;
  uint16_t service_off_;
  uint16_t service_len_;

  inline explicit Bind_Request(const uint16_t msg_type)
    : msg_type_(msg_type)
  { }

 public:
  inline uint16_t msg_type() const
  {
    return msg_type_;
  }
};

struct Bind_Request_Proxy
  : Bind_Request
{
 protected:
  typedef Bind_Request_Proxy Proxy_Class;

  inline bool validate(const char * const buffer, const size_t length) const
  {
    if ((service_off_ + service_len_ >= length) ||
	(buffer[service_off_ + service_len_] != '\0'))
    {
      return false;
    }

    return true;
  }

 public:
  inline static const Proxy_Class &get(const char * const buffer,
				       const size_t length)
  {
    const Proxy_Class &self = *reinterpret_cast<const Proxy_Class *>(buffer);

    if ((length < sizeof(MSG_CLASS)) ||
	!self.validate(buffer, length))
    {
      throw std::invalid_argument("message buffer");
    }

    return self;
  }

  inline std::string service() const
  {
    return std::string(reinterpret_cast<const char *>(this) + service_off_,
		       service_len_);
  }

  inline const char *service_ptr() const
  {
    return reinterpret_cast<const char *>(this) + service_off_;
  }

  inline size_t service_len() const
  {
    return service_len_;
  }
};

struct Bind_Request_Builder
  : Bind_Request
{
 private:
  struct iovec iov_[2];
  std::string service_;

 public:
  inline Bind_Request_Builder()
    : Bind_Request(MSG)
  { }

  inline const std::string &service() const
  {
    return service_;
  }

  inline void service(const std::string &service)
  {
    service_ = service;
  }

  inline iovec *iov()
  {
    size_t off = sizeof(MSG_CLASS);

    service_off_ = off;
    service_len_ = service_.length();
    off += service_len_;

    iov_[0].iov_base = this;
    iov_[0].iov_len = sizeof(MSG_CLASS);
    iov_[1].iov_base = const_cast<char *>(service_.c_str());
    iov_[1].iov_len = service_len_ + 1;

    return iov_;
  }

  inline int iovlen()
  {
    return sizeof(iov_)/sizeof(iov_[0]);
  }
};

struct Bind_Reply
{
  static const uint16_t MSG = 2;

 protected:
  typedef Bind_Reply MSG_CLASS;
  const uint16_t msg_type_;
  int32_t error_;

  inline explicit Bind_Reply(const uint16_t msg_type)
    : msg_type_(msg_type)
  { }

 public:
  inline uint16_t msg_type() const
  {
    return msg_type_;
  }

  inline int32_t error() const
  {
    return error_;
  }
};

struct Bind_Reply_Proxy
  : Bind_Reply
{
 protected:
  typedef Bind_Reply_Proxy Proxy_Class;

  inline bool validate(const char * const buffer, const size_t length) const
  {
    return true;
  }

 public:
  inline static const Proxy_Class &get(const char * const buffer,
				       const size_t length)
  {
    const Proxy_Class &self = *reinterpret_cast<const Proxy_Class *>(buffer);

    if ((length < sizeof(MSG_CLASS)) ||
	!self.validate(buffer, length))
    {
      throw std::invalid_argument("message buffer");
    }

    return self;
  }
};

struct Bind_Reply_Builder
  : Bind_Reply
{
 private:
  struct iovec iov_[1];

 public:
  inline Bind_Reply_Builder()
    : Bind_Reply(MSG)
  { }

  inline void error(const int32_t value)
  {
    error_ = value;
  }

  inline iovec *iov()
  {
    iov_[0].iov_base = this;
    iov_[0].iov_len = sizeof(MSG_CLASS);

    return iov_;
  }

  inline int iovlen()
  {
    return sizeof(iov_)/sizeof(iov_[0]);
  }
};


struct Listen_Request
{
  static const uint16_t MSG = 3;

 protected:
  typedef Listen_Request MSG_CLASS;
  const uint16_t msg_type_;
  uint16_t service_off_;
  uint16_t service_len_;

  inline explicit Listen_Request(const uint16_t msg_type)
    : msg_type_(msg_type)
  { }

 public:
  inline uint16_t msg_type() const
  {
    return msg_type_;
  }
};

struct Listen_Request_Proxy
  : Listen_Request
{
 protected:
  typedef Listen_Request_Proxy Proxy_Class;

  inline bool validate(const char * const buffer, const size_t length) const
  {
    if ((service_off_ + service_len_ >= length) ||
	(buffer[service_off_ + service_len_] != '\0'))
    {
      return false;
    }

    return true;
  }

 public:
  inline static const Proxy_Class &get(const char * const buffer,
				       const size_t length)
  {
    const Proxy_Class &self = *reinterpret_cast<const Proxy_Class *>(buffer);

    if ((length < sizeof(MSG_CLASS)) ||
	!self.validate(buffer, length))
    {
      throw std::invalid_argument("message buffer");
    }

    return self;
  }

  inline std::string service() const
  {
    return std::string(reinterpret_cast<const char *>(this) + service_off_,
		       service_len_);
  }

  inline const char *service_ptr() const
  {
    return reinterpret_cast<const char *>(this) + service_off_;
  }

  inline size_t service_len() const
  {
    return service_len_;
  }
};

struct Listen_Request_Builder
  : Listen_Request
{
 private:
  struct iovec iov_[2];
  std::string service_;

 public:
  inline Listen_Request_Builder()
    : Listen_Request(MSG)
  { }

  inline const std::string &service() const
  {
    return service_;
  }

  inline void service(const std::string &service)
  {
    service_ = service;
  }

  inline iovec *iov()
  {
    size_t off = sizeof(MSG_CLASS);

    service_off_ = off;
    service_len_ = service_.length();
    off += service_len_;

    iov_[0].iov_base = this;
    iov_[0].iov_len = sizeof(MSG_CLASS);
    iov_[1].iov_base = const_cast<char *>(service_.c_str());
    iov_[1].iov_len = service_len_ + 1;

    return iov_;
  }

  inline int iovlen()
  {
    return sizeof(iov_)/sizeof(iov_[0]);
  }
};


struct Accept_Event
{
  static const uint16_t MSG = 4;

 protected:
  typedef Accept_Event MSG_CLASS;
  const uint16_t msg_type_;
  uint16_t rdns_off_;
  uint16_t rdns_len_;

  inline explicit Accept_Event(const uint16_t msg_type)
    : msg_type_(msg_type)
  { }

 public:
  inline uint16_t msg_type() const
  {
    return msg_type_;
  }
};

struct Accept_Event_Proxy
  : Accept_Event
{
 protected:
  typedef Accept_Event_Proxy Proxy_Class;

  inline bool validate(const char * const buffer, const size_t length) const
  {
    if ((rdns_off_ + rdns_len_ >= length) ||
	(buffer[rdns_off_ + rdns_len_] != '\0'))
    {
      return false;
    }

    return true;
  }

 public:
  inline static const Proxy_Class &get(const char * const buffer,
				       const size_t length)
  {
    const Proxy_Class &self = *reinterpret_cast<const Proxy_Class *>(buffer);

    if ((length < sizeof(MSG_CLASS)) ||
	!self.validate(buffer, length))
    {
      throw std::invalid_argument("message buffer");
    }

    return self;
  }

  inline std::string rdns() const
  {
    return std::string(reinterpret_cast<const char *>(this) + rdns_off_,
		       rdns_len_);
  }

  inline const char *rdns_ptr() const
  {
    return reinterpret_cast<const char *>(this) + rdns_off_;
  }

  inline size_t rdns_len() const
  {
    return rdns_len_;
  }
};

struct Accept_Event_Builder
  : Accept_Event
{
 private:
  struct iovec iov_[2];
  std::string rdns_;

 public:
  inline Accept_Event_Builder()
    : Accept_Event(MSG)
  { }

  inline const std::string &rdns() const
  {
    return rdns_;
  }

  inline void rdns(const std::string &rdns)
  {
    rdns_ = rdns;
  }

  inline iovec *iov()
  {
    size_t off = sizeof(MSG_CLASS);

    rdns_off_ = off;
    rdns_len_ = rdns_.length();
    off += rdns_len_;

    iov_[0].iov_base = this;
    iov_[0].iov_len = sizeof(MSG_CLASS);
    iov_[1].iov_base = const_cast<char *>(rdns_.c_str());
    iov_[1].iov_len = rdns_len_ + 1;

    return iov_;
  }

  inline int iovlen()
  {
    return sizeof(iov_)/sizeof(iov_[0]);
  }
};
}
}

#endif

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License
 * at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and
 * limitations under the License.
 *
 * The Original Code is the nginetd (next-generation inetd) project.
 *
 * The Initial Developer of the Original Code is Christof Meerwald.
 * Portions created by the Initial Developer are Copyright (C)
 * Christof Meerwald. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
